#!/usr/bin/env python3
"""
    Wrapper utility around qstat -f that parses the output (given in json format)
    and returns a wide listing.
"""

import paramiko
import argparse
import subprocess
import json
from functools import reduce 
import time

# Fields to Extract from qstat full output
fields = [
    ['Job_Name'],
    ['Job_Owner'],
    ['job_state'], 
    ['queue'],
    ['Resource_List', 'nodect'],
    ['Resource_List', 'walltime']
]
field_character_limit = 50

def get_remote_qstat_output(machine):
    """
        Use paramiko to get qstat output from remote machine
       
        Note that for paramiko, must do this via shell. With Fabric it might be different.
    """
    # required as the non-login shell below will not know where to look. Seems quite standard location.
    qstat_path='/opt/pbs/bin/qstat'
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # Don't require the machine to be in 'known_hosts'
    ssh.connect(hostname=machine)
    stdin, stdout, stderr = ssh.exec_command(f"{qstat_path} -f -Fjson") 
    qstat_output = stdout.read()
    return json.loads(qstat_output)

def get_local_qstat_output():
    """
        Simply use subprocess to get qstat output in json format
    """
    qstat_output = subprocess.check_output('qstat -f -Fjson', shell=True)
    return json.loads(qstat_output)

def get_qstat_output(machine=None):
    """
        Either query qstat locally, or remotely and return output
        in json format
    """
    if machine:
        qstat_output = get_remote_qstat_output(machine=machine)
    else:
        qstat_output = get_local_qstat_output()
    return qstat_output

def print_wide_status(Job_ID, Job_Name, Job_Owner, job_state, queue, nodect, walltime, walltime_used):
    # we only apply the field character limit to the output job_name as the other fields should be much less
    print(f'{Job_ID:20s} {Job_Name:{field_character_limit}s} {Job_Owner:8s} {job_state:5s} {queue:15s} {nodect:5s} {walltime:15s} {walltime_used:15s}')

def pbs_q_query(user=None, machine=None):
    qstat_output = get_qstat_output(machine)
    print("\n -------------- qstat wide -------------- ")
    print_wide_status("JOB ID", "JOB NAME", "OWNER", "STATE", "QUEUE", "NODES", "REQ. WALLTIME", "USED WALLTIME")
    for job, job_info in qstat_output['Jobs'].items():
        job_vals = { "Job_ID" : job }
        for field in fields:
            # first get the actual final field id from the nest 'fields' list
            field_id = reduce( lambda a,b: b, field )

            # now desired entry from job info dictionary (again reduce to get nested entries)
            field_entry = str(reduce(dict.get, field, job_info))
            if field_id == "Job_Owner":
                field_entry = field_entry.split('@')[0] # remove PBS server from Job_Owner
            job_vals[field_id] = field_entry[:field_character_limit]
              
        # Only for running jobs do used times exist    
        walltime_used = "--:--"
        if job_info['job_state'] == 'R':
            if 'resources_used' in job_info.keys():
                if 'walltime' in job_info['resources_used'].keys():
                    walltime_used = str(job_info['resources_used']['walltime'])
        job_vals["walltime_used"] = walltime_used
            
        if user:
            if (job_vals['Job_Owner'] != user):
                continue
        print_wide_status(**job_vals)

def monitor_queue(user=None, machine=None, ntimes=1, freq=30):
    """
        Query the pbs queue ntimes, every freq seconds.
    """
    for _ in range(ntimes):
        pbs_q_query(user=user, machine=machine)
        if ntimes > 1:
            time.sleep(freq)

def parse_args():
    cli = argparse.ArgumentParser("Wrapper around qstat to support wide format queue listing")
    cli.add_argument('-m', '--machine', default=None, required=False, help="The machine to check.")
    cli.add_argument('-u', '--user',    default=None, required=False, help="The name of a user to search queue by.")
    cli.add_argument('-n', '--ntimes',  default=1,    required=False, help="The number of times to repeat query. Defaults to 1 query.", type=int)
    cli.add_argument('-f', '--freq',    default=30,   required=False, help="If ntimes > 1, defines the frequency (in seconds) of the queries. Defaults to 30 seconds", type=int)
    args = vars(cli.parse_args())
    return args

def main():
    args = parse_args()
    monitor_queue(**args)

if __name__ == "__main__":
    main()



