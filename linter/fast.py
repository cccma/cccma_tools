#!/usr/bin/env python3
'''
Fortran Adaptive Standardization Tools (FAST) for linting and adapting Fortran code
to CCCma standards. See README.md for user instructions.

@author: Matthew Fortier
gitlab.com/mfortier
'''

import os
import sys
import argparse
import re
import yaml
import shutil
from lib.linter import *
from lib.fixer import *

def parse_arguments():
    parser = argparse.ArgumentParser(description='FAST: Fortran Adaptive Standardization Tool. This tool serves multiple purposes; \
        in its default mode it will lint any fortran code contained within the current directory (with recursive subdirectory search). \
        It may also be used in \'fix mode\' to automatically modernize fortran files specified with the --fix argument. This is useful \
        for enforcing coding standards or modernizing old code. If the fixer messes up your work, you can use the --restore argument to \
        undo the changes.')
    parser.add_argument('--debug', action='store_true', help='Creates intermediary files between fixing steps for debugging.')
    parser.add_argument('--config', action='store', nargs=1, help='Specifies the YAML configuration file location. Relative \
        paths are acceptable. If no config is specified and FAST is running in linter mode, it will look in the current directory.')
    parser.add_argument('--fix', nargs='+', help='Rewrites the file enforcing linting rules. Backup file will be produced. If \
        used, the linter will only fix the specified files. Standard linting will not be performed.')
    parser.add_argument('--rename', action='store_true', help='Renames fixed files from .f/.F to .f90. Only used when running FAST in --fix mode.')
    parser.add_argument('--restore', nargs='+', help='Restores the file from a pre-linted backup. Will error if no backup found. \
        If used, the linter will only restore the specified files. Standard linting will not be performed.')
    args = parser.parse_args()
    return args

def main():
    args = parse_arguments()
    if (args.restore):
        run_restore(args.restore)
    if (args.fix):
        run_fixer(args.fix, debug=args.debug, rename=args.rename)
    if (not args.restore and not args.fix):
        if args.config is not None:
            args.config=args.config[0]
        run_linter(yaml_file=args.config)
    sys.exit()

# Deletes linted files, restores the backups into their place, and deletes
# the backup folder if it is no longer needed
def run_restore(files):
    for ifile in files:
        if '/' in ifile:
            backup = re.sub(r'(.*\/)([^\/]*)',r'\1backups/\2',ifile)
        else:
            backup = f'backups/{ifile}'
        if not os.path.isfile(backup):
            backup = backup[:-2]
            if not os.path.isfile(backup):
                print(f'Warning: no backup to restore for file {ifile}')
                continue
        backup_dir = ''
        directory = re.match(r'(.*)(?=\/)', ifile)
        if not directory:
            backup_dir += './backups'
        else:
            backup_dir += directory.group(1)
            backup_dir += '/backups'
        os.remove(ifile)
        mfn = re.sub(r'(.*)\.(.*)', r'\1.f90', ifile) # if the filename was modified
        if os.path.isfile(mfn):
            os.remove(mfn)
        os.remove(backup + '.comments')
        newlocation = re.sub(r'(.*\/?)backups\/(.*)',r'\1\2',backup)
        shutil.move(backup, newlocation)
        if len(os.listdir(backup_dir)) == 0:
            os.rmdir(backup_dir)

# Runs the linter, using an appropriate yaml spec file.
def run_linter(yaml_file=None):
    if yaml_file is None:
        yaml_file = f'{os.getcwd()}/linter.yml'
    try:
        with open(yaml_file, 'r') as f:
            yaml_specs = yaml.load(f, Loader=yaml.FullLoader)
    except:
        print('No linter spec file found, using default...')
        yaml_file = f'{os.path.dirname(os.path.realpath(__file__))}/linter.yml'
        with open(yaml_file, 'r') as f:
            yaml_specs = yaml.load(f, Loader=yaml.FullLoader)
    filelist = []
    for root, dirs, files in os.walk('.'):
        for name in files:
            filelist.append(os.path.join(root,name))

    blacklist = yaml_specs['ignore']['directories'] + yaml_specs['ignore']['files']
    filelist = [file for file in filelist if _should_lint(file, blacklist)]
    rules = _fill_rules(yaml_specs['rules'])
    lint(filelist, rules)

def _should_lint(file, blacklist):
    """
        Local function to determine if a file should be linted or not  
    """
    if file[-2:] not in ['.f','.F','.h'] and file[-4:] not in ['.f90','.F90']:
        return False
    for bf in blacklist:
        if bf in file:
            return False
    return True

def _fill_rules(yaml_rules):
    """
        Local function to fill out the yaml rules
    """
    full_rules = {}
    for rule, specs in yaml_rules.items():
        if "exceptions" not in specs:
            specs["exceptions"] = [ None ]
        full_rules[rule] = specs
    return full_rules

if __name__ == '__main__':
    main()
