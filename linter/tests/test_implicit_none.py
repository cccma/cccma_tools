import sys
import os
lint_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f'{lint_dir}/../')
from lib.linter import check_implicit_none


def test_subroutine_no_implicit_none():
  """Test for subroutines without implicit none
  """

  source = []
  source.append('subroutine foo()')
  source.append('end subroutine foo')

  assert check_implicit_none(source)

def test_subroutine_implicit_none():
  """Test for subroutines with implicit none
  """
  source = []
  source.append('subroutine foo()')
  source.append('implicit none')
  source.append('end subroutine foo')

  assert not check_implicit_none(source)

def test_function_no_implicit_none():
  """Test for functions without implicit none
  """

  source = []
  source.append('function foo()')
  source.append('end function foo')

  assert check_implicit_none(source)

def test_function_implicit_none():
  """Test for functions with implicit none
  """
  source = []
  source.append('function foo()')
  source.append('implicit none')
  source.append('end function foo')

  assert not check_implicit_none(source)

def test_module_no_implicit_none():
  """Test for modules without implicit none
  """

  source = []
  source.append('module foo')
  source.append('end module foo')

  assert check_implicit_none(source)

def test_module_implicit_none():
  """Test for modules with implicit none
  """
  source = []
  source.append('module foo')
  source.append('implicit none')
  source.append('end module foo')

  assert not check_implicit_none(source)

def test_subroutine_in_module_implicit_none():
  """Test for subroutines with implicit none
  """
  source = []
  source.append('module foo')
  source.append('implicit none')
  source.append('contains')
  source.append('subroutine foo()')
  source.append('end subroutine foo')
  source.append('end module foo')

  assert not check_implicit_none(source)

def test_function_in_module_implicit_none():
  """Test for functions with implicit none
  """
  source = []
  source.append('module foo')
  source.append('implicit none; private')
  source.append('contains')
  source.append('function foo()')
  source.append('end function foo')
  source.append('end module foo')

  assert not check_implicit_none(source)