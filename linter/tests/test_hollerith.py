
import sys
import os
lint_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f'{lint_dir}/../')
from lib.linter import check_hollerith_var

def test_hollerith():
  """Test for subroutines without implicit none
  """

  source = []
  source.append('3HROW')

  assert check_hollerith_var(source)

def test_hollerith_comment():
  """Test for subroutines without implicit none
  """

  source = []
  source.append('! In a comment 3HROW')

  assert not check_hollerith_var(source)