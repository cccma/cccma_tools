#!/usr/bin/python3
import re
import sys

def main():
    lines = []
    inout_vars = []
    with open('vars.txt', 'r') as f:
        inout_vars = [x.strip('\n') for x in f.readlines() if len(x) > 0]

    filename = sys.argv[1]
    with open(filename, 'r') as f:
        lines = f.readlines()

    correctedLines = []
    for line in lines:
        the_line = line
        if '!&!&!&' in the_line:
            the_line = the_line.replace('!&!&!&', '')
        for inout_var in inout_vars:
            var_regex = rf'^(\s*)(real\(\d+\)|integer\(\d+\)|real\*\d+|integer\*\d+|real|integer|logical|complex)[^&:]*::\s*{inout_var}'
            if re.match(var_regex, the_line, re.IGNORECASE):
                the_line = re.sub(r'^(.*)intent\(in\)(.*)$', r'\1intent(inout)\2', the_line, re.IGNORECASE)
        correctedLines.append(the_line)


    with open(filename, 'w') as f:
        for line in correctedLines:
            f.write(line)

if __name__ == '__main__':
    main()