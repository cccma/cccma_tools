#!/usr/bin/python3
import re
import sys

def main():
    filename = sys.argv[1]
    lines = []
    correctedLines = []
    with open(filename, 'r') as f:
        lines = f.readlines()

    variables = getVariables(lines)

    lines = addIntent(lines, variables)
    with open(filename, 'w') as f:
        for line in lines:
            f.write(line)



def addIntent(lines, variables):
    correctedLines = []
    for line in lines:
        module_decl = re.match(r'\s*(use|#include)\s+', line, re.IGNORECASE)
        if module_decl:
            correctedLines.append(f'!&!&!&{line}')
            continue
        theLine = line
        copied_variables = variables.copy()
        for var in copied_variables:
            varRegex = rf'^\s*(real|integer|logical|character|complex)[^:!&]*::\s*\b{var}\b'
            matched = re.match(varRegex, line, re.IGNORECASE)
            if matched:
                if re.match(r'^[^&]*\bintent\b', line, re.IGNORECASE) is None:
                    theLine = re.sub(rf'^(\s*)(real\(\d+\)|integer\(\d+\)|real\*\d+|integer\*\d+|logical\(\d+\)|real|integer|logical|complex)', r'\1\2, intent(in)', theLine, re.IGNORECASE)
                    variables.remove(var)
                    break
        correctedLines.append(theLine)
    return correctedLines


def getVariables(lines):
    continuation = False
    variables = []
    for line in lines:
        matchedLine = re.match(r'^\s*(subroutine|function)\s+(\w+)\s*\(\s*([^\n&!\)]*)(\)?)(&?)', line, re.IGNORECASE)
        if matchedLine:
            newVars = [x.strip() for x in matchedLine[3].split(',') if len(x.strip()) > 0]
            if len(newVars) > 0:
                variables.extend(newVars)
            if len(matchedLine[5]) > 0:
                continuation = True
            continue
        
        if continuation:
            matchedLine = re.match(r'\s*([^!&\)]*)(\)?)(&?)', line, re.IGNORECASE)
            newVars = [x.strip() for x in matchedLine[1].split(',') if len(x.strip()) > 0]
            if len(newVars) > 0:
                variables.extend(newVars)
            if len(matchedLine[2]) > 0:
                continuation = False
    return variables


if __name__ == '__main__':
    main()