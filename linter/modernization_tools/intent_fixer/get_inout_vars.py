#!/usr/bin/python3
import re
import sys

def main():
    log = sys.argv[1]
    lines = []
    with open(log, 'r') as f:
        lines = f.readlines()

    inout_variables = []
    other_errors = []
    filename = ''

    for i, line in enumerate(lines):
        if 'Error:' in line:
            implicit_var = re.search(r'Dummy argument \‘(\w+)\’', line, re.IGNORECASE)
            if implicit_var is None:
                #print('ERROR: unexpected problem with compilation')
                #print(line)
                #sys.exit(1)
                pass
            else:
                inout_variables.append(implicit_var[1])
    
    inout_variables = list(set(inout_variables))
    for v in inout_variables:
        print(v)

if __name__ == '__main__':
    main()