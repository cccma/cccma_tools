#!/usr/bin/python3
import re
import sys

def main():
    filename = sys.argv[1]
    lines = []
    correctedLines = []
    with open(filename, 'r') as f:
        lines = f.readlines()

    continuation = False
    found = False
    for line in lines:
        if re.match(r'^\s*implicit\s+[^nN]', line, re.IGNORECASE) and found == True:
            sys.stderr.write(f'ERROR: {filename} has multiple program components')
            sys.exit(1)
        if re.match(r'^\s*implicit\s+[^nN]', line, re.IGNORECASE) and found == False:
            found = True
            correctedLines.append('  implicit none\n')
            if re.match(r'^[^!&\n]*&', line, re.IGNORECASE):
                continuation = True
            continue
        if continuation:
            if re.match(r'^[^!&\n]*&', line, re.IGNORECASE) == None:
                continuation = False
            continue
        if re.match(r'^\s*\buse\b\s+', line, re.IGNORECASE):  # comment out module imports
            correctedLines.append(f'!{line}')
            continue
        correctedLines.append(line)

    with open('temp.f90', 'w') as f:
        for line in correctedLines:
            f.write(line)


if __name__ == '__main__':
    main()