# Modernization Tools
This directory contains two tools used to modernize the CanAM codebase: an implicit variable fixer, and an intent fixer. Their layout and use is nearly identical, preferring to process small numbers of files at a time (think 1 directory at a time).

Use of these tools is at the user's risk; they are at best finnicky, and will require double checking with each file changed. Used correctly, however, they can save a lot of time. Both tools are used as follows:

```
./orchestrator.sh [../../../path/to/files/*]
```

Before use, go into the orchestrator script. There is a line with a comment dictating the compiler to be specified. At time of creation, the author was using `gfortran` inside a singularity container, but on internal CCCma systems it may be better to simply specify the compiler a la carte, or use `ifort`.

## Implicit Fixer
This script iterates through each file passed to the orchestrator, and does the following:
* Copies the file to the current working directory
* Changes the implicit declaration to be `implicit none`
* Comments out any package imports
* Attempts to compile the file (will fail) and pipes the output to a local txt file
* Parses that txt file to get all the implicit declaration violations
* Formats those into errors into declaration statements, based on integers (i-n), and reals (everything else)
* Opens up the original fortran file in its original location, makes it implicit none, and inserts all these declarations immediately below

You will notice obvious flaws in this system, and edge cases it cannot handle. This is the unfortunate reality of these tools; the more specific you make them, the more brittle they become. This is why it's necessary to look over the files and ensure nothing has been broken.

## Intent Fixer
This script operates in a similar manner to the implicit fixer, with a few key differences:
* It detects all variables passed into a subroutine
* It makes all of those variables `intent(in)`
* Attempts compilation and records errors from the failed compilation, isolating which variables are being assigned
* Changes those variables to `intent(inout)`

Again, make sure to check over the changes and see if they make sense. This is all much quicker than doing it manually, but it's not as efficient as the FAST fixer.