'''
This tool combs through the code and keeps a tally of how many indentations
each line of code should have (and automatically corrects it). The logic here
is necessarily quite twisted, and I would not recommend attempting to modify
it unless absolutely necessary. This is about as detailed as the tool could
be without writing a literal compiler.

At the end of the program, self.levelNumber should be 0. If not, this means there
is a code block that is not terminated properly, or perhaps a bug in the linter's
ability to detect them.

The other error is thrown if self.levelNumber ever drops below 0, which should not
happen. If this error arises, check the line number where it happens.
'''
import re

class WhitespaceChecker(object):

    def __init__(self):
        print('Whitespace Checker initialized')

    def process(self, lines):
        self.lines = lines # stores the lines of the pre-linted file
        self.correctedLines = [] # will store the linted lines to be put back into the file
        self.ignoreLines = 0 # tracks number of lines from the !ignoreLint(x) directive
        self.directive = False # if true, we're in a preprocessor directive (and won't touch anything)
        self.levelNumber = 0 # how many block indentations in are we?
        self.multiline_spacing = 0
        self.continuationLine = False # flag for if we're continuing from a previous line with '&'
        self.continuedIf = False # flag for if we're in a continued if statement (important edge case)
        self.continuedWhere = False # flag for if we're in a continued where statement (important edge case)
        self.checkWhiteSpace()
        if self.levelNumber != 0: # we haven't finished with 0 whitespace; this is a problem.
            print(f'Whitespace error: finished with levelNumber {self.levelNumber}')
        for i, line in enumerate(self.correctedLines):
            if not line.endswith('\n'):
                line += '\n'
            self.correctedLines[i] = line
        return self.correctedLines

    def checkWhiteSpace(self):
        subcall = 0 # tracks the number of spaces to indent subsequent lines of a multi-line subroutine call
        for i, line in enumerate(self.lines):
            if self.ignoreLines == 0 and self.levelNumber < 0: # big error! we've got negative whitespace.
                print(f'Whitepsace error at line {i+1}: levelNumber < 0')
                return
            # check if there's another reason we should be ignoring this line
            if self.skippable(line):
                self.correctedLines.append(line)
                continue
            # deal with lines that only contain a comment
            if self.isCommentLine(line):
                newline = ''
                for x in range(self.levelNumber):
                    newline += '  '
                newline += line.strip(' ')
                self.correctedLines.append(newline)
                continue
            # check if we flagged the previous line as continuation
            if self.continuationLine:
                newline = ''
                # are we continuing a multi-line subroutine call?
                if subcall > 0:
                    for x in range(subcall):
                        newline += ' '
                    newline += line.strip()
                # otherwise, just use the line as-is
                else:
                    newline = line
                # end of continuation lines?
                if not re.match(r'^[^\n!]*&', line, re.IGNORECASE):
                    self.continuationLine = False
                    subcall = 0
                self.correctedLines.append(newline)
                continue
            # all other cases
            else:
                newline = ""
                # check that this line follows regular formatting
                parsed_line = re.match(r'(^)(\d*)(\s*)(.*)\n', line, re.IGNORECASE)
                line_label = parsed_line.group(2)
                line_contents = parsed_line.group(4)
                if not parsed_line:
                    self.correctedLines.append(line)
                    continue
                if self.isBlockEnd(line_contents):
                    self.levelNumber -= 1

                # if we have a labelled line, we must account for that in the whitespace
                if line_label != "":
                    newline += line_label
                    newline += " "

                # add whitespace based on the levelNumber, and account for line labelling
                num_spaces = max(0, 2*self.levelNumber - len(newline))
                for x in range(num_spaces):
                    newline += " "
                if self.continuedIf or self.continuedWhere:
                    newline += "    "
                # 'contains' should be shifted back by 2 spaces
                if re.match(r'\bcontains\b', line_contents, re.IGNORECASE):
                    newline = newline[:-2]
                newline += line_contents
                self.correctedLines.append(newline)

                # Now determine the action for next line
                self.analyzeLine(line_contents)
                if re.match(r'^[^\n!]*&', line, re.IGNORECASE) and not self.continuedIf and not self.continuedWhere:
                    self.continuationLine = True
                    subfuncall = re.match(r'^([^\n!\"\']*)\b(call|subroutine)\b([^\n!\(]*)\(', line, re.IGNORECASE)
                    declaration = re.match(r'^([^\n!:\"\']*):: ', line, re.IGNORECASE)
                    arithmetic = re.match(r'^([^\n!=\"\']*)= ', line, re.IGNORECASE)
                    if subfuncall:
                        subcall = len(subfuncall.group(0))
                    elif declaration:
                        subcall = len(declaration.group(0))
                    elif arithmetic:
                        subcall = len(arithmetic.group(0))


    # checks if a line should be skipped by the linter
    def skippable(self, line):
        lineSkip = re.match(r'^\s*!ignoreLint\((\d+)\)', line, re.IGNORECASE) # check for !ignoreLint()
        if lineSkip:
            self.ignoreLines = int(lineSkip.group(1))
            return True
        elif self.ignoreLines > 0:
            self.ignoreLines -= 1
            return True
        elif re.match(r'^[ \t]*#if', line, re.IGNORECASE): # the start of a preprocessor directive
            self.directive += 1
            return True
        elif re.match(r'^[ \t]*#endif', line, re.IGNORECASE): # last line of the directive
            self.directive -= 1
            return True
        elif re.match(r'^[ \t]*#', line, re.IGNORECASE): # last line of the directive
            return True
        elif self.directive > 0:
            return True
        elif self.isBlankLine(line):
            return True
        else:
            return False

    # checks if we should revert whitespace on this line (eg. 'end if')
    def isBlockEnd(self, line):
        return re.match(r'^\s*\b(end|case|else)\b', line, re.IGNORECASE)

    # checks if we should increase whitespace on the NEXT line (eg. 'do j=1,n')
    def analyzeLine(self, line):
        if re.match(r'^[^!]*<<<', line, re.IGNORECASE): # ignore lines that the linter has already flagged as awful
            return
        
        # Module/Procedure/Interface/Function/Program/Subroutine/Type/Case/Else/Select/Forall
        if re.match(r'^[^\n!\'"]*\b(module(?! procedure )|interface|function|program|do|type(?!\()(?!\s\()|subroutine|case|select|else|forall)\b', line, re.IGNORECASE) \
        and not re.match(r'^[^\n!]*\bend\b', line, re.IGNORECASE):
            self.levelNumber += 1
        else:

            # Where
            if self.continuedWhere: # halfway through the where conditional
                if re.match(r'^[^!\n]*&', line, re.IGNORECASE):
                    return
                elif self.isSingleLineWhere(f'where ({line}'):
                    self.continuedWhere = False
                else:
                    self.levelNumber += 1
            elif re.match(r'^[^\n!\'"]*\b(?<!end )(?<!else )where\b', line, re.IGNORECASE):
                if self.isContinuationLine(line):
                    if self.hasContainedConditional(line):
                        self.continuationLine = True
                    else:
                        self.continuedWhere = True
                    return
                elif not self.isSingleLineWhere(line):
                    self.levelNumber += 1

            # If
            if self.continuedIf and re.match(r'^[^!\n]*\bthen\b', line, re.IGNORECASE):
                self.levelNumber += 1
            elif self.continuedIf and re.match(r'^[^!\n]*&', line, re.IGNORECASE):
                return
            elif re.match(r'^[^!\n\'"]*\bif\b[^!\n]*\bthen\b', line, re.IGNORECASE):
                self.levelNumber += 1
            elif re.match(r'^[^!\n\'"]*(?<!end )\bif\b[^!\n]*&', line, re.IGNORECASE):
                self.continuedIf = True
                return
        self.continuedIf = False
        self.continuedWhere = False

    def isContinuationLine(self, line):
        return re.match(r'^[^\n!]*&', line)

    # For 'if' and 'where' loops, checks if the conditional is entirely contained on this line.
    def hasContainedConditional(self, line):
        singleLineMatch = re.match(r'^\s*\b(where|if)\s*(\(.*$)', line, re.IGNORECASE)
        if singleLineMatch is None:
            return False
        postControlText = singleLineMatch[2]
        # Create parentheses stack
        pStack, postControlText = [postControlText[:1]], postControlText[1:]
        while len(pStack) > 0:
            if len(postControlText) == 0:
                return False
            c, postControlText = postControlText[:1], postControlText[1:]
            if c == '(':
                pStack.append(c)
            elif c == ')':
                pStack.pop()
        return True

    def isBlankLine(self, line):
        return re.match(r'^[ \t]*$', line)

    def isCommentLine(self, line):
        return re.match(r'^[ \t]*!', line)

    # checks for single line where statements, which are a uniquely challenging case for Fortran linting (requires pushdown automata)
    def isSingleLineWhere(self, line):
        singleLineMatch = re.match(r'^\s*\bwhere\s*(\(.*$)', line, re.IGNORECASE)
        if singleLineMatch is None:
            return False
        postWhereText = singleLineMatch[1]
        # Create parentheses stack
        pStack, postWhereText = [postWhereText[:1]], postWhereText[1:]
        while len(pStack) > 0:
            if len(postWhereText) == 0:
                return False
            c, postWhereText = postWhereText[:1], postWhereText[1:]
            if c == '(':
                pStack.append(c)
            elif c == ')':
                pStack.pop()
        if re.match(r'^\s*[^!\n\s]+', postWhereText, re.IGNORECASE) is None:
            return False
        else:
            return True
