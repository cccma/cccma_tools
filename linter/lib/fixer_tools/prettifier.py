'''
Prettifier and syntax corrector. Tuples of rules are specified near the top of
the file, with the order being (regex to match, regex to change to, error string).
New rules can be added with relative ease. Some are commented out because they
caused more problems than they were worth, but may be desirable in very specific
files.

Code originally adapted from cphyc, though **heavily** modified. Credited below.

@author: cphyc
github of original author: https://github.com/cphyc
'''
import re

class FortranRules(object):
    rules = [
        (r'(\d+)(\s+)(\.\d)', r'\2\1\3', 'Erroneous whitespace'),
        (r'(\s+)\n$', r'\n', 'Whitespace at end of line'),
        [
            # " :: "
            (r'(\S)::(\S)', r'\1 :: \2',
             'Missing spaces around separator'),
            (r'(\S)::', r'\1 ::',
             'Missing space before separator'),
            (r'::(\S)', r':: \1',
             'Missing space after separator'),
            (r'(real\(\d+\)|real\*\d+|integer\(\d+\)|integer\*\d+|complex\(\d+\)|complex\*\d+|real|character\([^\)\n]*\)|character\*\d+|type\([^\)\n]*\)|complex|integer|logical)(\s+)(?!function)(\w+)',
             r'\1\2:: \3', 'Missing :: ')
        ],
        [
            # old -> new comparative operators
            (r'\.(eq|EQ)\.', r'==', '.eq. -> =='),
            (r'\.(neq|NEQ|ne|NE)\.', r'/=', '.ne. -> /='),
            (r'\.(gt|GT)\.', r'>', '.gt. -> >'),
            (r'\.(ge|GE)\.', r'>=', '.ge. -> >='),
            (r'\.(lt|LT)\.', r'<', '.lt. -> <'),
            (r'\.(le|LE)\.', r'<=', '.le. -> <=')
        ],
        # One should write "this, here" not "this,here"
        (r'({punctuations})(\w)', r'\1 \2',
         'Missing space after punctuation'),

        # Use 'dimension' attribute for variable declaration
        (r'^(\s+)(real\(\d+\)|integer|logical|real)\s*([^:&]*)\s*::\s*([a-zA-Z0-9_]*)\s*(\([a-zA-Z0-9,_:\s]*\))(?!\s*[,&])',
         r'\1\2\3, dimension\5 :: \4', 'Dimension attribute'),

        # should use lowercase for reserved words
        (r'\b({types_upper})\b', None,
         'Unnecessary use of upper case'),

        # if (foo), ...
        (r'({structs})\(', r'\1 (',
         'Missing space before parenthesis'),

        # Keep lines shorter than 80 chars
        (r'^.{linelen_re}.+$', None, 'Line length > {linelen} characters'),

        # Convert tabulation to spaces
        (r'\t', '  ', 'Should use 2 spaces instead of tabulation'),

        # Remove dead space at end of line
        (r'(^[^\n!]*?)(\s\s+)(&|then)', r'\1 \3', 'Removed dead space before line end'),
        (r'\)then\b', r') then', ')then => ) then'),
        # Fix "foo! comment" to "foo ! comment"
        (r'(\w)\!', r'\1 !', 'At least one space before comment'),


        # Fix "!bar" to "! bar"
        (r'\!(\w)', r'! \1', 'Exactly one space after comment'),
    
        # Line labels at the beginning of the line"
        (r'^\s+(\d+)(\s+)(continue)', r'\1\2\3', 'no whitespace before line labels'),

        # Fix "!bar" to "! bar"
        (r'(![!><])(\S)', r'\1 \2', 'Exactly one space after comment'),

        # Remove trailing ";"
        (r';\s*$', r'\n', 'Useless ";" at end of line'),

        # Remove space around precision operator
        (r'\b({types})\b\s+\*\s*(\d+)', r'\1*\2', 'Unnecessary space around *'),
        (r'\b({types})\b\s*\*\s+(\d+)', r'\1*\2', 'Unnecessary space around *'),

        # No space before preprocessor directives
        (r'^\s+#', r'#', 'No space before preprocessor directives'),

        [
            # Support preprocessor instruction
            (r'(?<!#)(end|END)(if|IF|do|DO|subroutine|SUBROUTINE|function|FUNCTION|program|PROGRAM|select|SELECT|where|WHERE)', r'\1 \2',
             'Missing space after `end\''),
            (r'\belseif\b', r'else if', 'elseif -> else if'),
            (r'\belsewhere\b', r'else where', 'elsewhere -> else where')
        ],

        # Trailing whitespace
        (r'( \t)+$', r'', 'Trailing whitespaces'),

        # Clean up arguments inside function calls)
        (r'(^[^!"\'=\n]*\()\s+(\w)', r'\1\2', 'Unnecessary space in function call'),
        (r'(\w)\s+\)', r'\1)', 'Spacing after arguments'),

        # MPI
        (r'include ["\']mpif.h[\'"]', None,
         'Should use `use mpi_f08` instead (or `use mpi` if not available)'),

        # Continuation lines
        [
            (r'([^\s&])(&)', r'\1 \2', 'Missing space before &'),
            (r'(^\s*)&', r'\1', 'No & needed at beginning of continued line')
        ]
    ]

    types = [r'real', r'character', r'logical', r'integer', r'function', r'subroutine', r'end', r'module',
            r'if', r'do', r'while', r'call']
    operators = [r'==', r'/=', r'<=', r'<(?!=)', r'>=', r'(?<!=)>(?!=)', r'\.and\.',
                 r'\.or\.', r'(?<![\d\.][eEdD])\+', r'(?<![\d\.][eEdD])-', r'\*\*', r'(?<!\*)(?<!real)(?<!integer)(?<!character)\*(?!\*)', r'(?<![=<>\/])=(?![=>])']
    structs = [r'if', r'IF', r'select', r'SELECT', r'case', r'CASE', r'while', r'WHILE']
    punctuation = ['''',',''' '\)', ';'] # commas have been removed from punctuation for now

    def __init__(self, linelen=120):
        self.linelen = linelen
        operators_re = r'|'.join(self.operators)
        types_upper = r'|'.join(self.types).upper()
        types_re = r'|'.join(self.types)
        types_all = r'|'.join([types_upper, types_re])
        struct_re = r'|'.join(self.structs)
        punctuation_re = r'|'.join(self.punctuation)
        fmt = dict(     # groups of words/operators to be formatted and compiled into the rules
            operators=operators_re,
            types_upper=types_upper,
            types=types_all,
            structs=struct_re,
            punctuations=punctuation_re,
            linelen_re="{%s}" % self.linelen,
            linelen="%s" % self.linelen)

        newRules = []
        for rule in self.rules:
            newRules.append(self.format_rule(rule, fmt)) # compile and format the rules
        self.rules = newRules

    def get(self):
        return self.rules

    def format_rule(self, rule, fmt):
        if isinstance(rule, tuple): # single rule (not grouped in a list)
            rxp, replacement, msg = rule
            msg = msg.format(**fmt) if msg is not None else None
            regexp = re.compile(rxp.format(**fmt))
            return (regexp, replacement, msg)
        elif isinstance(rule, list): # recursively format grouped lists of rules
            return [self.format_rule(r, fmt) for r in rule]
        else:
            raise NotImplementedError

class Prettifier(object):
    def __init__(self):
        self.rules = FortranRules()
        print('Prettifier initialized')


    def process(self, lines, linelen=120):
        self.lines = lines
        self.correctedLines = [] # these will be written to a new file
        self.corrected_comments = [] # comments about bad lines that were changed
        self.uncorrected_comments = [] # comments about bad lines that were not changed
        self.runs = 1 # tracks how many passes we've performed
        self.errcount = 0
        self.modifcount = 0
        self.errors = []
        self.correctedErrors = 0

        # Check the lines
        self.check_lines()
        while self.correctedErrors > 0:
            self.correctedErrors = 0
            self.uncorrected_comments = []
            self.errors = []
            self.lines = self.correctedLines.copy()
            self.correctedLines = []
            self.check_lines()
            self.runs += 1
            if self.runs > 50:
                break
        self.uncorrected_comments.sort(key=lambda tup: tup[0])
        self.corrected_comments.sort(key=lambda tup: tup[0])

        # Write corrected lines to old filename
        return self.correctedLines

    def check_lines(self):
        ignoreLines = 0
        for i, line in enumerate(self.lines):
            # if a line is '!ignoreLint(x)', then the linter skips the following x lines
            lineSkip = re.match(r'^\s*!ignoreLint\((\d+)\)', line)
            if lineSkip:
                self.correctedLines.append(line)
                ignoreLines = int(lineSkip.group(1))
                continue
            if ignoreLines > 0:
                self.correctedLines.append(line)
                ignoreLines -= 1
                continue
            meta = {'line': i + 1,
                    'original_line': line.replace('\n', '')}

            line, _ = self.check_ruleset(line, line, meta, self.rules.get())
            self.correctedLines.append(line)

    def check_ruleset(self, line, original_line, meta, ruleset, depth=0):
        if isinstance(ruleset, tuple):
            rule = ruleset
            line, hints = self.check_rule(
                line, original_line, meta, rule)
        else:
            for rule in ruleset:
                line, hints = self.check_ruleset(
                    line, original_line, meta, rule, depth+1)
                # Stop after first match
                if hints > 0 and depth >= 1:
                    break

        return line, hints

    def check_rule(self, line, original_line, meta, rule):
        regexp, correction, msg = rule
        errs = 0
        hints = 0
        newLine = line
        for res in regexp.finditer(original_line):
            meta['pos'] = res.start() + 1
            hints += 1
            if correction is not None:
                newLine = regexp.sub(correction, newLine)

            meta['correction'] = newLine
            if msg is not None:
                if correction is None:
                    self.uncorrected_comments.append((int(meta["line"]), str(meta["line"]) + ":" + msg))
                else:
                    self.correctedErrors += 1
                    self.corrected_comments.append((int(meta["line"]), str(meta["line"]) + ":" + msg))

        return newLine, hints
