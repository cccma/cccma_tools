'''
This is the structural analyzer for the linter. It checks for goto statements,
do loops terminated with CONTINUE statements, presence of implicit none,
multiple variables being declared in a single statement, and other structural
issues. Many of these are fixed automatically, and most others are flagged for
the user to fix.
'''

import re
import sys

class StructuralAnalyzer(object):
    def __init__(self):
        print('Structural Analyzer initialized')

    def process(self, lines):
        self.lines = lines
        self.comments = []
        self.correctedLines = []
        self.checkStructure()
        return self.correctedLines

    def checkStructure(self):
        structs = []
        doStructs = []
        gotoStructs = []
        ignoreLines = 0 # 
        assignObj = {} # tracks info on assignment expressions
        self.i = 0 # index for tracking lines
        while self.i < len(self.lines):
            line = self.lines[self.i]
            # if a line is '!ignoreLint(x)', then the linter skips the following x lines
            lineSkip = re.match(r'^\s*!ignoreLint\((\d+)\)', line)
            if lineSkip:
                ignoreLines = int(lineSkip.group(1))
                self.correctedLines.append(line)
                self.i += 1
                continue
            elif ignoreLines > 0:
                ignoreLines -= 1
                self.correctedLines.append(line)
                self.i += 1
                continue
            
            # Fix multiline implicit statements (to prevent conflict with declarations)
            if self.is_multiline_implicit(line):
                self.fix_multiline_implicit()
                continue #self.i is incremented in fix_multiline_implicit
            
            # Fix declarations
            if self.is_declaration(line):
                self.fix_declaration()
                continue # self.i is incremented in fix_declaration

            # Track 'go to' structs and notify when they are found
            goto = re.match(r'^[^!\n]*\bgo\s*to\b\s+(\d+)', line, re.IGNORECASE)
            if goto:
                gotoStructs.append(goto[1])
                self.comments.append(f'GOTO statement found on line {len(self.correctedLines)+1}')
            
            # Track 'do' loops
            do = re.match(r'^(((?!\bend\b|!|\'|\").)*\bdo\b\s+)(\b\d*\b)(.*)$', line, re.IGNORECASE)
            if do:
                loopLabel = do[3] if len(do[3]) > 0 else None
                doStructs.append(('do', loopLabel, len(self.correctedLines))) # keep track of where this line is
            
            # Track 'end do' statements
            endDo = re.match(r'^\s*\bend\s+do\b', line, re.IGNORECASE)
            if endDo:
                try:
                    currentLoop = doStructs.pop()
                except:
                    print('error handling endDo loop at line ' + str(len(self.correctedLines)))
            # Track 'continue' statements and possibly replace them.
            continueStatement = re.match(r'^\s*(\d+)\s*continue', line, re.IGNORECASE)
            if continueStatement:
                continueLabel = continueStatement[1]
                if continueLabel in gotoStructs or len(doStructs) == 0:
                    self.correctedLines.append(line)
                while len(doStructs) > 0 and doStructs[-1][1] == continueLabel:
                    doLine = doStructs[-1][2]
                    doStructs.pop()
                    self.correctedLines[doLine] = self.correctedLines[doLine].replace(f' {continueLabel}', '', 1)
                    if len(doStructs) > 0 and doStructs[-1][1] == continueLabel:
                        self.correctedLines.append('  end do\n')
                    else:
                        self.correctedLines.append(f'  end do ! loop {continueLabel}\n')
                self.i += 1
                continue
            self.correctedLines.append(line)
            self.i += 1

    # A few one-liners that are useful for parsing source code
    def has_continuation(self, line):
        return re.match(r'^[^!&\n]*&', line)
    def is_declaration(self, line):
        has_reserved = re.match(r'^\s*(real|integer|logical|complex|character)(?!\s*function)', line, re.IGNORECASE)
        if not has_reserved:
            return False
        # Check if previous line is continuation line
        is_cont = self.has_continuation(self.lines[self.i-1])
        if is_cont:
            return False
        return True
    def is_blank(self, line):
        return re.match(r'^\s*$', line)
    def is_comment(self, line):
        return re.match(r'^\s*!.*$', line,)
    def is_multiline_implicit(self, line):
        return re.match(r'^\s*implicit\s+real[^!&]*&', line, re.IGNORECASE)

    # Fixes implicit statements declared over two lines. This is necessary to prevent conflict
    # with the multiline declaration fixer.
    def fix_multiline_implicit(self):
        line1 = self.lines[self.i]
        a1, c1 = self.split_line_comment(line1)
        line2 = self.lines[self.i+1]
        a2, c2 = self.split_line_comment(line2)
        if not re.match(r'^\s*integer\s*\(\s*i\s*-\s*n\s*\)', a2, re.IGNORECASE):
            self.throw_error('unprocessable implicit statement')
        a1 = a1.replace('&\n','')
        if len(c2) > 0:
            c1 = c1+c2
        new_statement = f'{a1}{a2}{c1}'.replace('\n', '')
        self.correctedLines.append(f'{new_statement}\n')
        self.i += 2


    # Split a line into it's pre-comment and post-comment components. Be careful of string literals!
    def split_line_comment(self, line):
        sq = [q.start() for q in re.finditer(r'\'', line)]
        dq = [q.start() for q in re.finditer(r'\"', line)]
        ex = [x.start() for x in re.finditer(r'!', line)]
        ex_index = None
        for x in ex:
            sq_before = len([q for q in sq if q < x])
            dq_before = len([q for q in dq if q < x])
            if sq_before % 2 == 0 and dq_before % 2 == 0:
                ex_index = x
                break
        if ex_index is None:
            return line, ''
        else:
            return line[0:ex_index], line[ex_index:]

    # Detailed function for splitting up multiline declaration / assignment
    def fix_declaration(self):
        assignment_lines = []
        comment_lines = []
        pure_comment_lines = [] # lines which contain ONLY a comment
        continuing = True
        while continuing:
            line = self.lines[self.i]
            if self.is_blank(line):
                pass
            elif self.is_comment(line):
                pure_comment_lines.append(line)
            else :
                a, c = self.split_line_comment(line)
                c = c.strip(' \t\n')
                assignment_lines.append(a.strip(' \t\n&'))
                comment_lines.append(c if len(c) > 0 else '!<')
                if not self.has_continuation(line):
                    continuing = False
            self.i += 1

        full_statement = ''.join(assignment_lines)
        full_statement = re.match(r'^([^!&]*)::\s*(.*)$', full_statement)
        if not full_statement:
            print(''.join(assignment_lines))
        params = full_statement[1]
        variables = full_statement[2]
        if not params or not variables:
            self.throw_error('params or variables is blank')
        split_vars = self.split_variables(variables)
        comment_lines.extend(['!<']*(len(split_vars)-len(comment_lines)))
        declarations = [' '.join([i,j]) for i,j in zip(split_vars, comment_lines)]
        self.correctedLines.extend(pure_comment_lines)
        for dec in declarations:
            self.correctedLines.append(f'{params}:: {dec}\n')

    # Short helper function for error throwing
    def throw_error(self, err):
        print(f'Error at line {str(self.i)}: {err}')
        sys.exit(1)

    # Splits a line of variable declarations.
    def split_variables(self, vars):
        par_depth = 0 # depth of parentheses we're in
        indices = []
        for i, char in enumerate(vars):
            if char == '(':
                par_depth += 1
            elif char == ')':
                par_depth -= 1
            elif char == ',' and par_depth == 0:
                indices.append(i)
        slices = zip([-1]+indices, indices+[len(vars)])
        split_vars = [vars[i+1:j].strip(' ') for i,j in slices]
        return split_vars

