'''
This tool is used to make the conversion from fixed-form to free-form Fortran.
Code adapted from jgoppert, and only a few functions have been modified
from the original works.

Created on Jul 15, 2012
@author: jgoppert
github of original author: https://github.com/jgoppert
'''

import re

class Converter(object):

    #misc regex's
    re_line_continuation = re.compile('^(\s\s\s\s\s)([^\s])([\s]*)')
    re_hollerith = re.compile('[\s,]((\d+)H)')
    re_number_spacing = re.compile('([^A-Za-z][\d.]+)[\s]([\d.]+[^A-Za-z])')
    re_f77_comment = re.compile('^[cC*]')
    re_f90_comment = re.compile('^\s*!')

    #variable regex
    re_var_end = re.compile('.*[A-Za-z0-9]+$')
    re_var_begin = re.compile('(^\s\s\s\s\s)([^\s])([\s]*)([A-Za-z]+[0-9]?)+')

    #number regex
    re_number_end = re.compile('.*[\d.eE]+$')
    re_number_begin = re.compile('(^\s\s\s\s\s)([^\s])([\s]*)[\d.eE]+')

    #exponent regex
    re_exponent_old = re.compile('([\d.]E)[\s]([\d]+)')

    def __init__(self):
        print('Converter initialized')

    def process(self, source):

        #fix source code
        for i in range(len(source)):
            line = source[i].replace('\t','   ')
            line = self.remove_new_line(line)
            line = self.fix_comment(line)
            line = self.fix_exponents(line)

            if self.is_line_continuation(line):
                prev_line_index = self.find_prev_line(source,i)
                prev_line = source[prev_line_index]
                while self.is_preprocessor_directive(prev_line) and prev_line_index > 0:
                    prev_line_index -= 1
                    prev_line = source[prev_line_index]

                line,prev_line = self.fix_line_continuation(line,prev_line)
                source[prev_line_index] = prev_line

            line = self.fix_number_spacing(line)
            source[i] = line

        # return new source code
        for i, line in enumerate(source):
            source[i] = line+'\n'
        return source

    def is_line_continuation(self, line):
        if len(line) > 5 and self.re_line_continuation.match(line):
            return True
        return False
    
    def is_preprocessor_directive(self, line):
        if re.match(r'^\s*#', line):
            return True
        return False

    def find_prev_line(self, source, i):
        #find first non-commented previous line
        if i>0:
            for j in range(i-1, -1, -1):
                if not self.is_f90_comment(source[j]):
                    return j
        return None

    def remove_new_line(self, line):
        return re.sub(r'[\r\n]','', line)

    def is_f77_comment(self, line):
        return self.re_f77_comment.match(line)

    def is_f90_comment(self, line):
        return self.re_f90_comment.match(line)

    def fix_comment(self, line):
        if self.is_f77_comment(line):
            line = re.sub('^[Cc*]','!', line, count=1)
        return line

    def fix_line_continuation(self, line, prev_line):
        continuation_type = self.find_continuation_type(line,prev_line)

        #if no line continuation found, return
        if not continuation_type:
            return (line,prev_line)

        #add appropriate continuation to current line
        if continuation_type == "hollerith":
            line = self.re_line_continuation.sub('\g<1>&\g<3>', line, count=1)
        elif continuation_type == "var" or continuation_type == "number" :
            line = self.re_line_continuation.sub('\g<1>\g<3>&', line, count=1)
        elif continuation_type == "generic":
            line = self.re_line_continuation.sub('\g<1>&\g<3>', line, count=1)
        else:
            raise Exception("unknown continuation type")

        #add line continuation to previous line
        if re.match(r'^([^!\n]*)!.*$', prev_line, re.IGNORECASE):
            prev_line = re.sub(r'^([^!\n]*)(!.*)$', r'\1& \2', prev_line)
        else:
            prev_line = prev_line + '&'

        return (line,prev_line)

    def fix_exponents(self, line):
        return self.re_exponent_old.sub('\g<1>+\g<2>', line)

    def fix_number_spacing(self, line):
        return self.re_number_spacing.sub('\g<1>\g<2>', line)

    def find_continuation_type(self, line, prev_line):
        '''
        Finds line continuation type: hollerith, number, or generic
        if not a line continuation, return None
        '''
        if not self.is_line_continuation(line):
            return None

        # test for hollerith continuation
        for hollerith in self.re_hollerith.finditer(prev_line):
            hollerith_end = hollerith.end(1)+int(hollerith.group(2))+1
            prev_line_length = len(prev_line)-1
            if (hollerith_end > prev_line_length):
                return "hollerith"

        #var continuation
        if self.re_var_begin.match(line) and self.re_var_end.match(prev_line):
            return "var"

        #number continuation
        if self.re_number_begin.match(line) and self.re_number_end.match(prev_line):
            return "number"

        return "generic"
