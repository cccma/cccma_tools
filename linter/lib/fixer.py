'''
Code standardization tools are invoked from here.
'''

import sys
import os
import re
import shutil
from lib.fixer_tools.converter import *
from lib.fixer_tools.decapitalizer import *
from lib.fixer_tools.prettifier import *
from lib.fixer_tools.structure import *
from lib.fixer_tools.whitespace import *


# Runs the fixer over the specified files to enforce coding standards.
# Creates a backup of the original file in-place.
def run_fixer(files, debug=False, rename=False):
    lines = []
    converter = Converter()
    decapitalizer = Decapitalizer()
    prettifier = Prettifier()
    st_analyzer = StructuralAnalyzer()
    ws_correcter = WhitespaceChecker()
    for ifile in files:
        print(f'Linting {ifile}...')
        create_backup(ifile)
        with open(ifile, 'r') as f:
            lines = f.readlines()

        if is_fixed_format(lines):
            # run converter if file is Fortran 77 format
            print('  Converting to fixed format')
            lines = converter.process(lines)
            if debug:
                with open('after_converter.f90', 'w') as f:
                    for line in lines:
                        f.write(line)
        # run decapitalizer (assists prettifier to identify rule violations)
        print('  Decapitalizing file')
        lines = decapitalizer.process(lines)
        if debug:
            with open('after_decap.f90', 'w') as f:
                for line in lines:
                    f.write(line)

        # run prettifier (many miscellaneous rules)
        print('  Prettifying expressions')
        lines = prettifier.process(lines)
        if debug:
            with open('after_pretty.f90', 'w') as f:
                for line in lines:
                    f.write(line)

        # run structural analyzer (implicit none, unterminated loops)
        print('  Analyzing structure')
        lines = st_analyzer.process(lines)
        if debug:
            with open('after_structure.f90', 'w') as f:
                for line in lines:
                    f.write(line)

        # run prettifier (many miscellaneous rules)
        print('  Second prettifying run')
        lines = prettifier.process(lines)
        if debug:
            with open('after_pretty2.f90', 'w') as f:
                for line in lines:
                    f.write(line)

        # run whitespace checker (alignment)
        print('  Correcting whitespace')
        lines = ws_correcter.process(lines)
        if debug:
            with open('after_ws.f90', 'w') as f:
                for line in lines:
                    f.write(line)

        if rename and ('.f' in ifile or '.F' in ifile):
            ofile = re.sub(r'(.*)\.(.*)', r'\1.f90', ifile)
        else:
            ofile = ifile
        with open(ofile, 'w') as f:
            for line in lines:
                f.write(line)

        critical = st_analyzer.comments                 # for critical comments from structural analyzer
        uncorrected = prettifier.uncorrected_comments  # for uncorrected line comments in the prettifier
        corrected = prettifier.corrected_comments      # for corrected line comments in the prettifier

        # write all activity to the .f90.comments file (if the file doesn't already exist)
        if '/' in ifile:
            comment_name = re.sub(r'(.*\/)([^\/]*)', r'\1backups/\2.comments', ifile)
        else:
            comment_name = f'backups/{ifile}.comments'
        if os.path.isfile(comment_name) or os.path.isfile(re.sub(r'(.*\/)(.*)', r'\1backups/\2.comments', ifile[:-2])):
            pass
        else:
            with open(comment_name, "w") as f:
                f.write("=========================== CRITICAL ERRORS ===========================\n\n")
                for line in critical:
                    f.write(line)
                    f.write('\n')
                if len(critical) == 0:
                    f.write('\nNo critical errors found\n')
                f.write('\n')
                f.write("====================== Uncorrected Line Comments ======================\n\n")
                for line in uncorrected:
                    f.write(line[1])
                    f.write('\n')
                f.write("\n\n======================= Corrected Line Comments =======================\n\n")
                for line in corrected:
                    f.write(line[1])
                    f.write('\n')
    print("Linting completed successfully")

# Helper function for directory creation
def create_backup(ifile):
    path = ''
    directory = re.match(r'(.*)(?=\/)', ifile)
    if not directory:
        path += './backups'
    else:
        path += directory.group(1)
        path += '/backups'
    try:
        os.mkdir(path)
    except:
        pass
    if '/' in ifile:
        dest = re.sub(r'(.*\/)([^\/]*)', r'\1backups/\2', ifile)
    else:
        dest = f'backups/{ifile}'
    if os.path.isfile(dest):
        pass
    else:
        try:
            shutil.copyfile(ifile, dest)
        except Exception as e:
            print(e)
            pass

def is_fixed_format(lines):
    for line in lines:
        if re.match(r'^C', line, re.IGNORECASE):
            return True
    return False