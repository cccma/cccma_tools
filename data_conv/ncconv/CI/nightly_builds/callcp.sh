#!/bin/bash
#PBS -S /bin/bash
#PBS -q development
#PBS -j oe
#PBS -l walltime=02:30:00
#PBS -l select=1:ncpus=1:mem=50gb:res_image=eccc/eccc_all_ppp_ubuntu-18.04-amd64_latest
# 
#   This callcp.sh is a modified version to be ran in batch mode.
#   This script steps through a yearly chunk of all tables, as laid out by the
#   variables below. The output is automatically stored on the RUNPATH of the
#   user account running the gitlab runner.
#
#   It is expected that this script gets submitted with 
#
#   jobsub -c MACH --cwd callcp.sh -- -v "WRK_DIR=path2/dir,runid=myrun,user=user,hall=hallN,chunk=YYYY_YYYY,TABGRP_OPTS1=$optlst1,TABGRP_OPTS2=$optlst2"\
#                           -N jobname -o outputfile
#
#   Note: --cwd just makes the stderr/stdout be output to the working directory. The script still STARTS in your home dir

# get cccma conda and activate pycmor environment
export PATH=/home/ords/crd/ccrn/scrd102/ccc_conda/4.6.14/miniconda2/bin:$PATH
source activate py2_cmor_v1

cd $WRK_DIR
source path_export.sh                                       # get required pycmor variables
source /home/scrd101/generic/sc_cccma_setup_profile         # bring in cccma environment (required for optional decks and access)
source ${NCCONV_DIR}/CI/nightly_builds/nb_config.sh         # get nightly build configs specifically
echo "Converting data for ${user}'s $runid, chunk: $chunk"

# override the HALLN given by path_export.sh as data may be stored on cross mounted sitestore
HALLN=${hall//hall}

# set other pertinent vars
nemo_mask="nemo_mesh_mask_rc3.nc"
vtabdir=$NCCONV_DIR/tables/variable_tables
outpath=$NB_STRG_DIR
version='nightly-build'
tabgrp1='3hr 6hrLev 6hrPlev 6hrPlevPt AERday AERhr AERmon AERmonZ Amon CF3hr CFday'
tabgrp2='CFmon CFsubhr E1hr E3hr E3hrPt Eday EdayZ Efx Emon EmonZ Eyr IfxAnt IfxGre ImonAnt ImonGre LImon Lmon'
tabgrp3='Oclim Oday Ofx Omon Oyr SIday SImon day fx E1hrClimMon Esubhr'

# Set up additional environment considerations
export DATAPATH_DB=/space/${hall}/sitestore/eccc/crd/ccrn/users/$user/$runid/datapath_local.db:/space/${hall}/sitestore/eccc/crd/ppp_data/database_dir/datapath_ppp${HALLN}.db

pycmor.py $TABGRP_OPTS1 -m $nemo_mask -r $runid -t "$tabgrp1" -d $vtabdir -k $chunk -o $outpath -v $version >> ${runid}_${chunk}_tabgrp1.log 2>&1
pycmor.py $TABGRP_OPTS2 -m $nemo_mask -r $runid -t "$tabgrp2" -d $vtabdir -k $chunk -o $outpath -v $version >> ${runid}_${chunk}_tabgrp2.log 2>&1
pycmor.py $TABGRP_OPTS2 -m $nemo_mask -r $runid -t "$tabgrp3" -d $vtabdir -k $chunk -o $outpath -v $version >> ${runid}_${chunk}_tabgrp3.log 2>&1
