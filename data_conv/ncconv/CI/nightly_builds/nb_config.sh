# Nightly build config file

#============================
# Storage directory locations
#============================
export HDNODE=ppp3
export RUNPATH=/space/hall3/sitestore/eccc/crd/ccrn/users/$(whoami)
export NB_STRG_DIR=${RUNPATH}/pycmor_nightly_build  # Storage directory for ALL builds
export BLD_DIR=${NB_STRG_DIR}/$(date +%Y-%m-%d)     # Build specific storage directory
export NB_SCRPT_DIR=CI/nightly_builds               # Location of nighlty build scripts, with in the ncconv repo

#=============
# git settings
#=============
export NB_BRANCH=nightly-builds  
export NCCONV_REPO=git@gitlab.science.gc.ca:CanESM/ncconv.git               # Remote origin to push to

#==================
# validate settings
#==================
export VALIDATE_REPO=git@gitlab.science.gc.ca:ncs001/validate.git
export VALIDATE_BRANCH=master
export VALIDATE_DIR=${NB_STRG_DIR}/validate
export VALIDATE_VARLST=/home/rja001/netcdf_conversion/validation_lists/verification_variables_piControl_amip_1pctCO2_abrupt-4xCO2_historical_priority123_dreq01.00.29_02May2019.pkl
export VALIDATE_WRKDIR=${NB_STRG_DIR}/validate-wrk
export VALIDATE_PLTDIR=/home/scrd104/public_html/pycmor_nightly_builds
export VALIDATE_DATADIR=${NB_STRG_DIR}/CMIP6/CMIP/CCCma/CanESM5/historical
