#!/usr/bin/env python
'''
Compare different versions of netcdf files.
Versions are identified using the version string (at end of the CMIP6 dir path).

User identifies files to be compared by setting parameters in an input file, compare_output.json.
An example of this input file is in tools/.
Copy this file to a working dir and edit as needed.

The script will loop over tables.
Use cmor_vars arg to restrict the check to subsets of variables within each table.

This tool is useful to check for changes in netcdf files when modifying pycmor.
When doing offline conversions, run it in the working dir after the conversion, e.g.
  ./callcp.sh
  ./compare_output.py
It simply diffs the ncdump of two or more netcdf files, up to a specified number of lines.
It can display the diff selectively, and filter out metadata parameters that have
uninteresting differences (e.g. creation_date, history, ...).
'''
###############################################################################
import os
import subprocess
import datetime
import re
import json
###############################################################################

display_diff = 'whole diff'
display_diff = 'selective diff'

ext = 'nc'  # comparison will be done on filenames with this extension

lines = 50000

path0 = 'output'

write_diffs_to_file = True

# This path_template assumes CMIP6, but this should be project-specific - fix later!
path_template = '{mip_era}/{activity_drs}/{institution_id}/{source_id}/{experiment_id}/{member_id}/{table_id}/{variable_id}/{grid_label}/{version}'

# Default parameter values for all datasets 
# (will be overridden by any values specified in the input file)
base = {   # these are CMIP6-specific parameters, instead should set by project (from path_template?) - fix later!
        'mip_era' : 'CMIP6', 
        'institution_id' : 'CCCma', 'source_id' : 'CanESM5',
        'grid_label' : 'gn', 'version' : 'v20190429',
        'table_id' : 'day', 'variable_id' : 'zg',
        'activity_drs' : 'CMIP', 
        'experiment_id' : 'piControl', 
        'member_id' : 'r1i1p1f1', 
       }

# Get input from user file
user_input = 'compare_output.json'
with open(user_input, 'r') as f:
    args = json.load(f)

versions = args['versions']
cmor_tables = args['cmor_tables']
cmor_vars = {}
if 'cmor_vars' in args:
    cmor_vars = args['cmor_vars']
for k in base:
    if k in args:
        base[k] = args[k]

###############################################################################
version_ref = versions[0]  # this defines the reference version, to which other versions will be compared

# Get info from CMOR tables
paths = {
    'ncconv' : '..'  # path to ncconv repo (this is the dir ncconv was cloned into)
}
project = base['mip_era']

paths['project'] = os.path.join(paths['ncconv'], 'projects', project)

# Path to the project's official CMOR tables
paths['cmor_tables'] = os.path.join(paths['project'], 'cmor-tables')

# Specify the naming conventions for the table json files
filename_template = {
    'cmor_tables' : '{project}_{cmor_table}.json',
}

datasets = []
for cmor_table in cmor_tables:
    params = {'project' : project, 'cmor_table' : cmor_table}  # example: project = 'CMIP6', cmor_table = 'Amon'

    # Get CMOR variable names from the project's official CMOR table json file.
    # These are the variable names that are valid within that project (e.g., in CMIP6, 'pr' is precipitation).
    # They are the variable names used by the project's data request.
    filename = filename_template['cmor_tables'].format(**params)  # example: CMIP6_Amon.json
    filepath = os.path.join(paths['cmor_tables'], filename)
    with open(filepath, 'r') as f:
        d = json.load(f)
    cmor_var_info = d['variable_entry']  # dictionary containing the metadata for all CMOR variables in this table
    print('Loaded official table {} containing {} variables'.format(filepath, len(cmor_var_info)))
    del d
    if cmor_table in cmor_vars:
        # Use only a subset of the available variables
        use_vars = cmor_vars[cmor_table]
    else:
        # Use all available variables in the CMOR table
        use_vars = list(cmor_var_info.keys())
    use_vars.sort(key=str.lower) # ensure variables in CCCma table json file will appear in alphabetical order

    for cmor_var in use_vars:
        datasets.append({'table_id' : cmor_table, 'variable_id' : cmor_var})


###############################################################################

tmp_dir = './tmp_compare'
if not os.path.exists(tmp_dir): os.makedirs(tmp_dir)

def ncdump(path, filename, version):
    filepath = os.path.join(path, filename)
    output_file = 'ncdump__{}__{}'.format(version, filename)
    cmd = 'ncdump {} | head -{} > {}'.format(filepath, lines, os.path.join(tmp_dir, output_file))
    os.system(cmd)
    return output_file

if write_diffs_to_file:
    diffs_filepath = os.path.join(tmp_dir, 'diffs')
    w = 'netcdf file diffs, {}\n'.format(datetime.datetime.now())
    w += 'reference version:  {}\n'.format(version_ref)
    w += 'comparing to {} other versions:\n'.format(len(versions)-1)
    for version in versions:
        if version == version_ref: continue
        w += '  {}\n'.format(version)
    w += 'for {} datasets:\n'.format(len(datasets))
    template = '{table_id}_{variable_id}'
    for dataset in datasets:
        w += '  {}\n'.format(template.format(**dataset))
    w += '\n'
    with open(diffs_filepath, 'w') as f:
        f.write(w)

dataset_diffs = {}
dataset_ids = []
file_count = {}

# loop over output variables
for dataset in datasets:
    # create a version-independent id for the dataset, dataset_id, for keeping track of the comparison results
    params = dict(dataset)
    for k,v in base.items(): params.setdefault(k,v)
    params_id = dict(params)
    params_id['version'] = ''
    dataset_id = path_template.format(**params_id)

    print('\nchecking dataset: ' + dataset_id)
    print('comparing versions: ' + ', '.join(versions))

    # get paths & filenames for all versions
    files, paths = {}, {}
    for version in versions:
        params = dict(dataset)
        params['version'] = version
        for k,v in base.items(): params.setdefault(k,v)
        path = os.path.join(path0, path_template.format(**params))
        if os.path.exists(path):
            paths[version] = path  # path to this version of the dataset
            files[version] = [s for s in os.listdir(path) if s.endswith(os.path.extsep + ext)]  # filenames of all netcdf files in this version of the dataset
        else:
            print('path not found: ' + path)
            continue

    # confirm that path to reference version was found
    params['version'] = version_ref
    path = os.path.join(path0, path_template.format(**params))
    if version_ref not in paths:
        # skip all comparisons if the reference version wasn't found
        print('skipping comparison against reference dataset: ' + path)
        continue
    assert os.path.exists(paths[version_ref]), 'reference version not found: ' + path

    dataset_ids.append(dataset_id)
    dataset_diffs[dataset_id] = {version : 0 for version in versions}
    file_count[dataset_id] = {version : 0 for version in versions}

    # for each netcdf file in the reference dataset, compare it to the corresponding netcdf file from the other version(s) of the dataset
    for filename in files[version_ref]:
        path = paths[version_ref]
        stat0 = os.stat(os.path.join(path, filename))
        file_count[dataset_id][version_ref] += 1
        output_file0 = ncdump(path, filename, version_ref)
        for version in versions[1:]:
            params['version'] = version
            if version not in files:
                # print('no files found for version {}'.format(path_template.format(**params)))
                print('no files found for version: ' + version)
                file_count[dataset_id][version] = 'no files found'
                continue
            if filename not in files[version]:
                print('{} missing for version = {}'.format(filename, version))
                continue

            path = paths[version]
            stat = os.stat(os.path.join(path, filename))
            file_count[dataset_id][version] += 1
            # print('comparing {} for versions = {}, {} '.format(filename, version_ref, version))
            print('comparing {} for versions:'.format(filename))
            print('  version = {} (timestamp: {})'.format(version_ref, datetime.datetime.fromtimestamp(stat0.st_mtime)))
            print('  version = {} (timestamp: {})'.format(version,  datetime.datetime.fromtimestamp(stat.st_mtime)))
            print('  (current system time: {})'.format(datetime.datetime.now()))
            print('at paths:')
            print('  ' + paths[version_ref])
            print('  ' + path)
            output_file = ncdump(path, filename, version)

            # compare the two output files
            cmd = 'diff {} {}'.format(os.path.join(tmp_dir, output_file0), os.path.join(tmp_dir, output_file))
            proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
            stdout, stderr = proc.communicate()
            diff = stdout.decode() # converts b to str
            l = diff.split('\n')

            if write_diffs_to_file:
                w = '{}\n'.format(dataset_id)
                w += '{} vs. {}\n'.format(version_ref, version)
                w += diff + '\n'
                with open(diffs_filepath, 'a') as f:
                    f.write(w)

            if display_diff == 'whole diff':
                # Just show the whole diff
                print(diff)
                if len(diff) > 0:
                    dataset_diffs[dataset_id][version] += 1

            elif display_diff == 'selective diff':
                # Selectively show the diff, ignoring some metadata parameters (e.g. history, creation_date, ...).
                # ok_metadata_diffs specifies the metadata parameters whose differences that will be ignored.

                ok_metadata_diffs = []
                ok_metadata_diffs.append('{}:history ='.format(dataset['variable_id']))
                ok_metadata_diffs.append('creation_date =')
                ok_metadata_diffs.append('history =')
                ok_metadata_diffs.append('tracking_id =')
                ok_metadata_diffs.append('version =')
                ok_metadata_diffs.append('CCCma_pycmor_hash =')

                patt = '\d+c\d+\n'  # e.g. '59c59\n'
                line_diffs = re.split(patt, diff)
                line_diffs = [s for s in line_diffs if s not in ['']]
                line_nums = re.findall(patt, diff)
                assert len(line_nums) == len(line_diffs)
                ok = set()
                sep_diff = '---'
                for k, line_diff in enumerate(line_diffs):
                    ld = line_diff.split(sep_diff)
                    ld = [s.strip() for s in ld]
                    if len(ld) != 2: continue
                    if not( ld[0].startswith('<') and ld[1].startswith('>') ): continue
                    ld[0] = ld[0].strip('<').strip().strip('\t').strip(':')
                    ld[1] = ld[1].strip('>').strip().strip('\t').strip(':')
                    for s0 in ok_metadata_diffs:
                        if ld[0].startswith(s0) and ld[1].startswith(s0):
                            ok.add(k)
                            break
                count = 0
                for k, line_diff in enumerate(line_diffs):
                    if k in ok:
                        continue
                    else:
                        print(line_nums[k].strip())
                        print(line_diff)
                        count += 1
                print('number of unexpected line differences: {}'.format(count))
                dataset_diffs[dataset_id][version] += count

if len(dataset_diffs) > 0:
    print('\nOverall number of diffs from reference version ({}):'.format(version_ref))
    total_diffs = {version : 0 for version in versions[1:]}
    total_file_count_errors = {version : 0 for version in versions[1:]}
    for dataset_id in dataset_ids:
        print('  {}'.format(dataset_id))
        for version in versions:
            if version == version_ref: continue
            print('    {} differences: {}'.format(version, dataset_diffs[dataset_id][version]))
            total_diffs[version] += dataset_diffs[dataset_id][version]
            # also report if there were differences in the number of files found
            if file_count[dataset_id][version] != file_count[dataset_id][version_ref]:
                print('    {} file count: {} ({} file count: {})'.format(version, file_count[dataset_id][version], \
                                                                        version_ref, file_count[dataset_id][version_ref]))
                total_file_count_errors[version] += 1
    print('\nSummary:')
    print('  Total dataset diffs, by version:')
    for version in total_diffs:
        print('    {}: {}'.format(version, total_diffs[version]))
    print('  Total file count errors, by version:')
    for version in total_file_count_errors:
        print('    {}: {}'.format(version, total_file_count_errors[version]))

