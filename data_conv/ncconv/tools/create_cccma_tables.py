#!/usr/bin/env python
'''
This script creates a new set of CCCma variable tables for a project (e.g., CMIP6).

CCCma variable tables provide the map between a project's requested variables and 
CCCma model output. Requested variables are defined by the CMOR tables that are 
provided by the project. For example, the CMOR tables provided by CMIP6 are the
json files available at:
    https://github.com/PCMDI/cmip6-cmor-tables

The two kinds of tables, both of which are keyed by CMOR variable names, are
for brevity referred to as "CMOR tables" and "CCCma tables". They are stored
in separate subdirectories of each project folder.
'''
###############################################################################
import os
import json
from collections import OrderedDict
import argparse
import json
import sys
###############################################################################
# Get user input parameters

user_input = 'create_cccma_tables.json'  # default name of user input file
mismatch_warnings_logfile = 'mismatch_warnings.txt'

msg = '''Tool to generate CCCma variable tables, which map between requested variables (specified by project CMOR tables) and CCCma model output.

EXAMPLES:
  ./create_cccma_tables.py -p CMIP6 -t Amon day Omon -s CanESM5
  ./create_cccma_tables.py -ui create_cccma_tables.json -c

Should be ok to run in any python environment since only standard library modules are used.  
'''
parser = argparse.ArgumentParser(description=msg, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-ui', '--user_input', type=str, default=user_input, help=\
                    f'Path to file with input parameters. Example file: tools/{user_input} (copy this file and edit as needed). Command-line options override these parameters. Some optional input parameters can only be set via this file, not by command-line options (see example file and documentation for further details).')
parser.add_argument('-p', '--project', type=str, help=\
                    'Project name (required), used to identify CMOR tables that specify the characteristics of converted variables. Example: \'CMIP6\'')
parser.add_argument('-s', '--source_id', type=str, help=\
                    'Model name (required) for which CCCma tables will be created. Example: \'CanESM5\'')
parser.add_argument('-t', '--cmor_tables', nargs='+', type=str, help=\
                    'Whitespace-separated list of tables to create. Examples: \'Amon\', \'SImon Omon\'. If not given, CCCma tables corresponding to all project CMOR tables are created. Optionally can specify in user input file (-ui option) subsets of variables by table, otherwise all variables in a table are included.')
parser.add_argument('-c', '--clobber', default=False, action='store_true', help=\
                    'Automatically replace any existing CCCma tables files (otherwise user is given option to either replace existing files or abort).')
parser.add_argument('-pct', '--previous_cccma_tables', type=str, help=\
                   'To import information from a previous project\'s set of CCCma tables into the new tables, give name of the previous project (see documentation for further details). Example: \'CMIP6\'')
cmd_line_args = parser.parse_args()

warn_about_overwrite = not cmd_line_args.clobber  # warn if we're about to overwrite existing CCCma tables

# Get values of input arguments. In order of precedence:
# 1. defaults
# 2. user input file (if it exists)
# 3. command-line input (if given)
# e.g. giving a parameter by command-line overrides the user input file
args = { # default values (None ==> no default, user must specify a value)
    'project' : None,
    'source_id' : None,
    'cmor_tables' : ['*'], # default behaviour is to use all of a project's tables
    'cmor_vars' : {}, # provides the option to include only a subset of variables, otherwise all variables are included
    # Options pertaining to importing info from previous CCCma tables:
    'previous_cccma_tables' : '',
    'pct_source_id' : '',
    'pct_path_template' : '',
    'pct_filename_template' : '{project}-CCCma_{cmor_table}.json',
    'pct_exclude_parameters' : [],
    'pct_tables' : {},
    'pct_check_mismatch' : [],
}
get_args = list(args.keys())
# Get values from user input file
user_input = cmd_line_args.user_input
if os.path.exists(user_input):
    with open(user_input, 'r') as f:
        args.update({k:v for k,v in json.load(f).items() if k in get_args})
        print('\nUser input loaded from {}'.format(user_input))
else:
    print('No user input file file, options will be set by command-line input only')
del user_input

# Get command-line input
args.update({k:v for k,v in cmd_line_args.__dict__.items() if v and k in get_args})

# Render input parameters as local variables
locals().update(args)
del args, cmd_line_args

assert project, 'project is required, found: {}'.format(project)
assert source_id, 'source_id is required, found: {}'.format(source_id)

###############################################################################
# Setup

# Specify json file that defines CCCma table parameters and their default values.
# Note:
# - all parameters to be included in the output must have a default value set in defaults json file.
# - the order of parameters in the json file defines the order in which they'll appear in the CCCma table output file
# - parameters only need to be included in the output file if their values differ from the defaults
#   (this helps keep the CCCma tables concise, hence more easily readable)
# - if cccma_table_defaults is changed then it should be updated in table_utils.py as well
cccma_table_defaults = 'cccma_table_defaults_{project}.json'.format(project=project)

if 'NCCONV_DIR' not in os.environ:
    msg = '\n\nMust set shell env variable NCCONV_DIR giving path to ncconv repo (the top-level dir of the repo).'
    msg += '\nIf a working dir with path_export.sh exists, NCCONV_DIR can be set by sourcing it. Relative to the project dir:'
    msg += '\n  source ../../netcdfconv/path_export.sh'
    msg += '\nAlternately, in the shell simply do (relative to the project dir):'
    msg += '\n  export NCCONV_DIR=../.. \n'
    raise Exception(msg)


paths = {
    'ncconv' : os.environ['NCCONV_DIR'],  # path to ncconv repo (this is the dir ncconv was cloned into)
}

paths['project'] = os.path.join(paths['ncconv'], 'projects', project)
paths['project'] = os.path.relpath(paths['project'])  # use path relative to current dir (simplifies the stdout shown to user)

# Path to the project's official CMOR tables
paths['cmor_tables'] = os.path.join(paths['project'], 'cmor-tables')

# Get default variable parameters for CCCma tables
paths['cccma_tables'] = os.path.join(paths['project'], 'cccma-tables', source_id)

# Specify the naming conventions for the table json files
filename_template = {
    'cmor_tables' : '{project}_{cmor_table}.json',

    'cccma_tables' : '{project}-CCCma_{cmor_table}.json',
    # This naming convention for 'cccma_tables' copies Dave's naming convention in the branch
    # he developed for CCMI-2022 conversions:
    #   https://gitlab.science.gc.ca/rdp001/ncconv/commit/bf2ccb58d7cf1bb95dd3bb7356e792cbb5719f71
}

def ask_user(msg):
    print(msg)
    response = input('("y" or Enter for yes, any other key to exit without creating any tables)  ')
    if response.lower() not in ['y', '']:
        print('Exiting')
        sys.exit()

print('Project: ' + project)
print('Model: ' + source_id)
print('Project directory: ' + os.path.abspath(paths['project']))

if len(previous_cccma_tables) > 0:
    # Option to bring in info from previously defined CCCma tables.
    # Use this option to clean up CCCma tables without losing essential info from the previous version.
    # Or, to start with a completely clean slate in defining CCCma tables, don't use this option.
    pct_project = previous_cccma_tables
    del previous_cccma_tables
    if pct_source_id == '':
        pct_source_id = source_id
    if pct_path_template == '':
        pct_path_template = os.path.join(paths['ncconv'], 'projects', '{project}/cccma-tables/{source_id}')
    params = {'project' : pct_project, 'source_id' : pct_source_id}
    paths['previous_cccma_tables'] = pct_path_template.format(**params)
    filename_template['previous_cccma_tables'] = pct_filename_template
    del pct_path_template, pct_filename_template, params

    propagate_previous_cccma_tables = os.path.exists(paths['previous_cccma_tables'])
    if propagate_previous_cccma_tables:
        print('\nInformation will be imported into new CCCma tables from previous CCCma tables in:') 
        print('  ' + paths['previous_cccma_tables'])
        if len(pct_tables) > 0:
            print('New tables will import information from the previous tables:')
            col0, col1 = 'New table:', 'Previous table:'
            m0 = max([len(s) for s in list(pct_tables.keys()) + [col0]])
            m1 = max([len(s) for s in list(pct_tables.values()) + [col1]])
            fmt0, fmt1 = '%-{}s'.format(m0), '%-{}s'.format(m1)
            print('  {}  {}'.format(fmt0 % col0, fmt1 % col1))
            print('  {}  {}'.format('-'*m0, '-'*m1))
            for cmor_table, previous_cmor_table in pct_tables.items():
                print('  {}  {}'.format(fmt0 % cmor_table, fmt1 % previous_cmor_table))
            print('  (For any others, the names of new and previous tables must match)')
        if len(pct_exclude_parameters) > 0:
            print('The following parameters in the previous CCCma tables will NOT be imported:')
            for p in pct_exclude_parameters:
                print('  ' + p)
        if len(pct_check_mismatch) > 0:
            print('Will check for mismatches between same-named parameters in CMOR tables and previous CCCma tables.')
            print(f'Warnings will be written to logfile {mismatch_warnings_logfile}')
            print('CMOR table parameters found in previous CCCma tables will be excluded from the new CCCma tables.')
    else:
        ask_user('\nPath {path} for previous CCCma tables not found, continue?'.format(path=paths['previous_cccma_tables']))


else:
    propagate_previous_cccma_tables = False

# Get default values of CCCma table parameters
filepath = os.path.join(paths['project'], cccma_table_defaults)
with open(filepath, 'r') as f:
    cccma_var_defaults = json.load(f, object_pairs_hook=OrderedDict)
    cccma_var_defaults.pop('_comment')

exclude_default_values = True  # don't include a parameter if its value matches the default value for the project

# Specify parameters that must appear for each variable in the CCCma tables, even if no value is set for them
# (purpose is to make it obvious to user that they need to be set for the variable to be converted)
required_parameters = [
    "CCCma TS var name",
    "CCCma diag file",
]

if os.path.exists(mismatch_warnings_logfile):
    os.remove(mismatch_warnings_logfile)

###############################################################################
# Validate list of project's CMOR tables 
print('\nValidating CMOR tables...')
if len(cmor_tables) == 1 and cmor_tables[0] == '*':
    # Get list of all tables in the project
    params = {'project' : project}
    CV_filepath = os.path.join(paths['cmor_tables'], '{project}_CV.json'.format(**params))
    if os.path.exists(CV_filepath):
        # Get list of tables from CV file
        with open(CV_filepath) as f:
            cmor_tables = json.load(f)['CV']['table_id']
    else:
        # Look at all files in the CMOR tables dir to determine which ones are variable tables
        ld = os.listdir(paths['cmor_tables'])
        cmor_tables = []
        for filename in ld:
            filepath = os.path.join(paths['cmor_tables'], filename)
            with open(filepath) as f:
                d = json.load(f)
                # Check if this file contains variables (there are other files in the dir, e.g. {project}_CV.json)
                if 'variable_entry' in d:
                    cmor_table = d['Header']['table_id'].split()[-1]
                    params.update({'cmor_table' : cmor_table})
                    # Check that the extracted table name (cmor_table) is consistent with the expected CMOR table filename
                    if filename != filename_template['cmor_tables'].format(**params):
                        raise Exception('CMOR table name {cmor_table} inconsistent with filename: '.format(**params) + filename)
                    if cmor_table in ['grids']:
                        # Known exceptions (file is not a variables table)
                        continue
                    cmor_tables.append(cmor_table)
                del d
    cmor_tables = sorted(set(cmor_tables), key=str.lower)
else:
    # If list of CMOR tables was provided by user, check that desired project tables exist
    not_found = []
    cmor_tables = sorted(set(cmor_tables), key=str.lower)
    for cmor_table in list(cmor_tables):
        params = {'project' : project, 'cmor_table' : cmor_table}
        filename = filename_template['cmor_tables'].format(**params)
        filepath = os.path.join(paths['cmor_tables'], filename)
        if not os.path.exists(filepath):
            print('  File not found: ' + filepath)
            not_found.append(cmor_table)
            cmor_tables.remove(cmor_table)
    if len(not_found) > 0:
        print('Skipping CMOR tables that were not found:')
        for cmor_table in not_found:
            print('  ' + cmor_table)

n_tables = len(cmor_tables)
# print(f'\n{n_tables} CCCma tables will be created for project={project}, source_id={source_id}')
print(f'Number of CCCma tables to be created: {n_tables}')
print('Names of CCCma tables to be created: ' + ', '.join(cmor_tables))

# Create output dir for CCCma tables if it doesn't exist yet
if not os.path.exists(paths['cccma_tables']):
    os.makedirs(paths['cccma_tables'])

# Check if previous CCCma tables exist in the output dir where new CCCma tables will be written
output_filepath = {}  # holds paths of CCCma tables that will be written
file_exists = []
for cmor_table in cmor_tables:
    params = {'project' : project, 'cmor_table' : cmor_table}
    filename = filename_template['cccma_tables'].format(**params)
    output_filepath[cmor_table] = os.path.join(paths['cccma_tables'], filename)
    if os.path.exists(output_filepath[cmor_table]):
        file_exists.append(output_filepath[cmor_table])
if warn_about_overwrite and len(file_exists) > 0:
    print('\nThe following CCCma tables already exist:')
    for s in file_exists:
        print(s)
    ask_user('Overwrite them?')

# Loop over official CMOR tables
for cmor_table in cmor_tables:
    print('\nGenerating {} CCCma table: {}'.format(project, cmor_table))
    params = {'project' : project, 'cmor_table' : cmor_table}

    # Get CMOR variable names from the project's official CMOR table json file.
    # These are the variable names that are valid within that project (e.g., in CMIP6, 'pr' is precipitation).
    # They are the variable names used by the project's data request.
    filename = filename_template['cmor_tables'].format(**params)  # example: CMIP6_Amon.json
    filepath = os.path.join(paths['cmor_tables'], filename)
    with open(filepath, 'r') as f:
        d = json.load(f)
    cmor_var_info = d['variable_entry']  # dictionary containing the metadata for all CMOR variables in this table
    print('Loaded official table {} containing {} variables'.format(filepath, len(cmor_var_info)))
    del d
    if cmor_table in cmor_vars:
        # Use only a subset of the available variables
        use_vars = cmor_vars[cmor_table]
    else:
        # Use all available variables in the CMOR table
        use_vars = list(cmor_var_info.keys())
    use_vars.sort(key=str.lower) # ensure variables in CCCma table json file will appear in alphabetical order

    cccma_table_previous = {}
    if propagate_previous_cccma_tables:
        # Get info from previously defined CCCma tables
        pct_params = dict(params)
        if cmor_table in pct_tables:
            pct_params.update({'cmor_table' : pct_tables[cmor_table]})
        pct_params.update({'project' : pct_project})
        filename = filename_template['previous_cccma_tables'].format(**pct_params)
        filepath = os.path.join(paths['previous_cccma_tables'], filename)
        if os.path.exists(filepath):
            with open(filepath, 'r') as f:
                cccma_table_previous = json.load(f)
            if '_metadata_' in cccma_table_previous:
                cccma_table_previous.pop('_metadata_')
            print('Importing from previous CCCma table {} containing {} variables'.format(filepath, len(cccma_table_previous)))
        else:
            # print('WARNING: no previous CCCma table info found for table ' + cmor_table)
            print('WARNING:') 
            print('  Previous CCCma table not found: ' + filepath)
            print('  Skipping import from previous CCCma table!')
        del pct_params

    # Loop over variables in the table.
    # This could be all variables or a subset (determined above, where use_vars is set)
    cccma_table = OrderedDict()
    for cmor_var in use_vars:
        assert cmor_var in cmor_var_info, 'Variable {} not found in CMOR table {}'.format(cmor_var, cmor_table)
        
        # Set defaults
        cccma_table[cmor_var] = OrderedDict(cccma_var_defaults)

        if len(cccma_table_previous) > 0:
            # Import all info about this variable from the previous version of the CCCma tables.
            # If a previous version includes info from the official CMOR tables, it will be filtered out.
            # (CCCma tables should not repeat info from the CMOR tables.)
            if cmor_var in cccma_table_previous:
                d = {p:v for p,v in cccma_table_previous[cmor_var].items() if p not in pct_exclude_parameters}
                cccma_table[cmor_var].update(d)
                del d

            if len(pct_check_mismatch) == 0:
                # Don't check for mismatches between previous CCCma table parameters and CMOR table parameters
                check_params = {}
            elif pct_check_mismatch[0].lower() == 'all':
                check_params = list(cccma_table[cmor_var].keys())
            else:
                check_params = pct_check_mismatch
            for p in check_params:
                # Warn user about any mismatch between previous CCCma table parameters and same-named CMOR table parameters
                if p in cccma_table[cmor_var]:
                    if p in cmor_var_info[cmor_var]:
                        if cccma_table[cmor_var][p] != cmor_var_info[cmor_var][p]:
                            # print('Warning for {}_{}: previous CCCma table has incorrect {}'.format(cmor_var, cmor_table, p))
                            err_msg = 'Warning for {}_{} : previous CCCma table has incorrect {}'.format(cmor_var, cmor_table, p)
                            print(err_msg)
                            with open(mismatch_warnings_logfile, 'a') as f:
                                f.write(err_msg + '\n')

        # Filter out any info present in these tables that should come from the official CMOR tables (data request).
        # This is not optional! It prevents redundant info from going into the CCCma tables. 
        # And it ensures the correct provenance of info that comes from the CMOR tables.
        # cccma_table[cmor_var] = {p : v for p,v in cccma_table[cmor_var].items() if p not in cmor_var_info[cmor_var]}
        for p in list(cccma_table[cmor_var].keys()):
            if p in cmor_var_info[cmor_var]:
                cccma_table[cmor_var].pop(p)

        if exclude_default_values:
            # Exclude any parameters whose values are the same as their default values.
            # There would be no harm in keeping these in, but they clutter up the tables.
            exclude = []
            for p in cccma_table[cmor_var]:
                if p in required_parameters:
                    continue
                # Check if this parameter has a default value
                if p in cccma_var_defaults:
                    # If the parameter is in the defaults, check if the value is the same as the default value
                    if cccma_table[cmor_var][p] == cccma_var_defaults[p]:
                        # Parameter value matches the default value, so there's no reason to keep it
                        exclude.append(p)
            for p in list(cccma_table[cmor_var].keys()):
                if p in exclude:
                    cccma_table[cmor_var].pop(p)

        # Finally, ensure that every parameter in the output had a default value specified.
        # This is a guard against unknown parameters mysteriously appearing in the output by accident.
        # Since we may be importing info from previously defined tables, this is a safety check.
        # It ensures that the defaults file determines the set of allowed parameters.
        for p in list(cccma_table[cmor_var].keys()):
            if p not in cccma_var_defaults:
                cccma_table[cmor_var].pop(p)

    with open(output_filepath[cmor_table], 'w') as f:
        json.dump(cccma_table, f, indent=2)

    print('Wrote {} for {} variables'.format(output_filepath[cmor_table], len(use_vars)))

if os.path.exists(mismatch_warnings_logfile):
    print(f'\nWrote logfile of parameter mismatches: {mismatch_warnings_logfile}')
