#!/bin/bash
set -e
# NOTE: compute_system doesn't get used downstream, but we'll keep it in the interface for now

#~~~~~~~~~~~~~~
# Function def
#~~~~~~~~~~~~~~
function usage(){
    echo "$(basename $0): runid=RUNID run_start=RUN_START run_stop=RUN_STOP variant_label=VARIANT_LABEL"
    echo "                  parent_runid=PARENT_RUNID parent_branch_time=PARENT_BRANCH_TIME"
    echo "                  source_id=SOURCE_ID experiment_id=EXPERIMENT_ID activity_id=ACTIVITY_ID"
    echo "                  [subexperiment_id=SUBEXPERIMENT_ID] [compute_system=COMPUTE_SYSTEM]"
    echo ""
    echo "  Build experiment entry for the defined run and return it to stdout, so it can be"
    echo "  injected into an experiments table to be picked up by pycmor.py"
    echo ""
    echo "  NOTE: compute_system is a legacy variable that isn't used any more, but it will be injected"
    echo "          into the experiment entry if provided"
}

function err_echo(){
    # output to stderr
    local msg=$@
    >&2 echo $msg
    return 0
}

function parse_args(){
    # parse command line args
    local arg var val
    for arg in "$@"; do
        case $arg in
            *=*) 
                var=$(echo $arg | awk -F\= '{printf "%s",$1}')
                val=$(echo $arg | awk -F\= '{printf "%s",$2}')
                case $var in
                                 runid) runid="$val" ;;
                             run_start) run_start="$val" ;;
                              run_stop) run_stop="$val" ;;
                         variant_label) variant_label="$val" ;;
                          parent_runid) parent_runid="$val" ;;
                    parent_branch_time) parent_branch_time="$val" ;;
                             source_id) source_id="$val" ;;
                         experiment_id) experiment_id="$val" ;;
                           activity_id) activity_id="$val" ;;
                      subexperiment_id) subexperiment_id="$val" ;;
                        compute_system) compute_system="$val" ;;
                esac ;;
              *)
                err_echo "Invalid command line arg --> $arg <--"; usage; exit 1;;
        esac
    done

    # check for required vars
    required_vars="runid run_start run_stop variant_label parent_runid parent_branch_time source_id experiment_id"
    required_vars="${required_vars} activity_id"
    for varname in $required_vars; do
        [[ -z ${!varname} ]] && { err_echo "$(basename $0): $varname must be given at the command line!"; usage ; exit 1; }
    done
    
    # set default for subexperiment
    [[ -z $subexperiment_id ]] && subexperiment_id="none"
    return 0
}

function days_in_month(){
   set -e
   # Determine the number of days in a given month (assuming 365 day year)
   local days_this_month
   [[ -z "$1" ]] && { err_echo "--> days_in_month <-- requires an integer as the first arg"; exit 1; }
   case $1 in
     1|01) days_this_month=31 ;;
     2|02) days_this_month=28 ;;
     3|03) days_this_month=31 ;;
     4|04) days_this_month=30 ;;
     5|05) days_this_month=31 ;;
     6|06) days_this_month=30 ;;
     7|07) days_this_month=31 ;;
     8|08) days_this_month=31 ;;
     9|09) days_this_month=30 ;;
       10) days_this_month=31 ;;
       11) days_this_month=30 ;;
       12) days_this_month=31 ;;
        *) { err_echo "month = $1 is out of range"; exit 1; };;
   esac
   echo $days_this_month
}

function create_experiment_entry(){

    # process start/stop dates
    run_start_array=( ${run_start//:/ } )
    [[ -z ${run_start_array[1]} ]] && run_start_array[1]=01 # assume jan start
    [[ -z ${run_start_array[2]} ]] && run_start_array[2]=01 # assume first day of the month
    [[ -z ${run_start_array[3]} ]] && run_start_array[3]=00 # assume start at midnight
    run_stop_array=( ${run_stop//:/ } )
    [[ -z ${run_stop_array[1]} ]] && run_stop_array[1]=12 # assume dec stop
    [[ -z ${run_stop_array[2]} ]] && run_stop_array[2]=$(days_in_month ${run_stop_array[1]})
    [[ -z ${run_stop_array[3]} ]] && run_stop_array[3]=23 # assume the final hour is from 23-24
   
    # get formatted branch time in parent
    BTIP_year=$(echo ${parent_branch_time%_*} | sed 's/^0*//') # strip leading zeros
    BTIP_mnth=$(echo ${parent_branch_time#*_m}| sed 's/^0*//')
    if (( BTIP_mnth == 12 )); then 
        tmp_yr=$((BTIP_year+1))
        tmp_yr=$(printf '%04d' "$tmp_yr") # pad to four digits
        branch_time_in_parent=${tmp_yr}:01:01:00
    else
        tmp_yr=$(printf '%04d' "$BTIP_yr") # pad to four digits
        tmp_mnth=$((BTIP_mnth+1))
        tmp_mnth=$(printf '%02d' "$tmp_mnth") # pad to two digits
        branch_time_in_parent=${tmp_yr}:${tmp_mnth}:01:00
    fi

    # get formatted branch time in child
    branch_time_in_child=${run_start_array[0]}:${run_start_array[1]}:${run_start_array[2]}:${run_start_array[3]}

    # set up entry and return
    NEW_ENTRY="| $runid | $activity_id | $source_id | $experiment_id | $subexperiment_id "
    NEW_ENTRY=${NEW_ENTRY}"| $variant_label | | $parent_runid | $branch_time_in_parent "
    NEW_ENTRY=${NEW_ENTRY}"| $branch_time_in_child | ${run_start_array[0]}m${run_start_array[1]}:${run_stop_array[0]}m${run_stop_array[1]} "
    NEW_ENTRY=${NEW_ENTRY}"| $(whoami) | ${compute_system} | | |"
    echo $NEW_ENTRY
}

#~~~~~~~~~~~~~
# Main script
#~~~~~~~~~~~~~
parse_args $@
create_experiment_entry
