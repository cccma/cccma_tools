"""
    answer_checks.py:
        A module built to be used to validate 'answers' coming out of the conversion process.

"""
import argparse
import xarray as xr
import numpy as np
import os
import subprocess
import json
import cftime
import sys

mod_dir   = os.path.dirname(os.path.realpath(__file__)) 
hash_file = os.path.join(mod_dir,'..','CI','nightly_builds','var_hashes.json')

def _set_gbl_pycmor_hash():
    """ 
        Set global variable 'pycmor_hash' to avoid calling 'git rev-parse HEAD', as 
        a subprocess, potentially thousands of times.
    """
    # nagivate to module directory
    cwd = os.getcwd()
    os.chdir(mod_dir)
    
    # populate global var
    global pycmor_hash
    pycmor_hash = subprocess.check_output(['git','rev-parse','HEAD']).strip()
    
    # navigate back to working directory
    os.chdir(cwd)

def get_conv_key(fl,chunk=None):
    """
        Take in a netcdf file return the conversion key associated with it. 

        To-Do: add ability to send in opened dataset.

        Inputs:
            fl          (str) : name of netcdf file to inspect
            chunk       (str) : (optional) if given, use this chunk string instead of querying the file for it

        Returns:
            conv_key    (str) : unique conversion key associated with the data inside fl
    """
    def check_special_case(var,table):
        """
            As a result of the CMOR tables sometimes having different 'CMOR Names' and 'out_name',
            this function was created to handle some special cases. This is required because during the 
            conversion process, the conversion keys are created using the CMOR name, and thus, if there
            is a mismatch in the 'CMOR Name' and 'out_name', the 'variable_id' cannot be used  to 
            create a matching conversion key. 

            Ex. hus7h (CMOR Name) in 6hrPlevPt, is written as 'hus'.
        """
        special_cases = {
                            "hus-6hrPlev"   : "hus4",
                            "wap-6hrPlev"   : "wap4",
                            "hus-6hrPlevPt" : "hus7h",
                            "va-6hrPlevPt"  : "va7h",
                            "ua-6hrPlevPt"  : "ua7h",
                            "ta-6hrPlevPt"  : "ta7h",
                            "zg-6hrPlevPt"  : "zg7h",
                            "hfsnthermds-Omon"  : "hfsnthermds2d",
                            "diftrelo-Oclim"    : "diftrelo2d"
                        }
        key = "{}-{}".format(var,table)
        if key in special_cases.keys():
            return special_cases[key]
        else:
            return var

    # define table list to override the chunk information
    #   - 3hr is required here because its data has been shifted by 3hrs, and its causing 
    #       the time extracting code to fail to get the right end year
    override_chunk = ['fx','3hr']

    try:
        vardata = xr.open_dataset(fl, decode_times=False)
    except IOError:
        err_str  = "answer_checks.calc_hash(): unsupported filetype -> {}.\n ".format(fl)
        err_str += "Expects a netcdf filename!"
        raise IOError(err_str)

    # get easy parameters
    runid = vardata.attrs['CCCma_runid']
    table = vardata.attrs['table_id']
    var   = vardata.attrs['variable_id']
    model = vardata.attrs['source_id']

    # check for special case of CMOR Name 'out_name' mismatch
    var = check_special_case(var,table)

    # get chunk
    if not chunk:
        if table in override_chunk:
            print("You didn't provide a chunk string, and {} is in the override chunk list!".format(table))
            print("Setting chunk string to 'unknown_unknown'")
            chunk = "unknown_unknown"
        else:
            cal     = vardata['time'].attrs['calendar']
            tunits  = vardata['time'].attrs['units']
            chunk_beg = cftime.num2date(vardata['time'].values[0],tunits,calendar=cal)
            chunk_end = cftime.num2date(vardata['time'].values[-1],tunits,calendar=cal)
            chunk = "{}_{}".format(chunk_beg.year,chunk_end.year)

    # build unique conversion key
    conv_key = "{}-{}-{}-{}-{}".format(model,var,table,runid,chunk)

    # close and return
    vardata.close()
    return conv_key

def write_answers(hash_info,local_file='var_hashes.json',update_hashes=False):
    """
        Take in a dictionary containing variable hash information and write it to 'var_hashes.json'
        in the current working directory.

        Inputs:
            hash_info       (dict) : dictionary containing hash information.
            local_file      (str)  : name of file to contain the hash information.
            update_hashes   (bool) : if true, will update the existing 'var_hashes.json' file instead
                                        of creating a new one.
    """
    if update_hashes:
        with open(local_file,'r') as f:
            old_hash_info = json.load(f)
        old_hash_info.update(hash_info)
        hash_info = old_hash_info
    
    with open(local_file,'w') as f:
        json.dump(hash_info,f,sort_keys=True,indent=4)

def calc_hash(inp):
    """
        This function takes in an xarray dataset object or a netcdf file and return the 
        integer hash associated with the variables contained in the data. If multiple variables
        are contained in the data, the integer hash is the sum of the hashes from all vars.

        Inputs:
            inp  (xarray.core.dataset.Dataset/str) : xarray dataset object or netcdf file for which to calculate the 
                                                        integer hash associated with its internal variables

        Returns:
            var_hash    (int)   : summed integer hash associated with the variables in 'inp'
    """
    # get variable data
    if isinstance(inp, xr.core.dataset.Dataset):
        vardata = inp
    elif isinstance(inp, str):
        try:
            vardata = xr.open_dataset(inp, decode_times=False)
        except IOError:
            err_str  = "answer_checks.calc_hash(): unsupported filetype -> {}.\n ".format(inp)
            err_str += "Expects a netcdf filename!"
            raise IOError(err_str)
    else:
        err_str  = "answer_checks.calc_hash(): unsupported object -> {}.\n ".format(type(inp))
        err_str += "Expects a netcdf filename or xarray dataset object!"
        raise TypeError(err_str)

    # calculate integer hash
    var_hash = 0
    for var,dat in vardata.iteritems():
        
        # convert to string (to be compatible with the hash() function)
        array_str = dat.values.tostring()

        # add hash
        var_hash += hash(array_str)

    # close dataset if a filename was given
    if isinstance(inp,str): vardata.close()
    return var_hash 

def check_delta(fl,conv_key,stored_var_hashes=None,verbose=False):
    """
        This function takes in a cmorized file, calculates an integer hash associated
        with all the variables in the file and compares it to a stored hash in 
        a stored hash file (see answer_checks.hash_file for the exact filename). 

        Inputs:
            fl                  (str)  : file containing the cmorized data
            conv_key            (str)  : unique conversion identifier (model-var-cmortable-runid-chunk)
            stored_var_hashes   (dict) : optional dictionary of stored hashes that will be used for the
                                            comparison if provided (instead of opening answer_checks.hash_file
                                            every time).
            verbose             (bool) : if True, prints calculated hashes

        Returns:
            hash_info           (dict) : dictionary containing the updated hash info for the data in question
            hash_delta          (bool) : True if hash has changed
    """
    # initiate useful vars
    hash_info   = {}                                                

    # get stored hashes if not provided
    if stored_var_hashes is None:
        with open(hash_file,'r') as f:
            stored_var_hashes = json.load(f)

    # try to get entry for this conversion key
    try:
        stored_hash_info = stored_var_hashes[conv_key]
    except KeyError:
        print("No stored hash found for {}".format(conv_key))
        stored_hash_info = {
                                'hash'          : None,
                            'hash_prod_cmmt'    : None,
                                'frozen'        : False,
                            }

    # set new hash_info's 'frozen' status to match stored info
    hash_info['frozen'] = stored_hash_info['frozen']
     
    # check if the variable was successfully converted
    if fl is None:  
        hash_info['hash'] = None
    else:
        # open file and store in dataset object
        vardata = xr.open_dataset(fl, decode_times=False)

        # calculate hash and store it
        hash_info['hash'] = calc_hash(vardata)

    if verbose:
        print("stored hash: {}\ncalc'd hash: {}".format(stored_hash_info['hash'],hash_info['hash'])) 

    # compare hashes and return
    if hash_info['hash'] == stored_hash_info['hash']:
        hash_info['hash_prod_cmmt'] = stored_hash_info['hash_prod_cmmt']
        hash_delta = False
    else:
        if fl is None:
            # the running pycmor failed to convert the var.
            #   Store its hash.
            try:
                hash_info['hash_prod_cmmt'] = pycmor_hash
            except NameError:
                _set_gbl_pycmor_hash()
                hash_info['hash_prod_cmmt'] = pycmor_hash
        else:
            # store hash from file
            hash_info['hash_prod_cmmt'] = vardata.attrs['CCCma_pycmor_hash']
        hash_delta = True
    
    # close dataset if needed
    if not fl is None:
        vardata.close()
    return hash_info,hash_delta

def check_delta_test(fl,conv_key,verbose=False):
    """
        Take in a converted file that was used to create the stored hash in the stored var_hashes.json file, 
        along with associated, unique conversion key, and make sure that the check_delta function catches
        various data permutations in the original file.

        Inputs:
            fl          (str) : file containing dataset to test permutations on
            conv_key    (str) : unique conversion key associated with data in file (model-var-cmortable-runid-chunk)
            verbose     (bool): set to true to print out hash information
    """
    
    def add_eps_array(dset,var):
        """ 
            Add the machine epsilon to the entire array
        """
        new_dset = dset.copy(deep=True)
        val_array = new_dset[var].values

        # determine epsilon according to the mean of the data
        mean_val = np.mean(val_array)
        eps = np.finfo(type(mean_val)).eps*mean_val

        # scale entire array
        new_dset[var].values = val_array + eps
        return new_dset.compute()

    def add_eps_elem(dset,var):
        """
            Add the machine epsilon to the last element of the array
        """
        new_dset = dset.copy(deep=True)
        val_array = new_dset[var].values

        # get index for last item in data-array
        ind = tuple([-1]*val_array.ndim)

        # determine epsilon for last item
        last_elem = val_array[ind] 
        eps = np.finfo(type(last_elem)).eps*last_elem

        # add eps to last elem
        val_array[ind] = eps + val_array[ind]
        new_dset[var].values = val_array
        return new_dset

    def arr_permute(dset,var):
        """
            Permute location of two n-1 arrays, within n dimensional data-array.
        """
        new_dset = dset.copy(deep=True)
        val_array = new_dset[var].values

        # get largest dimension index
        shape = list(val_array.shape)
        lrgst_dim = shape.index(max(shape))

        # get center-ish indices along largest dim
        dim1 = shape[lrgst_dim]//2
        dim2 = dim1-1

        # before we can shuffle two (n-1) arrays, we need to move 
        #   lrgst dim to first dimension.
        arr_copy = np.moveaxis(val_array,lrgst_dim,0)

        # shuffle values at dim1 and dim2 and restore proper 
        #   dimension order
        tmp1 = arr_copy[dim1].copy()
        tmp2 = arr_copy[dim2].copy()
        arr_copy[dim1] = tmp2
        arr_copy[dim2] = tmp1
        arr_copy = np.moveaxis(arr_copy,0,lrgst_dim)

        # finally assign back to new dataset
        new_dset[var].values = arr_copy
        return new_dset

    def arr_flip(dset,var):
        """
            Flip data-array along its smallest dimension
        """
        new_dset = dset.copy(deep=True)
        val_array = new_dset[var].values

        # get smallest dimension index
        shape = list(val_array.shape)
        smllst_dim = shape.index(min(shape))
        
        # flip along smallest dimension
        new_dset[var].values = np.flip(val_array,smllst_dim)
        return new_dset
    
    def no_change(dset,var):
        """
            Simply copy dataset 
        """
        new_dset = dset.copy(deep=True)
        return new_dset

    # open file dataset
    orig_vardata = xr.open_dataset(fl,decode_times=False)
     
    # get stored hash information
    try: 
        with open(hash_file,'r') as f:
            stored_hash_info = json.load(f)
    except IOError:
        print("No hash file located.. halting test")
        return 
   
    # type of array perturbations to test 
    perturb_fnctns = [
                        add_eps_array,
                        add_eps_elem,
                        arr_permute,
                        arr_flip,
                        no_change
                      ] 
    for fnctn in perturb_fnctns:
        # loop over each variable in file
        for var in orig_vardata.keys():
            # create perturbed file
            tmp_f = "{}-{}-{}.nc".format(conv_key,var,fnctn.__name__)
            perturbed_vardata = fnctn(orig_vardata,var)
            perturbed_vardata.to_netcdf(tmp_f,mode='w')
            perturbed_vardata.close()
            perturbed_hash_info, hash_delta = check_delta(tmp_f,conv_key,verbose=verbose)
            if fnctn.__name__ != 'no_change':
                if not hash_delta:
                    print("{} test failed to flag {} perturbation!".format(fnctn.__name__,var))
                else:
                    print("{} test passed for {}".format(fnctn.__name__,var))
            else:
                if not hash_delta:
                    print("{} test passed for {}".format(fnctn.__name__,var))
                else:
                    print("{} test improperly flagged {} perturbation!".format(fnctn.__name__,var))
    orig_vardata.close()

# if ran from the command line
if __name__ == "__main__":

    #======================================================================
    # Define simple command line interface for interacting with this module
    #======================================================================
    description = "A simple command line interface for interacting with the answer_checks module."
    description += "\n Currently limited to just calculating the variables hashes of files in a given directory."
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--calc_hashes", metavar="DIRECTORY", action="store",
                        help=("Calculate the variable hashes for all CMORized netcdf files in the given directory"+
                              " and store in a local var_hashes.json file. If the code fails to extract a chunk"+
                              " from a given netcdf file, it tries to use the chunk from a previously insepcted file."+
                              " This assumes that files in the same directory are from the same chunk. We make this "+
                              "assumption to handle 'fx' variables."))
    parser.add_argument("--chunk", metavar="CHUNK", action="store", default=None,
                        help=("(Optional) Can only be used with '--calc_hashes' and is used to specified a default 'chunk'"+
                              " string that will be attached to the conversion key for the inspected files, if code fails" +
                              " to extract one from then netcdf file. This is typically required for 'fx' variables without"+
                              " any time information"))
    args = parser.parse_args()
    # make sure arguments make sense
    if args.chunk and not args.calc_hashes:
        print("--chunk can only be used with --calc_hashes!")
        sys.exit(1)

    # calculate integer hashes for all CMORized netcdf files in the given directory
    if args.calc_hashes:
        def_chunk   = args.chunk
        directory   = args.calc_hashes
        output_f    = "var_hashes.json"

        # check if local var_hashes.json file exists and warn user of overwriting
        if os.path.isfile(output_f):
            print("Warning! A local copy of {} exists!".format(output_f))
            print("It will be overwritten!")
            while True:
                cont = raw_input("Continue? [yes(y)/no(n)]: ")
                if cont == "yes" or cont == "y":
                    break
                elif cont == "no" or cont == "n":
                    print("..halting")
                    sys.exit()
                else:
                    print("invalid input")

        # initiate hash dicts
        var_hashes  = {}
        with open(hash_file,'r') as f:
            stored_hashes = json.load(f)

        # get hash information for each file
        files = []
        chunk = None
        for r,_,fs in os.walk(directory):
            for f in fs:
                if f.endswith(".nc"): 
                    fl = os.path.join(r,f)
                    conv_key = get_conv_key(fl,def_chunk)
                    hash_info,hash_delta = check_delta(fl,conv_key,stored_var_hashes=stored_hashes)
                    if hash_delta: 
                        print("Hash delta for {}".format(conv_key))
                    var_hashes[conv_key] = hash_info

        # write out updated hashes
        write_answers(var_hashes, output_f)
