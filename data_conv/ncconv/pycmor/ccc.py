#!/usr/bin/env python

###############################################################################
import os
import sys
import time
import struct
import numpy as np
np.core.set_printoptions(linewidth=200)
###############################################################################

STENCIL = []
# Each horizontal field in a ccc file has an description record (IBUF) followed by the field itself (DATA).
# IBUF record:
STENCIL.append({'label' : 'IBUF start', 'bytes' : 4, 'format' : '>i'}) # size of record in bytes
STENCIL.append({'label' : 'KIND',  'bytes' : 8, 'format' : '8s'}) # type of data (e.g. 'GRID', 'ZONL', ...)
STENCIL.append({'label' : 'TIME',  'bytes' : 8, 'format' : '>2i'}) # timestamp
STENCIL.append({'label' : 'NAME',  'bytes' : 8, 'format' : '8s'}) # field name (e.g. 'PCP')
STENCIL.append({'label' : 'LEVEL', 'bytes' : 8, 'format' : '>2i'}) # vertical level
STENCIL.append({'label' : 'ILG',   'bytes' : 8, 'format' : '>2i'}) # no. of longitudes
STENCIL.append({'label' : 'ILAT',  'bytes' : 8, 'format' : '>2i'}) # no. of latitudes
STENCIL.append({'label' : 'KHEM',  'bytes' : 8, 'format' : '>2i'}) # integer indicating whether data is global (0), NH (1), SH (2), or ocean (3)
STENCIL.append({'label' : 'PACK',  'bytes' : 8, 'format' : '>2i'}) # packing density
STENCIL.append({'label' : 'IBUF stop', 'bytes' : 4, 'format' : '>i'}) # size of record in bytes
# DATA record:
STENCIL.append({'label' : 'DATA start', 'bytes' : 4, 'format' : '>i'}) # size of record in bytes
STENCIL.append({'label' : 'xMin', 'bytes' : 8, 'format' : '>d'}) # minimum value in the field (float)
STENCIL.append({'label' : 'xMax', 'bytes' : 8, 'format' : '>d'}) # maximum value in the field (float)
STENCIL.append({'label' : 'DATA', 'format' : '>i'})  # field, stored as integer for packing but will expand to array of floats
STENCIL.append({'label' : 'DATA stop', 'bytes' : 4, 'format' : '>i'}) # size of record in bytes

IBUF_PARMS = ['KIND', 'TIME', 'NAME', 'LEVEL', 'ILG', 'ILAT', 'KHEM', 'PACK']

def chksme(x):
    '''
    Return unique entries of list x (filter out duplicate entries), preserving list order.
    '''
    return [x[m] for m in filter(lambda m: x[m] not in x[:m], range(len(x)))]


def read(filepath, records='all'):
    '''
    Read records from ccc-format binary file.
    '''
    verbose = not True
    lr = [] # list to store records
    n = 0 # count no. of records read
    with open(filepath, 'rb') as f:
        while True:
            n += 1
            end_of_file_reached = f.tell() >= os.fstat(f.fileno()).st_size
            if records in ['all', -1]:
                read_record = not end_of_file_reached
            else:
                assert isinstance(records, int), 'invalid no. of records to read: {}'.format(records)
                read_record = ( n <= records ) and ( not end_of_file_reached )
            if not read_record: break
            if verbose: print('\n * record {} * '.format(n))
            ibuf, params, rec = {}, {}, {}
            bytes_read, bytes_expected = None, None # for tracking how many bytes have been read from the file
            count_bytes = {'>i' : 4}
            for k,d in enumerate(STENCIL):
                label, fmt = d['label'], d['format']
                nbytes = None
                if label.endswith('start'):
                    bytes_read = 0
                elif label.endswith('stop'):
                    if bytes_read < bytes_expected:
                        # read any remaining bytes, ignoring their content
                        nbytes = bytes_expected - bytes_read
                        bytes = f.read(nbytes)
                        bytes_read += nbytes
                if label == 'DATA':
                    count = ibuf['ILG']*ibuf['ILAT']
                    ad = np.fromfile(f, dtype=fmt, count=count)
                    nbytes = count*count_bytes[fmt]
                    pack, xMin, xMax = ibuf['PACK'], params['xMin'], params['xMax']
                    NBITS = 64 / pack
                    XSCAL = (2.**(NBITS-1))/(xMax-xMin)
                    ad = ad/XSCAL + xMin
                    if verbose: print('data array shape: {}, start: {}, end: {}'.format(ad.shape, ad[:3], ad[-3:]))
                else:
                    nbytes = d['bytes']
                    bytes = f.read(nbytes)
                    a = struct.unpack(fmt, bytes)[-1]
                    if not isinstance(a, (int,float)): 
                        a = str(a.decode('UTF-8'))
                        a = a.strip()
                    if label in IBUF_PARMS:
                        ibuf[label] = a
                    else:
                        params[label] = a
                    if verbose: print('{} ({}) : {}'.format(label, str(type(a)), a))
                if label.endswith('start'):
                    bytes_expected = params[label]
                elif label.endswith('stop'):
                    if bytes_read != bytes_expected:
                        raise Exception('Mismatch between number of bytes read ({}) and number of bytes expected ({})'.format(bytes_read, bytes_expected))
                else:
                    # bytes_read is the count of bytes read from the file excluding the start,stop indicators
                    bytes_read += nbytes
            rec = {'ibuf' : ibuf, 'params' : params, 'data' : ad}
            lr.append(rec)
    if verbose: print('\nno. of records read: {}'.format(len(lr)))
    return lr
            
def variables(records, arrays=True):
    '''
    From an list of records produced by read(), group records into variables and return the list of variables.
    '''
    lv = []
    v = {'name' : None}
    for rec in records:
        ibuf = rec['ibuf']
        assert ibuf['NAME'] is not None
        if ibuf['NAME'] != v['name']:
            # if name doesn't match previous record's name, start new variable
            v = {'name' : ibuf['NAME'], 'records' : [],
                 'nlat' : ibuf['ILAT'], 'nlon' : ibuf['ILG'], 'level,time' : []}
            lv.append(v)
        v['records'].append(rec)
        v['level,time'].append( (ibuf['LEVEL'], ibuf['TIME']) )
        assert ibuf['ILAT'] == v['nlat']
        assert ibuf['ILG']  == v['nlon']

    if arrays:
        for v in lv:
            # replace list of records with a single array
            nlat, nlon = v['nlat'], v['nlon']
            v['levels'] = chksme([t[0] for t in v['level,time']])
            v['times'] = chksme([t[1] for t in v['level,time']])
            nlev, ntim = len(v['levels']), len(v['times'])
            nrec = len(v['records'])
            assert nlev*ntim == nrec
            assert len(v['level,time']) == nrec
            shp = (nlat, nlon, nlev, ntim)
            v['dims'] = ['latitude', 'longitude', 'level', 'time']
            ad = np.zeros(shp)
            ad.fill(np.nan)
            v['ibuf'] = {}
            for k, rec in enumerate(v['records']):
                lev, tim = v['level,time'][k]
                assert rec['ibuf']['TIME'] == tim
                assert rec['ibuf']['LEVEL'] == lev
                assert rec['data'].shape == (nlat*nlon,)
                rec['data'].shape = (nlat,nlon)
                (m,t) = v['levels'].index(lev), v['times'].index(tim)
                ad[:,:,m,t] = rec['data']
                v['ibuf'][(m,t)] = rec['ibuf']
            assert not np.any(np.isnan(ad))
            for s in ['records', 'level,time']:
                v.pop(s)
            v['data'] = ad
            for s in ['levels', 'times']:
                v[s] = np.array(v[s])

    return lv

class cccvar():
    '''
    Mimic how cccma_py returns variables.
    '''
    def __init__(self,v):
        self.data = v['data']
        self.dims = v['dims']
        self.ibuf = v['ibuf']
        self.timesteps = v['times']
        self.levels = v['levels']
        self.varname = v['name']
    def load_field(self,t,m):
        # t = time index, m = level index
        # They are ordered (time,level) in the function call because that's the
        # way the cccma_py load_field() function does it. 
        return self.data[:,:,m,t]
    def load_ibuf(self,t,m):
        return self.ibuf[(m,t)]

###############################################################################
###############################################################################
if __name__ == '__main__': # testing

    machine = os.uname()[1]
    dirpath = None
    if machine in ['dlxlp14.cccma.ec.gc.ca']: # James linux laptop
        dirpath = '/HOME/rja/local_data/cmor_test/input'

        filename = 'dc_rc3-pictrl_5020_m09_gp.011'
        filename = 'sc_p2-amip01_198201_198212_tp_derived'
        filename = 'sc_p2-amip01_198201_198212_gp_pcp.001'
    else:

        # example from Carsten of a ccc file with a header (long string containing some info from the model run)
        dirpath = '/space/hall3/sitestore/eccc/crd/ccrn/users/rca001/amip-ref-5.1-release-ca/data'
        filename = 'dc_amip-ref-5.1-release-ca_2008_m09_gp.012'

        # example from Carsten of site data
        dirpath = '/space/hall3/sitestore/eccc/crd/ccrn/users/rca001/amip-mts-5.1-release-ca-0006/data/'
        filename = 'sc_amip-mts-5.1-release-ca-0006_200501_200512_site_olr.001'


        dirpath = '/home/rja001/netcdf_conversion/ncconv/netcdfconv/input'
        filename = 'sc_rms0084-nudged-001_201801_201801_6h_st'


    filepath = os.path.join(dirpath, filename)

    compare_to_cccma_py = True
    if sys.version_info.major != 2: # python2
        compare_to_cccma_py = False

    ###########################################################################
    ###########################################################################
    if compare_to_cccma_py:
        lib_dir = None
        if machine in ['dlxlp14.cccma.ec.gc.ca']: # James linux laptop
            lib_dir = '/usr/local/cview2015/lib/x86_64'
        else:
            # assume we're on science
            lib_dir = '/home/ords/crd/ccrn/scrd101/softpkg_deps/pycmor_deps'
        assert os.path.exists(lib_dir)
        if lib_dir not in sys.path: 
            sys.path += [lib_dir]
        import cccma_py
        assert cccma_py.__file__ == os.path.join(lib_dir, 'cccma_py.so')
    
        time_start = time.time()

        ###########################################################################
        # copied from /usr/local/cview2015/cccma/filereader.py:

        #09/20/2010 - This variable controls whether superlabels should always override the regular field name.
        #          It is passed on to the binary file reader when the file is opened.
        #          When disabled, the original heuristics (from the 2008 IDL ccidv) are used.
        #          This MUST be left disabled in the CCCma_FileReader case for backward compatibility.
        #          If there's a need to enable it, create a subclass (see CCCma_FileReaderSuperlabels below)
        always_use_superlabels = False

        #10/30/2012 - The sort_vars property governs whether the set of records is sorted before
        #             being exposed to the python interface. With this enabled, the set of
        #             records with the same variable name is conglomerated and sorted by level and
        #             timestep. This is generally desirable behavior, but we provide the option
        #             to turn it off if the authors of individual files use the record order to
        #             store out of band information.
        sort_vars = True
        ###########################################################################
        f = cccma_py.open(filepath, always_use_superlabels, sort_vars)
        assert f is not None, 'Failed to open file: ' + filepath
        print('Read file using cccma_py: ' + filepath)
        lv = f.getvarlist()

        #import copy
        #lc0 = copy.deepcopy(lv)
        # dunno why but copy.deepcopy(lv) produces a segfault when running this script
        # repeatedly in an ipython session.
        lc0 = lv 

        del f

        nv_max = 12
        nv_tot = len(lv)
        nv = 1

        for v in lv:
            if False:
                # display attributes
                for att in ['varname', 'var_index', 'timesteps', 'levels']:
                    if not hasattr(v, att):
                        continue
                    a = getattr(v, att)
                    if att in ['timesteps', 'levels']:
                        n = len(a)
                        m_max = 10
                        s = ''
                        if len(a) > m_max:
                            a = a[:m_max]
                            s = ', ...'
                        print(att + ': ' + ', '.join([str(c) for c in a]) + s)
                        print('no. of ' + att + ': ' + str(n))
                    else:
                        print(att + ':', a)

            ibuf = v.load_ibuf(0,0)    # seems only to work for input args 0,0, but that's fine I guess (do we ever need info from other IBUF's?)
            #print('IBUF: ', ibuf)
            
            if ibuf['KIND'].upper() in ['LABL', 'CHAR']: stop
            if ibuf['KIND'] != 'GRID' : stop
            
            #print 'no. of times:  ' + str(len(v.timesteps))
            #print 'no. of levels: ' + str(len(v.levels))
            #print('data (timestep, level, array shape, min, max):')

            n = 0
            n_tot = len(v.timesteps)*len(v.levels)
            n_max = 7
            for k, t in enumerate(v.timesteps):
                for m, lev in enumerate(v.levels):
                    a = v.load_field(k,m) 
                    # If the field doesn't exist for this k,m then this function returns None.
                    # If it does exist then 'a' should be a numpy array containing the field at this timestep and level.
                    # The dimensions of the array seem to be lat x lon.
                    # The cccma_py.get_lats(n) function returns a numpy array giving the Gaussian grid latitudes for a given even number n.
                    # IBUF specifies the number of latitudes. 
                    # For longitudes, there's a function in cccma/filereader.py but I haven't looked at it yet (13dec.18).
                    
                    #print(' '*4, t, lev, a.shape, a.min(), a.max())
                    
                    #if m > 5:
                    #    print ' '*4, 'total ' + str(len(v.levels)) + ' levels available'
                    #    break
                    n += 1
                    if n > n_max: break
                if n > n_max: break
                                        
                #if k > 5:
                #    print ' '*4, 'total ' + str(len(v.timesteps)) + ' times available'
                #    break
                    
            #print('total no. of horizontal fields available: ' + str(n_tot))
            
            nv += 1
            if nv > nv_max: break
            

        time_elapsed = time.time() - time_start
        print('\ntime taken = {} s\n'.format('%.3g' % time_elapsed))
        
        time_elapsed_for_cccma_py = time_elapsed
    ###########################################################################
    ###########################################################################



    time_start = time.time()
    
    lr = read(filepath)
    print('\nread ccc file: ' + filepath)    
    
    lv = variables(lr) 

    lc = [cccvar(v) for v in lv]

    
    time_elapsed = time.time() - time_start
    print('\ntime taken = {} s\n'.format('%.3g' % time_elapsed))


    ###########################################################################
    if compare_to_cccma_py:
        print('elapsed time / cccma_py elapsed time = {}'.format( '%.3g' % float(time_elapsed / time_elapsed_for_cccma_py)))

        assert len(lc) == len(lc0)
        for k,v in enumerate(lc):
            v0 = lc0[0]
            print(v.varname == v0.varname.strip())
            #print(v.timesteps, v0.timesteps)
            print(np.all(v.timesteps == v0.timesteps))
            #print(v.levels, v0.levels)
            print(np.all(v.levels == v0.levels))

            for m,lev in enumerate(v.levels):
                for t,tim in enumerate(v.timesteps):
                    a  =  v.load_field(t,m) 
                    a0 = v0.load_field(t,m) 
                    #print(m,t,a.shape == a0.shape, np.all(a == a0), np.all( np.isclose(a,a0)))
                    print(m, t, a.squeeze().shape == a0.squeeze().shape, np.all(a == a0), np.all( np.isclose(a,a0)))



