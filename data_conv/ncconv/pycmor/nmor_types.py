"""
Module defining various types used by the NEMO portion of pycmor.
"""

import xarray as xr
import numpy as np
import cftime

class nmor_var( object ):
    """
    Given the path to a netcdf file,  deduce and store the grid information needed to
    completely define the axes/grid metatdata
    Fields:
        has_x       (bool)   : True if the variable has a longitudinal extent
        has_y       (bool)   : True if the variable has a latitudinal extent
        has_z       (bool)   : True if the variable has a depth extent
        has_t       (bool)   : True if the variable has a time extent
        cccvar      (str)    : Name of the variable as it comes out of a CCCma model
        depth_var   (str)    : Name of the depth variable
        field_type  (str)    : The general calss of variable (2d, 3d, basin, strait, timeseries, etc)
        filename    (str)    : Filename where the variable is stored
        hor_stagger (char)   : Which of the horizontal staggered grids the variable 'lives' on
        ver_stagger (char)   : Which of the vertical staggered grids the variable 'lives' on
        vardata     (xarray) : xarray dataset or variable (assigned on construction)

    """
    # Set initial values for all of attributes of this class
    has_x = False
    has_y = False
    has_z = False
    has_t = False

    depth_var   = ''
    field_type  = ''
    filename    = ''
    hor_stagger = ''
    ver_stagger = ''

    vardata = None
    ntime = 0

    def set_variable_coords( self, cmor_dims ):
        """
        Determine various properties of a variable given and deduce the type of field that exists
        Input:
           cmor_dims (list) : Contains all the CMOR-specified dimensions 
        Output:
           modifies nmor_var types in-place
        """
        # Determine whether the variable has a vertical dimension by searching to see if a coordinate
        # with 'depth' in the name exists
        for coord in self.vardata.coords:
            if 'depth' in coord or 'z' in coord:
                self.has_z = True
                self.depth_var = coord
                break

        # Check for existence of x and y to determine whether the variable has a longitudinal
        # and/or latitudinal extent
        self.has_x = 'x' in self.vardata.dims
        if self.has_x: # Need to additionally check that the coordinate is more than 1 entry
            if self.vardata.dims['x'] < 2:
                self.has_x = False
        self.has_y = 'y' in self.vardata.dims
        if self.has_y: # Need to additionally check that the coordinate is more than 1 entry
            if self.vardata.dims['y'] < 2:
                self.has_y = False

        # Check CMOR-dims to see if this variable should have a time dimension
        self.has_t = any([True for dim in cmor_dims if 'time' in dim])
        if self.has_t: # Store the number of time variables
            self.ntime = len(self.vardata.time_counter.values)
        else: # Remove the time_counter dimension
            if 'time_counter' in self.vardata.coords:
                self.vardata = self.vardata.squeeze('time_counter')

        # Determine what type of variable this is (2d, 3d, x-section, y-section, timeseries, static)
        field_type = None
        if   self.has_x and self.has_y and not self.has_z:
            self.field_type = '2d'
        elif self.has_x and self.has_y and self.has_z:
            self.field_type = '3d'
        elif self.has_y and self.has_z:
            self.field_type = 'y-section'
        elif self.has_x and self.has_z:
            self.field_type = 'x-section'
        elif not self.has_x and not self.has_y and not self.has_z:
            self.field_type = 'timeseries'
        elif 'basin' in cmor_dims:
            self.field_type = 'basin'
        else:
            raise Exception("Ocean variable is not 2d, 3d, y-section, x-section, or timeseries")

        # Determine which grid this variable lives on
        if self.field_type not in ['timeseries', 'basin']:
            if self.has_z :
                self.deduce_horizontal_stagger()
                self.deduce_vertical_stagger()
            else:
                self.deduce_horizontal_stagger()

    def deduce_horizontal_stagger(self):
        """Determine where on the horizontal grid the variable exists

           For a 2d variable, at CCCma all variables in a file exist on the same grid for the ocean
           model. For a 3d variable a second consistency check is done by ensuring that the last letter
           of the 'depth' variable denotes the same grid.

        """
        coords = self.vardata.coords
        stagger = dict.fromkeys( ['filename','depthname','longname'], None )
        ## First see if we can deduce the grid from the filename
        # Define the filenames corresponding to the t, u, and v points
        t_grid_names = ['grid_t', 'grid_w', 'ptrc_t', 'diad_t']
        u_grid_names = ['grid_u']
        v_grid_names = ['grid_v']
        f_grid_names = ['grid_f']

        # Check to see if the filename matches any of the known grid conventions
        for file_id in t_grid_names:
            if file_id in self.filename:
                stagger['filename'] = 't'
                break
        for file_id in u_grid_names:
            if file_id in self.filename:
                stagger['filename'] = 'u'
                break
        for file_id in v_grid_names:
            if file_id in self.filename:
                stagger['filename'] = 'v'
                break
        for file_id in f_grid_names:
            if file_id in self.filename:
                stagger['filename'] = 'f'
                break
        ## If a 3d variable, try to determine stagger from the name of the depth variable
        depth_names = ['deptht','depthu','depthv','depthw']
        if self.has_z:
            for depth_name in depth_names:
                if depth_name in coords:
                    stagger['depthname'] = depth_name[-1]
                    # depthw seems like it always corresponds to the horizontal t-point
                    if stagger['depthname'] == 'w':
                        stagger['depthname'] = 't'
                    break

        ## Lastly check to see if we can figure out the stagger from the long name of the variable
        longnames = [ self.vardata[var].attrs['long_name'].lower() for var in self.vardata.variables
                                                                   if 'long_name' in self.vardata[var].attrs ]
        u_keywords = ['eastward',  'i-axis', 'u point']
        v_keywords = ['northward', 'j-axis', 'v point']
        for longname in longnames:
            if any( [ key in longname for key in u_keywords ] ):
                stagger['longname'] = 'u'
            if any( [ key in longname for key in v_keywords ] ):
                stagger['longname'] = 'v'

        # Return any of the staggering methods that are not none and then do some sanity checking
        stagger_returns = [ stagger[stagger_name] for stagger_name in stagger if stagger[stagger_name] is not None ]
        if len( stagger_returns ) == 0:
            self.hor_stagger = 't'
            return
        elif len(set(stagger_returns)) > 1:
            print(stagger_returns)
            for i in stagger:
                print(i, stagger[i])
            raise Exception("Horizontal stagger inconsistent between methods")
        else: # All the methods are consistent
            self.hor_stagger = stagger_returns[0]

    def deduce_vertical_stagger(self):
        """
        Determines on which point of the vertical grid the variable lives

           Inputs:
               variable (xarray variable) : Contains all the coordinates of the given file
                                          : Note that this assumes one variable per file
           Outputs:
               vertical_stagger   (str)   : Point on the vertical grid, possible values t, w
        """
        coords = self.vardata.coords
        # Only depthw is defined at the interface
        if 'depthw' in coords:
            self.ver_stagger = 'w'
        else:
            self.ver_stagger = 't'

    def apply_mask(self, nemo_grid, misval):
        """
        Figure out which mask to use and set masked data to misval
        Inputs:
            nemo_grid          (xarray data) : Contains nemo grid from mesh_mask file
            misval             (real)        : Missing value to use for masked fields
        """
        # First check to see if we should return immediately, then proceed to other types
        if   self.field_type in ['timeseries', 'basin']:
            return self
        elif self.field_type in ['x-section', 'y-section']:
            # We don't have a good way to deal with this other than assuming that 0. is a missing value
            return np.where( self == 0., misval, self )
        # This inherently assumes that 2d fields should have the same mask as the surface
        elif self.field_type == '2d':
            if   self.hor_stagger == 't':
                mask = nemo_grid['tmask'][:,0,...]
            elif self.hor_stagger == 'u':
                mask = nemo_grid['umask'][:,0,...]
            elif self.hor_stagger == 'v':
                mask = nemo_grid['vmask'][:,0,...]
        elif self.field_type == '3d':
            nz = len(self.vardata[self.depth_var])
            if   self.hor_stagger == 't':
                mask = nemo_grid['tmask'][:,:nz,...]
            elif self.hor_stagger == 'u':
                mask = nemo_grid['umask'][:,:nz,...]
            elif self.hor_stagger == 'v':
                mask = nemo_grid['vmask'][:,:nz,...]

        if not self.has_t:
            mask = mask.squeeze(dim = 't')
        self.vardata[self.cccvar] = self.vardata[self.cccvar].where( mask.values, misval )

    # See the docs directory for how these vertices are calculated
    def calc_vertices(self, grid):
        """
        Returns the lat,lon of the boundaries of the every point in lat/lon. See ncconv/docs for a more detailed
        description
        Inputs:
            grid (xarray data) : contains all the model metrics, usually a 'mesh_mask' file 
        """

        # Set the correct name for the lat/lon variable to use
        lat_varname = 'gphi'
        lon_varname = 'glam'
        center_lats = np.array(grid[lat_varname+self.hor_stagger][0,...])
        center_lons = np.array(grid[lon_varname+self.hor_stagger][0,...])

        if self.hor_stagger == 't':
            lat_varname += 'f'
            lon_varname += 'f'
        elif self.hor_stagger == 'u':
            lat_varname += 'v'
            lon_varname += 'v'
        elif self.hor_stagger == 'v':
            lat_varname += 'u'
            lon_varname += 'u'
        else:
            raise ValueError("Undefined horizontal stagger")

        lats = np.array(grid[lat_varname][0,...])
        lons = np.array(grid[lon_varname][0,...])

        # Every lat/lon point is a rectangular box, so it must have 4 vertices
        nlat, nlon = lats.shape
        nvert = 4

        vertices_latitude  = np.zeros( (nlat, nlon, nvert) )
        vertices_longitude = np.zeros( (nlat, nlon, nvert) )

        vertices_latitude[:,  :,  0] = lats[:   , :   ]
        vertices_latitude[1:, :,  1] = lats[0:-1, :   ]
        vertices_latitude[1:, 1:, 2] = lats[0:-1, 0:-1]
        vertices_latitude[:,  1:, 3] = lats[:   , 0:-1]
        vertices_longitude[:,  :,  0] = lons[:   , :   ]
        vertices_longitude[1:, :,  1] = lons[0:-1, :   ]
        vertices_longitude[1:, 1:, 2] = lons[0:-1, 0:-1]
        vertices_longitude[:,  1:, 3] = lons[:   , 0:-1]

        # The left and bottom edges are done explicitly in a loop to make it clearer how this is actually calculated
        # Calculate the vertices of the bottom left corner by assuming distances are the same
        # from the cell center and to an edge on east/west or north/south are the same
        i = 0 ; j =0
        dlat = lats[i,j] - center_lats[i,j]
        dlon = lons[i,j] - center_lons[i,j]

        vertices_latitude[i,j,0] = lats[i,j]
        vertices_latitude[i,j,1] = lats[i,j] - 2.*dlat
        vertices_latitude[i,j,2] = lats[i,j] - 2.*dlat
        vertices_latitude[i,j,3] = lats[i,j]

        vertices_longitude[i,j,0] = lons[i,j]
        vertices_longitude[i,j,1] = lons[i,j]
        vertices_longitude[i,j,2] = lons[i,j] - 2.*dlon
        vertices_longitude[i,j,3] = lons[i,j] - 2.*dlon

        # Now deal with the rest of the bottom row
        i = 0
        for j in range(1,nlon):
            dlat = lats[i,j] - center_lats[i,j]
            vertices_latitude[i,j,0] = lats[i,   j]
            vertices_latitude[i,j,1] = lats[i-1, j]
            vertices_latitude[i,j,2] = lats[i,   j] - 2.*dlat
            vertices_latitude[i,j,3] = lats[i,   j] - 2.*dlat

            vertices_longitude[i,j,0] = lons[i,   j]
            vertices_longitude[i,j,1] = lons[i-1, j]
            # Assume that the left side is all at the same longitude
            vertices_longitude[i,j,2] = lons[i, j-1]
            vertices_longitude[i,j,3] = lons[i, j-1]

        # Now deal with the rest of the left column
        j = 0
        for i in range(1,nlat):
            dlon = lons[i,j] - center_lons[i,j]
            vertices_latitude[i,j,0] = lats[i,   j]
            vertices_latitude[i,j,1] = lats[i-1, j]
            vertices_latitude[i,j,2] = lats[i-1, j]
            vertices_latitude[i,j,3] = lats[i,   j]

            vertices_longitude[i,j,0] = lons[i,   j]
            vertices_longitude[i,j,1] = lons[i-1, j]
            vertices_longitude[i,j,2] = lons[i-1, j] - 2*dlon
            vertices_longitude[i,j,3] = lons[i,   j] - 2*dlon

        return vertices_latitude, vertices_longitude
    def calc_lat_bnds( self ):
        """
        Calculate the latitude bounds given a vector of latitudes. The south bound is calculated as halfway between
        the current latitude and the south cell. The north bound is halfway between the current latitude and the north
        cell.
        """

        lat = self.vardata.nav_lat
        assert len(lat.shape) == 1, "nav_lat should only be one dimension if the field is not 2d"
        nlat = len(lat)
        cell_bnds = np.zeros((nlat,2))
        # Do interior cells first
        for i in range(1,nlat-1):
            cell_bnds[i,0] = 0.5*(lat[i-1]+lat[i])
            cell_bnds[i,1] = 0.5*(lat[i+1]+lat[i])
        # Now do the southern boundary
        dy = 0.5*(lat[0]+lat[1])-lat[0]
        cell_bnds[0,0] = lat[0] - dy
        cell_bnds[0,1] = 0.5*(lat[0]+lat[1])
        # Now do the northern boundary
        dy = lat[-1] - 0.5*(lat[-1]+lat[-2])
        cell_bnds[-1,0] = lat[-1] + dy
        cell_bnds[-1,1] = 0.5*(lat[-1]+lat[-2])

        return cell_bnds

    def shift_time_origin( self, new_units, new_calendar ):
        """
        Sets a new time variable within this nmor_var type that that corresponds to a given time origin and calendar
        Inputs:
            new_units (str) : The units of the reference calendar e.g. 'days since 1850-1-1 00:00:00'
            new_calendar  (str) : The type of calendar used (e.g. '365_day')
            Note: Both of these arguments need to be parseable by cftime
        Outputs:
            self.shifted_time        (ntime array)     !< The actual timestamps in the units and calendar
                                                       !! defined by the inputs
            self.shifted_time_bounds (ntime x 2 array) !< The timebounds in the units and calendar defined
                                                       !! by the inputs
        """
        original_units    = self.vardata['time_counter'].units
        original_calendar = self.vardata['time_counter'].calendar

        # First convert to a cftime datetime object, then convert back into an actual number
        self.shifted_time = cftime.date2num(
                cftime.num2date(self.vardata['time_counter'], original_units, original_calendar),
                new_units, new_calendar)
        # Do the same for the time bounds
        self.shifted_time_bnds = cftime.date2num(
                cftime.num2date(self.vardata['time_counter_bnds'], original_units, original_calendar),
                new_units, new_calendar)

    def __init__(self, vardata, filename, cccvar, cmor_dims):
        """
        As part of the initialization set the filename, load the data, and set all the coordinate info needed
        Inputs:
            vardata  (xr.dataset) : The dataset containing data from a previous call to xr.open_dataset
            filename (str)        : The filename containing this variable
            cccvar   (str)        : The 'CCCma' name for this variable
            cmor_dims (list)      : A list containing all the CMOR-specified dimensions
        """
        self.filename = filename
        self.cccvar   = cccvar
        self.vardata  = vardata.copy()
        self.set_variable_coords(cmor_dims)
    def __repr__(self):
        print("has_x: {} has_y: {} has_z: {} has_t: {}".format(self.has_x, self.has_y, self.has_z, self.has_t))
        print("depth_var: {} field_type: {} filename: {} hor_stagger: {} ver_stagger: {}".format(self.depth_var, self.field_type, self.filename, self.hor_stagger, self.ver_stagger))
        print(self.vardata)

        return "Contents of nnmor_var"
