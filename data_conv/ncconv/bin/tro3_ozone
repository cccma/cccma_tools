#!/bin/sh
set -e
# Create CMIP5 tro3 variable 

if [ $# -ne 3 ] ; then
  echo "Error in $0: invalid number of arguments $#"
  exit 1
fi

pfx=$1
inp=$2 # dummy file
out=$3 # output file

# derive runid from pfx
runid=`echo "$pfx" | cut -f2 -d'_'`

# access and select time period
release x
if [ "$runid" = "igm" -o "$runid" = "igl" -o "$runid" = "ide" -o "$runid" = "ied" -o "$runid" = "iec" ] ; then
  # historical runs (use historical part from rcp45)
  access x rcp45_ozone_rad_1850_2100_monthly_128_64.999
  echo "C*SELECT.  STEP    185001    200512    1     -9001 1000 NAME  ALL" | ccc select x x.1 ; mv x.1 x
elif [ "$runid" = "ibz" -o "$runid" = "icg" -o "$runid" = "idv" -o "$runid" = "idi" -o "$runid" = "ibr" ] ; then
  # rcp26
  access x rcp26_ozone_rad_1850_2100_monthly_128_64.999
  echo "C*SELECT.  STEP    200601    210012    1     -9001 1000 NAME  ALL" | ccc select x x.1 ; mv x.1 x
elif [ "$runid" = "ica" -o "$runid" = "ice" -o "$runid" = "iew" -o "$runid" = "iei" -o "$runid" = "ibv" ] ; then
  # rcp45
  access x rcp45_ozone_rad_1850_2100_monthly_128_64.999
  echo "C*SELECT.  STEP    200601    210012    1     -9001 1000 NAME  ALL" | ccc select x x.1 ; mv x.1 x
elif [ "$runid" = "idr" -o "$runid" = "icf" -o "$runid" = "idu" -o "$runid" = "idh" -o "$runid" = "ibs" ] ; then
  # rcp85
  access x rcp85_ozone_rad_1850_2100_monthly_128_64.999
  echo "C*SELECT.  STEP    200601    210012    1     -9001 1000 NAME  ALL" | ccc select x x.1 ; mv x.1 x
else
  echo "ERROR in tro3_ozone: unknown runid=$runid"
  exit -1
fi

# create a fake LNSP=LN(1000)
echo "C*SELLEV      1 1000" | ccc sellev x x.1
echo "C*XLIN            0.6.90775529" | ccc xlin x.1 x.2
echo "C*RELABL
C*         GRID           LNSP    1" | ccc relabl x.2 lnsp

# interpolate to required pressure levels
ccc gsapl x lnsp x.3 << CARD
C*GSAPL.     22        0.        0.  SIG        0.
    1    2    3    5    7   10   20   30   50   70  100  150  200  250  300  400
  500  600  700  850  925 1000
CARD

# convert units to 1e9
echo "C*XLIN          1.E9" | ccc xlin x.3 ${pfx}_${out}

release x x.1 x.2 x.3 lnsp .ccc_cards
