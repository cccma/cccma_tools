      PROGRAM SNOWDENS
C     PROGRAM SNOWDENS (SNO,       DENS,       OUTPUT,                  )       C2
C    1            TAPE1=SNO, TAPE2=DENS, TAPE6=OUTPUT)
C     ---------------------------------------------------------                 C2
C                                                                               C2
C     JUN 14/10 - S.KHARIN        
C                                                                               C2
CXLIN    - COMPUTE SNOW DENSITY OVER SEA-ICE FROM SNOW AMOUNT.          1  1 C  C1
C                                                                               C3
CAUTHOR  - S.KHARIN                                                             C3
C                                                                               C3
CPURPOSE - COMPUTE SNOW DENSITY OVER SEA-ICE FROM SNOW AMOUNT.                  C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      SNO  = SNOW AMOUNT (KG/M**2)                                             C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      DENS = SNOW DENSITY AS PARAMETERIZED OVER SEA-ICE.                       C3
C
C-----------------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/SNO(1038961)
C
      LOGICAL OK,SPEC
      COMMON/ICOM/IBUF(8),IDAT(2077922)
      DATA MAXX/2077922/,SNOMAX/10./
C---------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C
C     * READ THE NEXT GRID FROM FILE XIN.
C
      NR=0
  150 CALL GETFLD2(1,SNO,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0) CALL                           XIT('SNOWDENS',-1)
        CALL PRTLAB(IBUF)
        WRITE(6,6010) NR
        CALL                                       XIT('SNOWDENS',0)
      ENDIF
      IF(NR.EQ.0) CALL PRTLAB(IBUF)
      DO I=1,IBUF(5)*IBUF(6)
        IF(SNO(I).GT.SNOMAX) THEN
          SNO(I)=0.54*SNO(I)/LOG(1.+0.54*SNO(I)/275.)
        ELSE
          SNO(I)=275.
        ENDIF
      ENDDO
C
C     * SAVE ON FILE XOUT.
C
      CALL PUTFLD2(2,SNO,IBUF,MAXX)
      NR=NR+1
      GO TO 150
C---------------------------------------------------------------------
 6010 FORMAT('0 SNOWDENS READ',I6,' RECORDS')
      END
