#!/bin/ksh -e
#
#  Author: Yanjun Jiao
#   Usage: tso.ncat runid [optional yearlist, ctablist, varslist in any orders]
# Purpose: Split and concatenate NetCDF files into big chunk.
#
# The filenames must have CMIP6 standard format:
# fnamtype=${var}_${ctab}_${modid}_${expid}_${ripid}_gn_yyyy[mmddhh]-yyyy[mmddhh].nc
#----------------------------------------------------------------
# make sure this runid is defined in cmip6_experiments.txt
. $RUNPATH/ncconv/bin/tso.runinfo $@

ncout=$dirwrk00/$runid
##################################################################
# get full path of cmip6 directory
cd $ncout
dircmip6=${ncout}/$(find CMIP6 -maxdepth 5 -mindepth 5 -type d)

cd $dircmip6
[[ $ctablist ]] || ctablist=$(ls )

# Don't concatenate fixed and 6hrLev tables. sub-daily tables need to be tested for different time stamps
for ctab in Ofx fx CFsubhr 6hrLev ; do
  ctablist=$(echo " ${ctablist} " | sed "s/ ${ctab} / /g")
done

# limit the maximum output file size to 8GB
maxsize=$((8*1024*1024*1024))   

# keep loops over ctablist and varslist 
for ctab in $ctablist; do
  case $ctab in
    # maximum years for each concatenated chunk in different frequencies
     *yr*) freq=yr  ; maxyear2D=200; maxyear3D=10 ;;
    *mon*) freq=mon ; maxyear2D=200; maxyear3D=10 ;;    # histoical runs covers 165 years
    *day*) freq=day ; maxyear2D=200; maxyear3D=10 ;;
    *6hr*) freq=6hr ; maxyear2D=10 ; maxyear3D=1  ;;    # 6hrLev are 3D data on model levels to drive RCM, will be in 1-year chunk
    *3hr*) freq=3hr ; maxyear2D=10 ; maxyear3D=1  ;;    # all 3hr are 2D
    *1hr*) freq=1hr ; maxyear2D=10 ; maxyear3D=1  ;;
  esac

  cd $dircmip6/$ctab
  [[ $varslist ]] || varslist=$(ls -1)

  for var in $varslist; do
      #touch the flag
      touch $dircmip6/.concatenating_${runid}_${ctab}_${var}

    ##if [[ -z $varslist || " $varslist " =~ " $var " ]]; then
      vers=$(ls -1 $dircmip6/$ctab/$var/gn|tail -1)
      cd $dircmip6/$ctab/$var/gn/$vers
      fnamtype=${var}_${ctab}_*_*_*_gn_
      fnamlist=$(ls )

      #-------------------------------------------------------------------------
      #split start_time and stop_time 
      if [[ ${#start_time} -eq 4 ]]; then
        start_year=$start_time
        start_month=01
    
        stop_year=$stop_time
        stop_month=12
      else
        start_year=${start_time%m*}
        start_month=${start_time#*m}
    
        stop_year=${stop_time%m*}
        stop_month=${stop_time#*m}
      fi

      #check continuity of all qualified files 
      typeset -Z2 mo1 mo2 dd1 dd2

      mo1=$start_month
      if [[ $mo1 -eq 01 ]]; then
        mo2=12
      else
        mo2=$((mo1-1))
      fi

      dd1=01
      case $mo2 in
                 02) dd2=28 ;;
        04|06|09|11) dd2=30 ;;
                  *) dd2=31 ;;
      esac

      case $freq in
         yr) nyr=1   ; mdh1=""              ; mdh2=""               ;;
        mon) nyr=12  ; mdh1="${mo1}"        ; mdh2="${mo2}"         ;;
        day) nyr=365 ; mdh1="${mo1}${dd1}"  ; mdh2="${mo2}${dd2}"   ;;
        6hr) if [[ $var == @(acculist) ]]; then
               nyr=1460; mdh1="${mo1}${dd1}0300"; mdh2="${mo2}${dd2}2100"
             elif [[ $var == ps ]]; then
               nyr=1460; mdh1="${mo1}${dd1}0000"; mdh2="${mo2}${dd2}1800" 
             else
               nyr=1460; mdh1="${mo1}${dd1}0600"; mdh2="${mo1}${dd1}0000" 
             fi ;;
        3hr) if [[ $var == @(clt|hfls|hfss|mrro|prc|pr|prsn|rldscs|rlds|rlus|rsdscs|rsdsdiff|rsds|rsuscs|rsus|tos) ]]; then
               nyr=2920; mdh1="${mo1}${dd1}0130"; mdh2="${mo2}${dd2}2230" 
             elif [[ $var == ps ]]; then
               nyr=2920; mdh1="${mo1}${dd1}0000"; mdh2="${mo2}${dd2}2100"
             else
               nyr=2920; mdh1="${mo1}${dd1}0300"; mdh2="${mo1}${dd1}0000"
             fi ;;
      esac

      if [[ $ctab == CF3hr ]]; then
        nyr=2920; mdh1="${mo1}${dd1}0300"; mdh2="${mo1}${dd1}0000"
      elif [[ $ctab == E3hr ]]; then
        nyr=2920; mdh1="${mo1}${dd1}0130"; mdh2="${mo2}${dd2}2230" 
      elif [[ $ctab == 6hrPlevPt ]]; then
        nyr=1460; mdh1="${mo1}${dd1}0000"; mdh2="${mo2}${dd2}1800" 
      elif [[ $ctab == CFsubhr ]]; then
        nyr=1460; mdh1="${mo1}${dd1}0000"; mdh2="${mo2}${dd2}1800" 
      fi

      ##if [[ $hrvaris == inst ]]; then
      ##  case $freq in
      ##    6hr) nyr=1460; mdh1="${mo1}${dd1}0600"; mdh2="${mo1}${dd1}0000" ;;   #for instantaneous variables
      ##    3hr) nyr=2920; mdh1="${mo1}${dd1}0300"; mdh2="${mo1}${dd1}0000" ;;   #for instantaneous variables
      ##    1hr) nyr=8760; mdh1="${mo1}${dd1}0100"; mdh2="${mo1}${dd1}0000" ;;   #for instantaneous variables
      ##  esac
      ##else
      ##  case $freq in
      ##    6hr) nyr=1460; mdh1="${mo1}${dd1}00"; mdh2="${mo2}${dd2}18" ;;       #for accumulated variables
      ##    3hr) nyr=2920; mdh1="${mo1}${dd1}00"; mdh2="${mo2}${dd2}21" ;;       #for accumulated variables
      ##    1hr) nyr=8760; mdh1="${mo1}${dd1}00"; mdh2="${mo2}${dd2}23" ;;       #for accumulated variables
      ##  esac
      ##fi

      #todo: how do i know if the first or the last year is missing
      date3=""
      for fnam in $(ls ${fnamtype}*.nc); do
        date1=$(basename $fnam .nc|cut -d_ -f7|cut -d\- -f1)
        date2=$(basename $fnam .nc|cut -d_ -f7|cut -d\- -f2)

        year1=$(echo $date1|cut -c1-4)
        year2=$(echo $date2|cut -c1-4)
        #make sure all files have same time stamp pattern
        if [[ ${date1}-${date2} != ${year1}${mdh1}-${year2}${mdh2} ]]; then
          echo ">>> ERROR: $fnam doesn't contain full year data of ${year1}${mdh1}-${year2}${mdh2}"
          rm -f $dircmip6/.concatenating_${runid}_${ctab}_${var}
          break
        fi

        #check the continuity 
        if [[ -n $date3 ]]; then
          if [[ $date3 -gt $date1 ]]; then
            echo ">>> ERROR: chunk ${date1}-${date3} is overlapped at: "
            echo "$dircmip6/$ctab/$var/gn/$vers "
            ls -l ${fnamtype}*.nc
            rm -f $dircmip6/.concatenating_${runid}_${ctab}_${var}
            exit
          elif [[ $date3 -lt $date1 ]]; then
            #submit patch job if some year is missing
            year3=$(echo $date3|cut -c1-4)
            #while [[ $year3 -lt $year1 ]]; do
            #  echo "Patching ... $runid $year3 $ctab $var"
            #  tso.callcp $runid $year3 $ctab $var > /dev/null 2>&1
            #  year3=$((year3+1))
            #done
            #sleep 3

            #echo ">>> ERROR: chunk ${date3}-${date1} is missing at: "
            #echo "$dircmip6/$ctab/$var/gn/$vers \n"
            #ls -l ${fnamtype}*.nc
            #exit
          fi
        fi
        # date3 is next file should have
        if [[ ${mdh2} == *01010000 || $start_month -ne 01 ]]; then
          date3=$((year2+0))${mdh1} 
          #todo: this shifts stop_year to next year
        else 
          date3=$((year2+1))${mdh1}
        fi
      done

      #-----------------------------------------

      yrbeg=$(ls -1 ${fnamtype}*.nc|head -1|cut -d_ -f7|cut -d\- -f1|cut -c1-4)
      if [[ $yrbeg -le $start_year ]]; then
        ### in case of a run extended, a new range of start_time:stpo_time has been specified in the cmip6_experiment.txt
        ### user asked to concatenate the new chunk of data only
        yrbeg=$start_year
      fi
      yrend=$(ls -1 ${fnamtype}*.nc|tail -1|cut -d_ -f7|cut -d\- -f2|cut -c1-4)
      if [[ ${mdh2} == *01010000 ]]; then
        yrend=$((yrend-1)) 
      fi
      yrtot=$((yrend-yrbeg+1)) 

      if [[ $yrbeg -gt $start_year || $yrend -ne $stop_year ]]; then
        echo " ******** Some file is missing from $start_year to $stop_year:"
        ls -l ${fnamtype}*.nc
        rm -f $dircmip6/.concatenating_${runid}_${ctab}_${var}
        break
      fi

      # following 3D Omon and daily variables are concatenated into 10 years chunks 
      if [[ $ctab == *mon && $var == @(co3|co3abio|co3nat|co3satarag|co3satcalc|o2sat|ph|phabio|phnat|pathetao|prthetao|zooc|so|rsdo|dissicabio|talk|dissic|dissicnat|dissi14cabio|o2|no3|phyn|phyc|chl|thetao|agessc|sf6|obvfsq|cfc12|uo|umo|cfc11|pon|detoc|vo|vmo|wo|wmo|zmicro|zmeso|graz|phydiat|chlmisc|bfe|phyfe|phymisc|calc|chldiat|nh4|dfe) || \

            $ctab == *day && $var == @(zg|ta|hur|hus|ua|va|wap|sos|sossq|tos|tossq|cl|clw|cli|clcalipso|clisccp|jpdftaureliqmodis) || $ctab == 6hrLev ]]; then
        maxyear=$maxyear3D
      else
        maxyear=$maxyear2D
      fi

      maxfold=${maxfold00:=yes}
      if [[ $yrtot -le $maxyear ]]; then
        maxfold="non"
      fi

      ###      #split files (if necessary)
      ###      typeset -Z4 yr1 yr2 yr3
      ###      yr1=$yrbeg
      ###      while [[ $yr1 -le $yrend ]]; do
      ###        fnamlist=""
      ###        if [[ $maxfold == "yes" ]]; then
      ###          remyear=$((yr1%maxyear))
      ###          yr2=$((yr1+maxyear-remyear))
      ###        else
      ###          yr2=$((yr1+maxyear-1))
      ###        fi
      ###
      ###        if [[ ${yr2} -ge $yrend ]]; then yr2=$yrend; fi
      ###
      ###        #time stamps of split files 
      ###        yr3=$((yr2+1))    #to ensure it has 4 digits. Don't touch!
      ###        if [[ ${mdh2} == *01010000 || $start_month -ne 01 ]]; then
      ###          date3=${yr3}${mdh2}
      ###          date4=${yr3}${mdh1}
      ###        else
      ###          date3=${yr2}${mdh2}
      ###          date4=${yr3}${mdh1}
      ###        fi
      ###
      ###        for fnam in $(ls ${fnamtype}*.nc); do
      ###          date1=$(basename $fnam .nc|cut -d_ -f7|cut -d\- -f1)
      ###          date2=$(basename $fnam .nc|cut -d_ -f7|cut -d\- -f2)
      ###          
      ###          if [[ $date3 -ge $date1 && $date3 -lt $date2 ]]; then
      ###            year1=$(echo $date1|cut -c1-4)
      ###
      ###            idx=$((yr2-year1+1))
      ###            idx=$((idx*nyr))
      ###
      ###            echo "Spliting ... $dircmip6/$ctab/$var/gn/$vers/$fnam"
      ###            fnam1=$(echo $fnam | sed "s/-${date2}/-${date3}/")
      ###            fnam2=$(echo $fnam | sed "s/_${date1}/_${date4}/")
      ###
      ###            # ncks is the command should be used to split nc files, but it will modify the meta data 
      ###            # ncea is the alternative to split nc file without modifying meta data.
      ###            # in the newer nco, ncea has been renamed as nces.
      ###            ncea -h -O -F -d time,1,$idx      $fnam $fnam1
      ###            ncea -h -O -F -d time,$((idx+1)), $fnam $fnam2
      ###            rm -f $fnam
      ###          fi
      ###        done
      ###        yr1=$yr3
      ###      done

      #concatenate files
      yr1=$yrbeg
      while [[ $yr1 -le $yrend ]]; do
        fnamlist=""
        if [[ $maxfold == "yes" ]]; then
          remyear=$((yr1%maxyear))
          yr2=$((yr1+maxyear-remyear))
        else
          yr2=$((yr1+maxyear-1))
        fi
        if [[ ${yr2} -ge $yrend ]]; then yr2=$yrend; fi

        #time stamps of the chunk
        yr3=$((yr2+1))   #to ensure it has 4 digits. Don't touch!
        if [[ ${mdh2} == *01010000 || $start_month -ne 01 ]]; then
          date3=${yr1}${mdh1} 
          date4=${yr3}${mdh2}
        else
          date3=${yr1}${mdh1} 
          date4=${yr2}${mdh2}
        fi

        #build file list for each chunk
        for fnam in $(ls ${fnamtype}*.nc); do
          date1=$(basename $fnam .nc|cut -d_ -f7|cut -d\- -f1)
          date2=$(basename $fnam .nc|cut -d_ -f7|cut -d\- -f2)
          if [[ $date1 -ge $date3 && $date2 -le $date4 ]]; then
            fnamlist="$fnamlist $fnam"
          fi
        done

        #check continuity of the files in the chunk
        if [[ $(echo ${fnamlist}|wc -w) -gt 1 ]]; then
          date3=""
          for fnam in ${fnamlist}; do
            date1=$(basename $fnam .nc|cut -d_ -f7|cut -d\- -f1)
            date2=$(basename $fnam .nc|cut -d_ -f7|cut -d\- -f2)

            year1=$(echo $date1|cut -c1-4)
            year2=$(echo $date2|cut -c1-4)
      
            if [[ ${date1}-${date2} != ${year1}${mdh1}-${year2}${mdh2} ]]; then
              echo ">>> ERROR: $fnam doesn't contain full year data"
              echo "$dircmip6/$ctab/$var/gn/$vers/$fname \n"
              rm -f $dircmip6/.concatenating_${runid}_${ctab}_${var}
              exit
            fi

            if [[ -n $date3 ]]; then
              if [[ $date3 -gt $date1 ]]; then
                echo ">>> ERROR: chunk ${date1}-${date3} is overlapped at: "
                echo "$dircmip6/$ctab/$var/gn/$vers \n"
                ls -l ${fnamtype}*.nc
                rm -f $dircmip6/.concatenating_${runid}_${ctab}_${var}
                exit
              elif [[ $date3 -lt $date1 ]]; then
                echo ">>> ERROR: chunk ${date3} is missing at: "
                echo "$dircmip6/$ctab/$var/gn/$vers \n"
                ls -l ${fnamtype}*.nc
                rm -f $dircmip6/.concatenating_${runid}_${ctab}_${var}
                exit
              fi
            fi

            if [[ ${mdh2} == *01010000 || $start_month -ne 01 ]]; then
              date3=$((year2+0))${mdh1} 
            else 
              date3=$((year2+1))${mdh1}
            fi
          done
 
          #construct output filename
          outfnam=$(ls ${fnamlist} |head -1)
          outfnam=${outfnam%-*.nc}-${date2}.nc

          # determine the total size of output file     
          outsize=$(ls -l ${fnamlist} |awk -F" " 'BEGIN { sum=0 } { sum+=$5 } END { printf "%d\n", sum }')

          if [[ $outsize -le $maxsize ]]; then #concatenate
            echo "Concatenating ... $dircmip6/$ctab/$var/gn/$vers/$outfnam"

            #echo  ${fnamlist} $outfnam 
            ncrcat -h ${fnamlist} $outfnam && rm -f $fnamlist
          else
            echo ">>> ERROR: the total size of output file $outsize is larger than $maxsize "
            echo ">>> Check files in ... $dircmip6/$ctab/$var"
            rm -f $dircmip6/.concatenating_${runid}_${ctab}_${var}
            break
          fi
        fi

        yr1=$((yr2+1))
      done
    ##fi
    #remove the flag
    rm -f $dircmip6/.concatenating_${runid}_${ctab}_${var}
  done    #variable loop
  varslist=""
done    #ctablist loop

