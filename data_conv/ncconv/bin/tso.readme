
tso comprise a bunch of standalone shell scripts to do massive data conversion with pycmor, to concatenate NetCDF files, to clean up the CMIP6 directory, and to transfer data to data repository...

Command line input:
All tso scripts always take the first positional parameter as CCCma-runid, other positional parameters are optional yearlist, cmor table list, and/or variables list in any orders.
  yearlist: any sparodic years or a range of years separeted by colon :
  ctablist: any valid CMOR tables
  varslist: any parameter other than yearlist and/or ctablist are considered to a variable, even a typo
            Note that when varslist is defined, only one CMOR table is accepted

Programs:
# retrive runid related information form cmip6_experiments.txt; sort out input yearlist, ctablist and varslist on the command line
tso.runinfo runid

# call pycmor interactively (up to 20 background jobs on headnode)
tso.callcp runid [ctablist varslist yearlist] 

# call pycmor in batch mode (up to 44 OpenMP threads on computer node)
tso.callcp.batch runid [ctablist varslist yearlist] 

# concatenate NetCDF into big chunk interactively
tso.ncat runid [ctablist varslist] 

# concatenate NetCDF into big chunk in a batch job (up to 44 OpenMP threads)
tso.ncat.batch runid [ctablist varslist] 

# count the number of NetCDF files in the CMIP6 directory
tso.ncount runid [ctablist varslist] 

# clean up none-CMIP6-style files in the CMIP6 directory
tso.nclean runid [ctablist varslist] 

# transfer NetCDF files to data repository on ppp2
tso.ncxfer runid [ctablist varslist] 

Example:
tso.callcp       rc3.1-his01 Amon Omon                  # convert all variables in Amon and Omon tables of all years defined in cmip6_experiments.txt
tso.callcp       rc3.1-his01 Amon Omon Lmon 2000 2005   # convert all variables in Amon Lmon and Omon tables, but only for 2000 and 2005
tso.callcp       rc3.1-his01 Amon tas pr 2000:2005      # convert Amon tas and pr from 2000 to 2005
tso.callcp       rc3.1-his01                            # convert all variables in all cmor tables of all years defined in cmip6_experiments.txt
tso.callcp.batch rc3.1-his01                            # convert all variables in all cmor tables of all years defined in cmip6_experiments.txt in a batch job

tso.ncat         rc3.1-his01 Amon tas pr                # concatenate tas and pr in Amon into big chunk
tso.ncat.batch   rc3.1-his01 Amon tas pr                # concatenate tas and pr in Amon into big chunk in a batch job

tso.ncount       rc3.1-his01 Amon tas pr                # count how many tas and pr files
