.. Pycmor documentation master file, created by
   sphinx-quickstart on Thu Apr  4 23:40:24 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pycmor's documentation!
==================================

:code:`Pycmor` is a :code:`python` interface that works with PCMDI's Climate Model Output Rewriter, or 
`CMOR <https://github.com/PCMDI/cmor>`_, that has been written to convert CCCma's CanESM model output 
into CMIP format. This software has many moving parts, and gets information from **a lot** of 
different locations, so in addition to laying out how to use this software,  this documentation 
aims to educate users on what information is used where, and how the API works.

.. toctree::
    :maxdepth: 2
    :caption: Getting Started:

    setup_run  

.. toctree::
    :maxdepth: 2
    :caption: Further Reading:

    project_conversions
    conv_process
    optional_decks 
    nightly_blds
    compliance_checking

.. toctree::
   :maxdepth: 2
   :caption: API Reference:

   table_utils
   nmor
   amor
   answer_checks


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
