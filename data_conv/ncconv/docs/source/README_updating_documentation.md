### How to update the pycmor documentation

Activate the env that has sphinx:

    conda activate /home/rcs001/miniconda3/envs/sphinx-ncconv

(Or `source activate`, depending on conda version.)

Then navigate to the location of the documentation source files in the `ncconv` repo, `docs/source`, and edit the source files as needed.

Finally, build the documentation (still in `docs/source` dir):

    make html

The `docs/source/_build/html` dir then contains the `.html` files created by Sphinx that can be viewed in a web browser, e.g. by linking `docs/source/_build/html` to a `public_html` dir).
