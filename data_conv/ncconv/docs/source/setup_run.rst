
Environment Setup
-----------------
Prior to doing anything, you must clone the repository! To do so run 

.. code-block:: bash

    git clone --recursive -b pycmor git@gitlab.science.gc.ca:CanESM/ncconv.git

After this, in order to actually run the desired code, you need to use a specific :code:`anaconda`
environment. Luckily we have recently implemented a CCCma wide ``conda`` install. 
To gain access to this install, following the directions `here <https://gitlab.science.gc.ca/CanESM/Python_Envs>`_
to add ``conda`` to your ``$PATH`` variable (you have to be signed into the gitlab.science to view the page); 
once you do, you shoud be able to run

.. code-block:: bash

    conda info -e

and see the desired ``anaconda`` environment, ``py2_cmor_v1``. To activate this environment, run

.. code-block:: bash

    source activate py2_cmor_v1

and you should be good to go!

Running
-------

.. warning::

    Before running a conversion, it is worth noting that the :code:`HEAD` of the :code:`pycmor` 
    branch is considered "bleeding edge", and is therefore not guaranteed to work as expected. 
    As a result, it is recommended that users make use of a tagged version of :code:`pycmor`.
    To get a list of tags, run :code:`git tag` within the repository, and then 
    :code:`git checkout TAGNAME`, where :code:`TAGNAME` is the most recent tag.

Running :code:`pycmor` is as simple as following these steps:

1. After cloning the repo and activating the :code:`py2_cmor_v1` environment, navigate into the 
   top level of the repo, and configure/setup a working directory by executing

    .. code-block:: bash
        
        pycmor/config-pycmor
   
   and navigate into the resulting directory.

.. warning:: 
    
    When using the :code:`pycmor` code to execute a conversion, the working directories
    are known to take up considerable amounts of disc space, so it is therefore recommended 
    that you set up a working directory in a location with significant storage,
    like :code:`$RUNPATH` or better yet, :code:`$CCRNTMP`.

    The above :code:`config-pycmor` script can be ran from any where, and will set up 
    the working directory in what ever directory it is called from. Another option
    is to explicity tell it where you want to set up the directory, by providing it
    as an argument, i.e: :code:`/path/to/config-pycmor /path/to/my/dir`

2. Run :code:`source path_export.sh`, which adds necessary locations to your :code:`$PATH` variable.

3. Open :code:`callcp.sh`, and edit the following to satisfy your needs:

  * :code:`runid` -> CCCma runid of the desired run
  * :code:`user`  -> the user account used to run the afformentioned :code:`runid`
  * :code:`chunk` -> the desired date range in :code:`YYYY_YYYY` format
  * :code:`outpath` -> the directory where the output will appear
  * :code:`project` -> name of the project that defines the CMOR tables to be used (for example, :code:`project='CMIP6'`)
  * :code:`cmor_table` -> the CMOR table of interest (generally depends on the :code:`project`)
  * **instead of the entire table, if you wish to limit the conversion to a subset of variables**, 
    add :code:`-c 'var1 var2 var3 ...'` to the :code:`pycmor.py` call.

4. Finally, run :code:`./callcp.sh` to launch the conversion.

Inspecting the Log Files
------------------------
After running a conversion, :code:`pycmor` produces a large number of log files that can be inspected
to see how the conversion progressed. Below is a list of potential log files, along with a brief 
description of what is contained in each:

* :code:`pycmor-driver-log_RUNID_CMORTABLE.log`:
   
   * This file captures *most* of the output produced by the code and general gives a short message about 
     what happened for each variable. This file is written sequentially and can be quite large/hard to parse, 
     but the most important information comes at the end, where a stats section is output that informs the 
     user about things like how many variables were converted successfully, or how many failed in 
     optional decks.

* :code:`pycmor_logs/CMORTABLE/`:
  
  Files created in this directory contains more detailed and organized information about failed conversions.
  A good practice would be to :code:`grep` through these logs in order to find which contains information
  on the variable of interest.

   * :code:`table_def_errors.log`
   
      * lists the variables that failed out due because the :code:`CCCma TS var name` field was empty, or 
        an unrecognized :code:`modeling_realm` was noted.

   * :code:`optional_deck_errors.log`

      * lists variables that failed out due to errors in executing optional decks - also provides error 
        output to help guide the user.

   * :code:`cmor_errors.log`

      * lists variables that failed within :code:`CMOR` routines and provides the :code:`CMOR` output. 
        Note that this output is generally pretty generic, and users are encouraged to look at the 
        ``stderr``/``stdout`` from ``CMOR`` for additional information, which will typically be sent to 
        files within ``./cmor_logs/CMORTABLE/``. 

   * :code:`not_found.log`
    
      * lists variables where the input files couldn't be found.

   * :code:`nodiag_nocheck.log`
     
      * lists variables with nothing in the :code:`CCCma diag file` column, yet with no confirmation 
        text in :code:`CCCma check`.

   * :code:`pycmor_errors.log`

      * lists variables that broke due to non :code:`CMOR` exceptions, and the error text/traceback associated
        with them. These errors are often associated with software problems internal to :code:`pycmor` itself. 
        Users are encouraged to view these logs, but if they can't make heads or tails of it, they should 
        contact someon on the :code:`pycmor` development team.

* :code:`cmor_logs/CMORTABLE/`:

   * individual log files, produced by the :code:`CMOR` software, for each variable that caused an 
     exception or warning within :code:`CMOR` routines.

     .. note:: 

        These files are only produced if the used ``pycmor.py`` sets ``use_cmorvar_logfile = True`` in 
        its call to ``table_utils.convert_table()``. If this is not the case, the ``stderr``/``stdout`` 
        for ``CMOR`` will be written to the users ``stderr``/``stdout``.


Variable Tables
---------------

Each ``project`` has a set of **CMOR tables** that define its requested variables, and a corresponding set of **CCCma tables** maps these requested variables to CCCma model output variables. 
Reasons why a user may need to modify the **CCCma tables** include:

* Implementing conversion of a requested variable, by specifying which CCCma model output variable corresponds to the requested variable.

* Define additional postprocessing that may be required before netcdf conversion, by specifying an :doc:`optional deck <optional_decks>`.

* Modifying the conversion of a requested variable, such as to update it or fix errors.

**CMOR tables** are typically not modified by users, as these tables typically specify the data request for an official project (such as CMIP).
There may be reason to modify these on a case-by-case basis, such as when the project is a user-defined (unofficial) one, or to fix errors, or to update an project's CMOR tables to a more recent official version.
*Modifying the CMOR tables changes the formatting and metadata attributes that appear in the output netcdf files.*

  .. warning::

    For official projects such as CMIP, it is **not recommended** to modify the project CMOR tables.
    Doing so could create inconsistencies in the data provided by different modelling centres, undermining the scientific validity of multi-model analyses.
    Allowing them for a particular project should be a group decision, and if there is good justification for such modifications then they should be documented carefully.

For a given ``project``, the project directory contains the CMOR tables in its :code:`cmor-tables` folder and the CCCma tables, which are model-specific, in its :code:`cccma-tables/[model name]` folders, where :code:`source_id` identifies the model (e.g., :code:`cccma-tables/CanESM5` contains CCCma tables for ``CanESM5``).
Both sets of tables are :code:`.json` files.
For further explanation, including **how to set up a new project or add variables**, see :doc:`project-specific conversions <project_conversions>`.

  .. note::
    Earlier versions of ``pycmor`` used a `google spreadsheet <https://docs.google.com/spreadsheets/d/1JLsCcp9CCP-39ZE5OCN0xaerMwLe4EvmYj-a4-6KWjM/edit>`_ to edit the CCCma tables.
    The script :code:`tools/get_ggle_ss.py` was used to bring changes from the spreadsheet into the CCCma table :code:`.json` files.
    The spreadsheet is now deprecated and the :code:`.json` files, which are version-controlled in the ``ncconv`` repo, should be edited directly.
