#!/bin/bash
#   rebuild_ocean_tiles.tsk
if (( with_rbld_nemo == 1 )); then
    #~~~~~~~~~~~~~~~~~~~~~~
    # Set static variables
    #~~~~~~~~~~~~~~~~~~~~~~
    months_gcm=${months_run} # defines how big the model chunk size was

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Check if we need to rebuild the initial restart files
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if [[ $nemo_from_rest == 'off' && $job_start_date == $run_start_date ]]; then
        # need to rebuild the files from the initial restart
        #   TODO: assess if we need this anymore
        echo "Rebuilding ocean tiles from the initial restart"
        (
            canesm_nemo_rbld_save_hist=${canesm_nemo_rbld1st_save_hist};    \
            mon=$(( run_start_month - months_gcm + 12 ));                   \
            year=$(( run_start_year - 1 ));                                 \
            mon=$(pad_integer $mon 2);                                      \
            year=$(pad_integer $year 4);                                    \
            model1="mc_${runid}_${year}_m${mon}";
            source $CANESM_SRC_ROOT/CanNEMO/lib/nemo_rebuild.sh
        )
    fi

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Rebuild the files produced by the associated model chunk(s)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Note:
    #   - we need to run the repacking for each model chunk
    num_model_chunks_per_post_proc=$(( POSTPROC_CHUNK_SIZE / months_gcm ))
    int_year=${job_start_year}
    for n in $(seq ${num_model_chunks_per_post_proc}); do
        int_mon=$(( (n-1) * months_gcm + job_start_month ))
        if (( int_mon > 12 )); then
            int_mon=$((int_mon - 12))
            int_year=$((job_start_year + 1))
        fi
        (
            mon=$(pad_integer $int_mon 2);          \
            year=$(pad_integer $int_year 4);        \
            model1="mc_${runid}_${year}_m${mon}";   \
            mkdir tmp_ocean_rebuild_${mon}_${year};  \
            cd tmp_ocean_rebuild_${mon}_${year};     \
            source $CANESM_SRC_ROOT/CanNEMO/lib/nemo_rebuild.sh
        )
    done
else
    echo "Skipping ocean rebuilding as with_rbld_nemo != 1"
fi
