#!/bin/bash
set -e
#   Simple setup script for the maestro sequencer to be used as part of the given run
#   
#   TODO: make a proper interface for this tool and make the SEQUENCING_STRG_DIR optional
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Define Usage and Other Functions
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function usage(){
    echo "Setup a maestro suite to run CanESM, in the given DESTINATION_DIR"
    echo ""
    echo "$(basename $0): MAESTRO_SRC_DIR DESTINATION_DIR SEQUENCING_FILE_DIR RUNID RUN_ENV_FILE"
    echo ""
    echo "  MAESTRO_SRC_DIR     : the repo containing the suite code"
    echo "  DESTINATION_DIR     : where the run sequencer code will be placed"
    echo "  SEQUENCING_FILE_DIR : directory to store the temporary maestro sequencing files"
    echo "  RUNID               : the ID for the run in question"
    echo "  RUN_ENV_FILE        : the file that will be used to setup the environment for the given run"
    echo ""
}
function create_modules_directory(){
    #   Using the version controlled 'modules' directory, create a mirrored
    #   structure outside of VCS (in the current working dir) that has the same
    #   directory structure but creates soft links to all files
    #   (non-directories) within the version controlled structure.
    #
    #   This allows VCS to track changes to the various *.cfg/*.tsk files,
    #   while allowing run-unique flow.xml files, which define the dependencies
    
    local _maestro_source_dir=$1
    local _vcs_modules_dir
    local _vcs_files
    local _dirs_within_vcs_modules_dir
    local _path2dir
    local _path2dir_relative2modules
    local _path2file
    local _path2file_relative2modules
    local _filename

    mkdir modules

    # --- Setup up directory structure
    # get a list of absolute dir paths within the source dir
    _vcs_modules_dir=${_maestro_source_dir}/modules
    _dirs_within_vcs_modules_dir=$(find ${_vcs_modules_dir} -type d -not -path "${_vcs_modules_dir}")

    # create local dirs within modules
    for _path2dir in ${_dirs_within_vcs_modules_dir}; do
        # get relative path WITHIN modules dir
        _path2dir_relative2modules=${_path2dir#${_vcs_modules_dir}/}

        # create a local one!
        mkdir -p modules/${_path2dir_relative2modules}
    done

    # --- Link in version controlled files
    _vcs_files=$(find ${_vcs_modules_dir} -type f)
    for _path2file in ${_vcs_files}; do
        _path2file_relative2modules=${_path2file#${_vcs_modules_dir}/}
        _filename=$(basename ${_path2file_relative2modules})
        if [[ "${_filename}" == .* ]]; then
            # skip hidden files
            continue
        fi

        # create links!
        ln -s ${_path2file} modules/${_path2file_relative2modules}
    done
}

#~~~~~~~~~~~~~~~~~
# Check Arguments
#~~~~~~~~~~~~~~~~~
number_of_arguments=$#
if (( number_of_arguments != 5 )); then
    >&2 echo "$(basename $0): expects exactly four arguments!"
    usage
    exit 1
fi

maestro_source_dir=$1
destination_dir=$2
temp_sequencing_file_dir=$3
runid=$4
run_env_file=$5
if ! [[ -d $maestro_source_dir ]]; then
    >&2 echo "$(basename $0): $maestro_source_dir doesn't exist!"
    exit 1
fi
if [[ -d $destination_dir ]]; then
    >&2 echo "$(basename $0): $destination_dir already exists! Clean up before running!"
    exit 1
fi

#~~~~~~~~~~~~~~~~~
# Setup Directory
#~~~~~~~~~~~~~~~~~
# Note that the resources directory is setup at configuration time
if ! [[ -d $temp_sequencing_file_dir ]]; then
    mkdir $temp_sequencing_file_dir
fi
mkdir -p $destination_dir
export SEQ_EXP_HOME=$destination_dir
cd $destination_dir
mkdir hub logs stats listings
ln -s $temp_sequencing_file_dir sequencing
create_modules_directory $maestro_source_dir
ln -s modules/canesm EntryModule
ln -s ${maestro_source_dir}/defaults .
cp ${maestro_source_dir}/experiment.cfg .
makelinks -f --experiment=${runid}

# Add SEQ_EXP_HOME to the given environment file
[[ ! -f $run_env_file ]] && echo "Warning: creating brand new environment file $run_env_file"
echo "# Maestro Specific Variables!" >> $run_env_file
echo "SEQ_EXP_HOME=$SEQ_EXP_HOME ; export SEQ_EXP_HOME" >> $run_env_file
