#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Clean temporary loop files for the housing loop container, within maestro
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#   Note: 
#       - if our machines can't see the same file system, we'll need to
#           rethink this
if (( MAESTRO_CLEAN_TMPDIRS == 1 )) && (( loop_index > MAESTRO_CLEANUP_LAG )); then
    # find files
    chunk_to_delete=$(( loop_index - MAESTRO_CLEANUP_LAG ))
    dirs_to_delete=$( find ${SEQ_WORKBASE}${SEQ_CONTAINER} -type d -name "*+${chunk_to_delete}" )
    # find listings files
    SEQ_LISTINGSBASE="${SEQ_WORKBASE%/work/*}/listings/"
    listings_dirs_to_delete=$( find ${SEQ_LISTINGSBASE} -type f -name "*+${chunk_to_delete}.*" )

    # navigate out and delete
    cd $SEQ_EXP_HOME
    rm -rf $dirs_to_delete
    rm -rf $listings_dirs_to_delete
fi
