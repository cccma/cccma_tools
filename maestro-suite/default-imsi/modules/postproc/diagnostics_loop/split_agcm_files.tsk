#!/bin/bash
#   split_agcm_files.tsk
if (( with_time_series == 1 )); then

    #~~~~~~~~~~~~
    # Set inputs
    #~~~~~~~~~~~~
    # The main inputs for this are determined from 'diag2ts_prefix_list' and  'diag2ts_suffix_list', where
    #
    #   diag2ts_prefix_list -> a colon-delimited list of filetypes
    #                           (where filetypes are defined by their prefix)
    #   diag2ts_suffix_list -> a colon-delimited list of sub-lists of suffixes to split for each
    #                           filetype entry in diag2ts_prefix_list
    #
    #   Example:    diag2ts_prefix_list="dc:mc"
    #               diag2ts_suffix_list="gp zp tp dd dp 6h 1h dx:3h"
    #       means that for the diagnostic filetypes (dc), it will split the 'gp zp tp dd dp 6h 1h dx' file, and
    #       for the model filetypes (mc), it will split the 3h files.
    #
    #   Note: 
    #       - spltdiag looks to have been written to initially work on diagnostic files, so many of the
    #           input vars reference 'diags' even though it splits other filetypes. Additionally it only
    #           operates on one filetype (prefix) at a time, so we loop and run the script multiple times
    #       - it is not required that the prefix list only contain unique entries

    # create arrays of the suffix/prefix lists to allow for looping
    tmp_suffix_list=${diag2ts_suffix_list// /-}     # replace potential spaces with '-' to avoid confusing the array construction
    prefix_array=( ${diag2ts_prefix_list//:/ } )
    suffix_list_array=( ${tmp_suffix_list//:/ } )
    num_filetypes=${#prefix_array[@]}
    num_suffix_lists=${#suffix_list_array[@]}
    if (( num_filetypes != num_suffix_lists )); then
        bail "split_agcm_files: diag2ts_prefix_list must have equal numbers of entries (using ':' as the delimiter)!"
    fi

    # define the output prefix
    #   TODO: the cccjob system allows this to be overwritten, but it appears to be never used. Assess if this is needed
    mts_uxxx="sc"

    # set input time vars
    time_start_year=$(pad_integer $job_start_year 4)
    time_start_month=$(pad_integer $job_start_month 2)
    time_stop_year=$(pad_integer $job_stop_year 4)
    time_stop_month=$(pad_integer $job_stop_month 2)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Run the splitting script
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~
    for (( i = 0; i < $num_filetypes; i++)); do
        filetype_specific_suffix_list=${suffix_list_array[i]}
        filetype_specific_suffix_list="${filetype_specific_suffix_list//-/ }"
        filetype=${prefix_array[i]}
        tmp_work_space=tmp_${filetype}_$$
        mkdir -p $tmp_work_space # use -p in case this filetype has already been operated on

        # using a self-contained sub-shell start the splitting of
        #   each filetype in a sub directory
        #   TODO: assess parallelizing this by putting the subshell execution into the background
        #           - we'd need to add error handling/logging
        (   cd $tmp_work_space;                                 \
            diag_uxxx=$filetype;                                \
            diag_suffix_list=$filetype_specific_suffix_list;    \
            source spltdiag.sh  )
    done
else
    echo "Skipping AGCM time series creation as with_time_series != 1"
fi
