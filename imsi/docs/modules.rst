.. _imsiAPI:

The imsi API
================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

This section describes the :program:`imsi` Application Programming Interface 
(API). 


Command line interface (cli.py) and link to backend (ui_manager.py)
--------------------------------------------------------------------

.. automodule:: imsi.cli
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.user_interface.ui_manager
   :members:
   :undoc-members:
   :show-inheritance:

Configuration manager (config_manager.py)
----------------------------------

.. automodule:: imsi.config_manager.config_manager
   :members:
   :undoc-members:
   :show-inheritance:

Shell interface 
--------------------

.. automodule:: imsi.shell_interface.shell_interface_manager
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.shell_interface.shell_comp_environment
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.shell_interface.shell_config_parameters
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.shell_interface.shell_diag_parameters
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.shell_interface.shell_inputs_outputs
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.shell_interface.shell_interface_utilities
   :members:
   :undoc-members:
   :show-inheritance:

Scheduler interface 
--------------------

.. automodule:: imsi.schedulers.schedulers
   :members:
   :undoc-members:
   :show-inheritance:

Sequencer interface
-------------------

.. automodule:: imsi.sequencer_interface.sequencers
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.sequencer_interface.iss
   :members:
   :undoc-members:
   :show-inheritance:

Simple shell sequencer (sss.py)
-----------------------------

.. automodule:: imsi.sequencer_interface.sss
   :members:
   :undoc-members:
   :show-inheritance:

Utility functions 
-----------------

.. automodule:: imsi.utils.general
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.utils.dict_tools
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.utils.nml_tools
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: imsi.utils.git_tools
   :members:
   :undoc-members:
   :show-inheritance: