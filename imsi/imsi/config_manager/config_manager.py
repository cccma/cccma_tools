"""This module provides classes and utilities for managing configurations,
   including models, experiments, machines, compilers, and their interactions.

   The module includes classes for defining configuration elements such as
   Model, Experiment, Machine, and Compiler. Additionally, it provides a
   Configuration class for composing these elements into a complete configuration.

   ConfigManager is a class used to establish configuration objects and
   facilitate saving and loading configurations for later use. The module also
   includes abstract classes such as ConfigDatabase for defining an abstract
   interface for a configuration database and its concrete implementation JsonConfigDatabase.

   Lastly, the module contains Factory classes such as MachineFactory and CompilerFactory,
   which provide methods for creating instances of Machines and Compilers respectively,
   and applying machine-specific compiler options.

   The module is designed to support flexibility in configuration management,
   allowing configurations to be composed, serialized, and deserialized.

   This module is a WIP. It is next required to create infrastructure
   modules that use the configurations created here.

   Neil Swart, April 2024
"""
# TODO:
#   - Add a setup / user config (selections incoming from setup / subsequent "config" calls)
#   - Perhaps represent/process shell_parameters more explicitly

from abc import ABC, abstractmethod
import collections
from typing import Dict, Any
from dataclasses import dataclass, asdict
import copy
import json
import re
import socket
import os
from imsi.utils.dict_tools import combine_configs, parse_config_inheritance, update, recursive_lookup, replace_curlies_in_dict

@dataclass
class Experiment:
    '''Model experiment configuration dataclass
    '''
    name: str
    parameters: dict

@dataclass
class Model:
    '''Model configuration dataclass
    '''
    name: str
    parameters: dict

@dataclass
class Machine:
    '''Machine configuration dataclass'''
    name: str
    parameters: dict
    # This exists because it is required by CompilerFactory
    # However, of course there are many other elements of `parameters`
    # So whether this is an OK approach, or if the actual contents of
    # parameters should be more explicitly resolved as class variables
    # is a good question (also see below).
    def get_default_compiler(self) -> str:
        """Retrieve the default compiler from the machine configuration"""
        compilers = self.parameters.get('compilers', [])
        if not compilers:
            raise ValueError("No compilers found in machine configuration")
        return compilers[0]
    
    def get_default_sequencer(self) -> str:
        """Retrieve the default compiler from the machine configuration"""
        sequencers = self.parameters.get('sequencers', [])
        if not sequencers:
            raise ValueError("No seqeuncers found in machine configuration")
        return sequencers[0]
    
    def get_default_sequencing_suffix(self) -> str:
        """Retrieve the default sequencing suffix from the machine configuration"""
        flow = self.parameters.get('default_sequencing_suffix')
        if not flow:
            raise ValueError("No default_sequencing_suffix found in machine configuration")
        return flow   

@dataclass
class Compiler:
    '''Compiler configuration dataclass
    '''
    name: str
    parameters: dict

@dataclass
class PostProcessing:
    '''Post-processing configuration dataclass
    '''
    # Current this is just a big dict, ingested straight from
    # what is in the database. Almost certainly though it will
    # need to be developed to make it more useful.
    # Likely, these settings need to also appear and be modifiable within
    # experiment definitions (ala components).
    parameters: dict

@dataclass
class Sequencing:
    '''Sequencing configuration dataclass
    '''
    parameters: dict

@dataclass
class SetupParams:
    '''Variables defined at setup, e.g. runid and paths, e.g. wrk_dir
    '''
    parameters: dict

@dataclass
class Utilities:
    '''For configuring "external" utilities
       e.g. providing a dict of utlity scripts to extract from the repo
    '''
    parameters: dict

class ConfigDatabase(ABC):
    '''Defines an abstract interface for a config DB
       This might be useful if in the future different DBs could be used.
       For instance there could be an sqlite or xml based db instead of json, 
       which is why the ConfigDatabase abstraction is in place, allowing future
       flexibility (Open for expansion, closed for change principle)

       The interface defines two "get" methods which return a dictionary of the 
       config      
    '''
    @abstractmethod
    def get_config(self, config_name: str) -> Dict:
        '''Returns all entries under a configuration category, for example
        if config_name="machines", it would return a dict containing the configurations
        for all machines in the library (which machine names as keys)'''
        pass

    @abstractmethod
    def get_parsed_config(self, config_name: str, config_key: str) -> Dict:
        '''Returns a specific named entry from a specific configuration category.
        For example if config_name="machines", and config_key="ppp6", it would \
        return a dict containing the machine configuration for ppp6'''
        pass

class JsonConfigDatabase(ConfigDatabase):
    '''A specific implementation of a ConfigDatabase, which reads the inputs from a hierachy
       of json files. 
    '''
    def __init__(self, imsi_config_path):
        self.imsi_config_path = imsi_config_path
        self.database = self._load_config()

    def _load_config(self) -> Dict:
        '''Parses the imsi_config_path for all json files and combines
        them into one big dict
        '''
        if not os.path.exists(self.imsi_config_path):
            raise FileNotFoundError(f'Could not find imsi config directory at: {self.imsi_config_path}')

        return combine_configs(self.imsi_config_path)

    def get_config(self, config_name: str) -> Dict:
        '''Returns all entries under a configuration category, for example
        if config_name="machines", it would return a dict containing the configurations
        for all machines in the library (which machine names as keys)'''
        if config_name not in self.database:
            raise NameError(f'{config_name} not found in database created from {self.imsi_config_path}')
        return self.database[config_name]

    def get_parsed_config(self, config_name: str, config_key: str) -> Dict:
        '''Returns a specific named entry from a specific configuration category.
        For example if config_name="machines", and config_key="ppp6", it would \
        return a dict containing the machine configuration for ppp6'''
        config_realm_data = self.get_config(config_name)
        if config_key not in config_realm_data:
            raise NameError(f'{config_key} not found in {config_name} in the configuration '
                            f'database created from {self.imsi_config_path}')
        return parse_config_inheritance(config_realm_data, config_key)
class MachineFactory:
    '''A class to create instances of Machines
       Implemented to encapsulate the several methods required,
       this keeping the Machine class itself simple.
    '''
    @staticmethod
    def create_from_database(db: ConfigDatabase, machine_name: str = None):
        machine_name = machine_name or MachineFactory.get_machine_name_from_hostname(db)
        machine_data = db.get_parsed_config('machines', machine_name)
        return Machine(name=machine_name, parameters=machine_data)

    @staticmethod
    def get_machine_name_from_hostname(db: ConfigDatabase) -> str:
        machine_configs = db.get_config('machines')
        return MachineFactory.find_matching_machine(machine_configs)

    @staticmethod
    def find_matching_machine(machine_configs: dict) -> str:
        hostname = MachineFactory.get_hostname()
        for machine_name, config in machine_configs.items():
            if re.search(config['nodename_regex'], hostname):
                return machine_name
        raise NameError(f'Could not find any IMSI-supported machine matching hostname {hostname}')

    @staticmethod
    def get_hostname() -> str:
        try:
            return socket.gethostname()
        except OSError as e:
            raise ValueError(f"Could not determine the hostname: {e}")
class CompilerFactory:
    '''Class containing methods to instantiate an instance of Compiler.
    Like for machines, several parsing functions are required and encapsulated here.
    '''
    @staticmethod
    def create_from_database(db: ConfigDatabase, machine: Machine, compiler_name: str = None):
        compiler_name = compiler_name or CompilerFactory.get_default_compiler(machine)
        compiler_configs = db.get_config('compilers')
        if compiler_name not in compiler_configs.keys():
            valid_compilers = ','.join(compiler_configs.keys())
            raise NameError(f'Compiler {compiler_name} not in compiler list ({valid_compilers})')
        
        compiler_config = db.get_parsed_config('compilers', compiler_name)
        compiler_config = CompilerFactory.apply_machine_specific_compiler_opts(compiler_config, machine.name)
        compiler_config = CompilerFactory.process_compiler_appends(compiler_config)
        return Compiler(name=compiler_name, parameters=compiler_config)

    @staticmethod
    def get_default_compiler(machine: Machine) -> str:
        """Retrieve the default compiler from the machine configuration"""
        return machine.get_default_compiler()
    
    @staticmethod
    def apply_machine_specific_compiler_opts(compiler_config: dict, machine_name: str) -> dict:
        """Apply machine-specific compiler options to the compiler configuration.

        This function applies machine-specific compiler options to the given compiler
        configuration dictionary based on the specified machine name. If machine-specific
        overrides are found in the compiler configuration for the given machine name, they
        are applied to the compiler configuration dictionary, and the 'machine_specific'
        key is removed from the resulting dictionary.

        Parameters:
            compiler_config (dict): The compiler configuration dictionary.
            machine_name (str): The name of the machine for which machine-specific
                compiler options are to be applied.

        Returns:
            dict: The compiler configuration dictionary with machine-specific options
            applied, if available.

        Example:
            compiler_config = {
                'fortran': {
                    'FC': 'ifort',
                    'FFLAGS': {
                        'DEFAULT': ['-O2'],
                    },
                },
                'machine_specific': {
                    'my_machine': {
                        'fortran': {
                            'FFLAGS': {
                                'DEFAULT': ['-O3', '-xHOST'],
                            },
                        },
                    },
                },
            }
            machine_name = 'my_machine'
            applied_config = apply_machine_specific_compiler_opts(compiler_config, machine_name)
            # applied_config['fortran']['FFLAGS']['DEFAULT'] would now contain ['-O3', '-xHOST']
        """
        if 'machine_specific' in compiler_config:
            machine_specifics = compiler_config['machine_specific']
            if machine_name in machine_specifics:
                machine_specific_overrides = parse_config_inheritance(machine_specifics,machine_name) # I think no deep copy need as compiler_config is already a copy (not db original)
                compiler_config = update(compiler_config, machine_specific_overrides)
                del compiler_config['machine_specific']
        return compiler_config

    @staticmethod
    def process_compiler_appends(compiler_config: dict) -> dict:
        """Process compiler-specific append options in the compiler configuration.

        This function iterates over the compiler configuration dictionary to find
        fields that have the 'APPEND' keyword. It appends the values specified under
        'APPEND' to the relevant list in the configuration, if the key is present.
        This functionality exists, because the 'machine_specific' compiler
        flags dictionaries might start with an `APPEND` key, indicating the machine 
        specific options should be appended to the default options, rather than wholesale
        replacing them (which occurs if the dict is defined without starting with the
        APPEND key)

        Parameters:
            compiler_config (dict): The compiler configuration dictionary to be processed.

        Returns:
            dict: The modified compiler configuration dictionary with appended values.

        Notes:
            This function is specifically designed to handle compiler-specific append
            options in the configuration. It loops through the structure and looks for
            fields that contain the 'APPEND' keyword, then appends the specified values
            to the relevant lists. This approach helps avoid repetition in the JSON
            configuration files and provides flexibility in configuring compiler options.

        Example:
            compiler_config = {
                'fortran': {
                    'FC': 'ifort',
                    'FFLAGS': {
                        'DEFAULT': ['-O2'],
                        'APPEND': {
                            'DEFAULT': ['-g'],
                            'OPENMP': ['-fopenmp']
                        }
                    }
                }
            }
            processed_config = process_compiler_appends(compiler_config)
            # The 'FFLAGS' list for 'DEFAULT' would now contain ['-O2', '-g'],
            # and the 'FFLAGS' list for 'OPENMP' would contain ['-fopenmp'].
        """
        for language, lang_config in compiler_config.items():
            if isinstance(lang_config, collections.abc.MutableMapping):
                for option, values in lang_config.items():
                    if isinstance(values, collections.abc.MutableMapping):
                        option_keys = values.keys()
                        if "APPEND" in option_keys:
                            for k, v in values['APPEND'].items():
                                if k in option_keys:
                                    values[k] += v
                                elif "DEFAULT" in option_keys:
                                    values['DEFAULT'] += v
                                else:
                                    print(f"Nowhere found to append {k}")
                            del values['APPEND']
        return compiler_config

class SequencingFactory:
    '''Class containing methods to instantiate an instance of Sequencing.
    Like for machines, several parsing functions are required and encapsulated here.
    '''
    @staticmethod
    def create_from_database(db: ConfigDatabase, machine: Machine, sequencer_name: str, flow_name: str = None,
                             model_config: str = None):
        '''Create a sequencing instance given the config db, the machine, sequencer and flow names'''

        sequencing_config = {} # To build up here
        sequencing_configs = db.get_config('sequencing') # Everything from the DB
        # Validity checks
        SequencingFactory.verify_sequencing_structure(sequencing_configs)

        # Run dates
        sequencing_config['run_dates'] = sequencing_configs['run_dates']

        # This is the sequencer specific content (encapsulate in a function)
        sequencing_config['sequencer'] = SequencingFactory.set_sequencer_config(
                                                                    sequencer_name=sequencer_name, 
                                                                    sequencers=sequencing_configs['sequencers'], 
                                                                    machine_name=machine.name
                                                                    )

        sequencing_config['sequencing_flow'] =  SequencingFactory.set_flow_config(
            flow_name=flow_name, 
            flows=sequencing_configs['sequencing_flow'], 
            sequencer_name=sequencer_name, 
            sequencer_config=sequencing_config['sequencer'],
            machine=machine,
            model_config=model_config)

        return Sequencing(parameters=sequencing_config)
   
    @staticmethod
    def verify_sequencing_structure(sequencing_config: dict):
        '''
        Checks what we got from the db includes mandatory sections
        (replaceable by schema validation?)
        '''
        required_keys =["run_dates", "sequencing_flow", "sequencers"]
        for key in required_keys:
            if key not in sequencing_config.keys():
                raise KeyError(f"The key {key} is not in the 'sequencing' configuration "
                                "provided, but is a required element")

    @staticmethod
    def determine_default_flow(model_config: str, sequencer_name: str, 
                               sequencer_baseflows: dict, flows, machine: Machine):
        """Get the default sequencing flow, which handles the selected model_config, and
           also has configuration support for the selected sequencer and machine.

           Parameters:
            model_config (str): The model configuration, e.g. ESM, AMIP or OMIP
        """
        if not model_config in sequencer_baseflows:
            supported_model_configs = ', '.join(sequencer_baseflows.keys())
            raise KeyError(f"The selected model configuration: {model_config} is not supported by "
                           f"available workflows of the selected sequencer: {sequencer_name}. "
                           f"Supported model configurations are: {supported_model_configs}")
        
        # All baseflows for this sequencer, and this model_config
        base_flows = sequencer_baseflows[model_config].keys()

        # The platform/machine specific implementation of baseflows is denoted with a suffix on baseflow
        platform_sequencing_default_suffix = SequencingFactory.get_default_sequencing_suffix(machine)
        for baseflow in base_flows:
            machine_specific_flow = f"{baseflow}-{platform_sequencing_default_suffix}"
            if machine_specific_flow in flows:
                return machine_specific_flow
        supported_base_flows = ', '.join(base_flows)
        raise KeyError(f"Could not determine a default sequencing flow for sequencer: {sequencer_name} "
                       f"on machine: {machine.name}/{platform_sequencing_default_suffix} that supports model_config: {model_config}. "
                       f"Support baseflows found for {model_config} are: {supported_base_flows}. "
                       f"Machine specific version(s) for {platform_sequencing_default_suffix} are "
                        "not implemented but required."
                       )
        
    @staticmethod
    def get_default_sequencing_suffix(machine: Machine):
        """Get the default sequencer defined in the machine config??"""
        return machine.get_default_sequencing_suffix()
        
    @staticmethod
    def get_default_sequencer(machine: Machine):
        """Get the default sequencer defined in the machine config??"""
        return machine.get_default_sequencer()
    
    @staticmethod
    def set_sequencer_config(sequencer_name: str, sequencers: dict, machine_name: str):
        # This is the sequencer specific content (encapsulate in a function)
        if sequencer_name not in sequencers:
            supported_sequencers = ', '.join(sequencers.keys())
            raise KeyError(f"Selected sequencer {sequencer_name} not in list of configured sequencers {supported_sequencers}")
        
        sequencer_config = parse_config_inheritance(sequencers, sequencer_name)

        # Validation
        if "supported_machines" not in sequencer_config:
            raise KeyError(f"No 'supported_machines' field in sequencer definition for {sequencer_name}")
        if machine_name not in sequencer_config.get("supported_machines"):
            supported_machines = ', '.join(sequencer_config.get("supported_machines")) 
            raise KeyError(f"Machine {machine_name} not listed as supported by sequencer {sequencer_name}. " 
                           f"Supported machines for {sequencer_name} are {supported_machines}. "
                            "Either change machine or sequencer.")
        if not sequencer_config or "baseflows" not in sequencer_config:
            raise ValueError(f"The sequencer config for {sequencer_name} does not contain a 'baseflows' definition")
        
        return sequencer_config
    
    @staticmethod
    def set_flow_config(flow_name: str, flows: dict, sequencer_name: str, 
                        sequencer_config: dict, machine: Machine, model_config: str):
        # This is the flow specific content

        flow_name = flow_name or SequencingFactory.determine_default_flow(model_config, sequencer_name, 
                               sequencer_config.get('baseflows'), flows, machine)

        if flow_name not in flows:
            supported_flows = ', '.join(flows.keys())
            raise KeyError(f"Selected sequencing flow {flow_name} not in list of configured flows:"  
                           f"{supported_flows}")
        flow_config = parse_config_inheritance(flows, flow_name)

        # Validation
        if not flow_config and "base_flow" in flow_config:
            raise ValueError("The flow configuration for flow {flow_name} is not valid "
                             "or does not contain the required 'base_flow'")
        base_flow = flow_config.get("base_flow")
        
        if base_flow not in sequencer_config["baseflows"][model_config]:
             supported_base_flows = ', '.join(sequencer_config["baseflows"][model_config].keys())
             raise ValueError(f"The sequencer config for {sequencer_name} does not contain a "
                              f"flow definition for flow: {flow_name}. Supported (base) flows are: {supported_base_flows}")
        return flow_config
    
class ExperimentFactory:
    '''Class containing methods to instantiate an instance of Experiment.
    On particular check that the model and experiment are consistent
    '''
    @staticmethod
    def create_from_database(db: ConfigDatabase, experiment_name: str, model_name: str):
        '''Create a sequencing instance given the config db, the machine, sequencer and flow names'''

        # An issue with this check here is that if a users changes the model and does
        # imsi config, this will not be triggered, but it could be invalid (should only be done
        # via imsi set)
        experiment_data = db.get_parsed_config('experiments', experiment_name)
        if experiment_data and 'supported_models' in experiment_data:
            if model_name not in experiment_data.get('supported_models'):
                supported_models = ', '.join(experiment_data.get('supported_models'))
                raise ValueError(f"For the selected experiment: {experiment_name} the "
                                 f"selected model: {model_name} is not supported. Valid "
                                 f"models for this experiments are: {supported_models} ")
        return Experiment(name=experiment_name, parameters=experiment_data)
    
@dataclass
class Configuration:
    '''Container class that combines sub-configurations and serves as the goto reference 
       defining the configuration of a simulation.

    '''
    # maybe better to include model (header), experiment (header) and a new component object (merged)??
    merged_model_experiment: dict 
    machine: Machine
    compiler: Compiler
    postproc: PostProcessing
    setup_params: SetupParams
    utilities: Utilities
    sequencing: Sequencing
    # Add "output templates" of which "shell_params" could be one?
    # This would avoid needing the DB downstream in shell_interface, and make the 
    # Configuration complete.

    def __post_init__(self):
        '''
        Used to specifically update defaults only for specific configs
        '''
        # For sequencing, we know we want to fill default time parameters
        # with those from experiment. 
        # We are handling similar replacements in shell_parameters in the shell
        # config. However, generalizing templates might be important (see #22)
        sequencing_dict = self.get_subconfig('sequencing')
        sequencing_dict = replace_curlies_in_dict(sequencing_dict, self.to_dict())
        self.sequencing = Sequencing(**sequencing_dict)

    def to_dict(self)->Dict:
        return asdict(self)

    @classmethod
    def from_dict(cls, config_dict: Dict[str, Any]) -> 'Configuration':
        """Create a Configuration instance from a dictionary."""
        return cls(
            merged_model_experiment=config_dict['merged_model_experiment'],
            machine=Machine(**config_dict['machine']),
            compiler=Compiler(**config_dict['compiler']),
            postproc=PostProcessing(**config_dict['postproc']),
            setup_params=SetupParams(**config_dict['setup_params']),
            utilities=Utilities(**config_dict['utilities']),
            sequencing=Sequencing(**config_dict['sequencing']),
        )
    
    def get_subconfig(self, config_name: str) -> Dict:
        selfdict = self.to_dict()
        if config_name in selfdict:
            return copy.deepcopy(selfdict[config_name])
        else:
            raise ValueError(f"The requested sub-configuration: '{config_name}' is not found.")

    def get_unique_key_value(self, key: str):
        '''Search recursively through the nested dicts of the configuration to try and find a specified key
           and return its value if the key is unique. If mulitple instances of they key exist, return an error.
        '''
        # This is searching the whole imsi config for a match for key (variable)
        result = set(list(recursive_lookup(key, asdict(self))))
        if len(result) !=1:
            # No results or no unique results
            # No unique results is a major challenge. But resolving this would require specifying more information
            # than just {{variable}}. For example, something like "input_files" which appears in the configs for
            # each model would not be unique. So far, we only need to search for uniquely defined values.
            raise ValueError(f"Could not find a unique imsi definition of {key}")
        else:
            return result.pop() 

class ConfigManager:
    '''This class is used to establish configuration objects, as well as save/load them for later use'''
    # Injecting the DB is good, but it might be useful to initialize it inline.
    def __init__(self, db: ConfigDatabase=None):
        self.db = db

    def create_configuration(self, model_name:str, experiment_name: str, 
                             machine_name: str='', compiler_name: str='',
                             sequencer_name: str='', flow_name: str='',
                            **kwargs):
        
        '''Create the individual instances of config elements and return a configuration composed of these'''
        # Capture all key=value pairs passed, filtering out "self" and kwargs (else it would be nested below a kwargs key)
        setup_params = {key: value for key, value in locals().items() if key != 'self' and key != 'kwargs'}
        # Merge kwargs into setup_params, without being nested
        setup_params.update(kwargs)

        if self.db is None:
            raise RuntimeError('imsi ConfigManager has no database')

        experiment = self.create_experiment(experiment_name, model_name)
        model = self.create_model(model_name)
        machine = self.create_machine(machine_name)
        compiler = self.create_compiler(machine, compiler_name)
        setup_params = self.create_SetupParams(setup_params, machine) # improvable
        utilities = self.create_utilities()
        sequencing = self.create_sequencing(machine, setup_params.parameters['sequencer_name'], flow_name, experiment)     

        # This is important. Combining the model and experiment configurations, with the experiment
        # values taking precedence in the case of overlap.
        merged_model_experiment = update(copy.deepcopy(model.parameters),
                                         copy.deepcopy(experiment.parameters))
        
        # Create the postproc object, depending on the post_proc profile specified in model/experiment
        postproc_profile = merged_model_experiment.get('postproc_profile')
        postproc = self.create_postproc(postproc_profile)

        # Probably a bad idea, and replaced/improved with postproc_profile concept.
        # Merge postproc with experiment postproc if present
        #if 'post-processing' in merged_model_experiment:
        #    postproc.parameters = update(copy.deepcopy(postproc.parameters), 
        #                                    copy.deepcopy(merged_model_experiment['post-processing']))
    
        # This is almost certainly a bad idea too. Maybe a sequencing baseflow could be 
        # specified in experiment? Gets tricky due to dimensions with machines / sequencers etc.
        # Merge sequencing with experiment sequencing if present
        #if 'sequencing' in merged_model_experiment:
        #    sequencing.parameters = update(copy.deepcopy(sequencing.parameters), 
        #                                   copy.deepcopy(merged_model_experiment['sequencing']))
        
        return Configuration(merged_model_experiment, machine, compiler, postproc, 
                             setup_params, utilities, sequencing)

    def create_experiment(self, experiment_name: str, model_name: str) -> Experiment:
        experiment_data = self.db.get_parsed_config('experiments', experiment_name)
        return ExperimentFactory.create_from_database(self.db, experiment_name, model_name)
        
    def create_model(self, model_name: str) -> Model:
        model_data = self.db.get_parsed_config('models', model_name)
        return Model(name=model_name, parameters=model_data)

    def create_machine(self, machine_name: str = None) -> Machine:
        return MachineFactory.create_from_database(self.db, machine_name)
    
    def create_compiler(self, machine: Machine, compiler_name: str = None) -> Compiler:
        return CompilerFactory.create_from_database(self.db, machine, compiler_name)

    def create_postproc(self, postproc_profile:str) -> PostProcessing:
        postproc_data = self.db.get_parsed_config('post-processing', postproc_profile)
        return PostProcessing(parameters=postproc_data)
    
    def create_SetupParams(self, setup_params: Dict, machine: Machine) -> SetupParams:
        if 'sequencer_name' in setup_params:
            if not setup_params['sequencer_name']: # It is empty
                setup_params['sequencer_name'] = SequencingFactory.get_default_sequencer(machine)
        return SetupParams(parameters=copy.deepcopy(setup_params))

    def create_utilities(self) -> Utilities:
        utilities_data = copy.deepcopy(self.db.get_config('utility_config'))
        return Utilities(parameters=utilities_data)

    def create_sequencing(self, machine: Machine, sequencer_name: str, flow_name: str, 
                          experiment: Experiment) -> Sequencing:
        return SequencingFactory.create_from_database(self.db, machine=machine, 
                                                      sequencer_name=sequencer_name, 
                                                      flow_name=flow_name,
                                                      model_config=experiment.parameters['model_config'])

    # Simple serialization
    def save_configuration(self, configuration: Configuration, filepath: str):
        """Save the configuration to a file"""

        with open(filepath, 'w') as file:
            json.dump(configuration.to_dict(), file, indent=2)

    # Simple de-serialization
    def load_configuration(self, filepath) -> Configuration:
        """Load the configuration from a file"""
        try:
            with open(filepath, 'r') as file:
                config_data = json.load(file)
                # Create Configuration object
                return Configuration.from_dict(config_data)
        except FileNotFoundError:
            print(f'Could not open imsi config file at: {filepath}')
            return None

if __name__ == "__main__":
    # What appears below are purely for development testing.
    # This content should be moved into official tests where useful.
    setup_params = {
        'model_name' : 'canesm51_p1',
        'experiment_name' : 'cmip6-piControl',
        'machine_name' : 'ppp6',
        'runid' : 'ncs001',
        'work_dir' : '/home/ncs001/imsi/imsi/shell_interface',
        'run_config_path' : os.path.join('/home/ncs001/imsi/imsi/shell_interface', 'config'), # by convention. Could make it settable.
        'source_path' : '/home/path',
        'imsi_config_path' :'/home/ncs001/canesm/imsi-config/' ,
        "sequencer_name" : "maestro",
        "flow_name": "canesm_split_job_flow-hall6"
    }

    # Create a database instance
    db = JsonConfigDatabase(imsi_config_path=setup_params.get('imsi_config_path'))

    # Test making a config_manager with db injection
    config_manager = ConfigManager(db) # Here we are injecting the db
    configuration = config_manager.create_configuration(**setup_params)

    CONFIG_FILE_PATH = 'test-configuration.json'
    config_manager.save_configuration(configuration, CONFIG_FILE_PATH)
    
    # Check that what is loaded == what is saved
    loaded_configuration = config_manager.load_configuration(CONFIG_FILE_PATH)

    if configuration.to_dict() != loaded_configuration.to_dict():
        raise ValueError(f"Configuration mismatch between loaded dictionary at {CONFIG_FILE_PATH} "
                         "and the saved dictionary")
    print(json.dumps(configuration.get_subconfig('postproc'), indent=2))
