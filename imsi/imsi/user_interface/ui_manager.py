import os, shutil, sys, glob, copy, json
from imsi.config_manager import config_manager as cm
from imsi.shell_interface import shell_interface_manager 
from imsi.utils.general import delete_or_abort
from imsi.utils import git_tools
from imsi.utils.dict_tools import parse_vars, update, load_json
from typing import Dict
from imsi.scheduler_interface.schedulers import Scheduler, create_scheduler
from imsi.sequencer_interface.sequencers import Sequencer, create_sequencer
import subprocess
import re

def save_run_specific_configuration(configuration: cm.Configuration):
    '''
    Utility function that implements the convention the imsi_configuration_${runid}.json is
    saved in the $WRK_DIR defined during setup.
    '''
    # Possibly replaceable with simply a definition for where this file is in the configuration
    # rather than relying/constructing this configuration by convention
    wrk_dir = configuration.get_unique_key_value('work_dir') # annoyingly, inconsistent with wrk_dir. TB updated.
    runid = configuration.get_unique_key_value('runid')
    config_manager = cm.ConfigManager()
    config_manager.save_configuration(configuration,filepath=os.path.join(wrk_dir, f'imsi_configuration_{runid}.json'))

def check_runid(runid):
    # Confirm that the given runid conforms to known restrictions
    # -- Allowed length
    runid_length = len(runid)
    if runid_length > 20:
        raise ValueError(f"Your runid ---> {runid} <--- is too long! "
                         "Runids must be 20 or fewer characters to avoid conflicting with filename restrictions")

    # -- Allowed characters
    if not re.match(r'^[a-z0-9-]+$', runid):
        #if re.match(r'^[a-z0-9.-]+$', runid) and config == "REFENV":
        #    # we allow "." characters for reference environment names
        #    pass
        #else:
        raise ValueError(f"Your runid ---> {runid} <--- contains unsupported characters! "
                          'You must use a runid containing only lowercase alphanumeric characters "a-z0-9", or hyphens "-"'
                          "Also, remember to make it short (<20chars) and unique to you"
                          "Examples: runid=ncs-tst-ctrl-01")

def setup_run(runid, repo, ver, exp, model, fetch_method, seq, machine=None, compiler=None, flow=None):
    ''' 
    Create a run directory, clone in the source code, and checkout version ver  
    
    Inputs:
    -------
    runid : str
        The name of the simulation, also used to create the directory.
    repo : str
        The path or url to the git repository to be cloned.
    ver : str
        The git treeish to checkout, typically a commit, tag or branch name
    exp : str
        Name of a valid imsi experiment to setup (required)
    model : str
        Name of a valid imsi model to use (optional)
    fetch_method: str
        How to get the source. Either "clone", "copy" or "link".
    seq : str
        Sequencer to use. Currently either "iss" or "maestro"
    flow : str
        Sequencing flow to use.

    Outputs:
    --------
    Returns nothing, but creates the directory named for runid and recursively
    clones the code from repo and afterwards checks out ver.
    '''
    # Moving forward, there will need to be more configurable set of paths
    # or options for where to put things. These might be in the user config
    # or machine config, or elsewhere.
    # In terms of dealing with cloning or not, having different paths might help to
    # separate the source from the working directory.   
    check_runid(runid)

    if os.path.exists(runid):
        print(f'\n\n**WARNING**: The directory {runid} already exists.')
        delete_or_abort(runid)
    os.mkdir(runid)
    os.chdir(runid) 

    cwd = os.getcwd()

    # Clone, soft link or copy the source code
    super_repo_source = "src"
    if fetch_method == 'clone':
        print(f'Cloning {ver} from {repo} for {runid}')
        git_tools.recursive_clone(repo, super_repo_source, ver)
    elif fetch_method == 'link':
        print(f'Soft linking source from {repo} for {runid}')
        os.symlink(repo, super_repo_source)
    elif fetch_method == 'copy':
        print(f'Copying source from {repo} for {runid}')
        shutil.copytree(repo, super_repo_source, symlinks=True, ignore_dangling_symlinks=True)  

    src_dir = os.path.join(cwd, super_repo_source)
    imsi_config_path = os.path.join(src_dir, 'imsi-config') # Requirement that imsi-config appears at the highest repo level of a supported model repo.
    if not os.path.exists(imsi_config_path):
        raise ValueError("\n\n **ERROR**: 'imsi-config' directory not found at the top repo level, but is required. Is this a valid imsi configured code base?")

    os.chdir(cwd)

    setup_params = {
       "model_name" : model,       # user input
       "experiment_name" : exp,    # user input
       "machine_name" : machine,   # user input - optional
       "compiler_name" : compiler, # user input - optional
       "runid" : runid,
       "work_dir" : cwd,         # implicitly set by user
       "source_path" : src_dir,  # Make settable in upstream imsi (machine) config? and/or at cmd line?
       "source_repo" :  repo,  # about tracking, but not so used
       "source_version" : ver, # "
       "run_config_path" : os.path.join(cwd, 'config'), # convention. Make settable?
       "imsi_config_path" : imsi_config_path,            # convention. Make settable?
       "fetch_method" : fetch_method,
       "sequencer_name": seq,
       "flow_name": flow
    }
    # Everything above this could be extracted to an "external" setup command, while everything else
    # could in theory come from within the repo, ala current setup/adv-setup. Not sure how to manage
    # that in the context of python packaging though!
    configuration, db = create_imsi_configuration(imsi_config_path, setup_params)
    build_run_config_on_disk(configuration, db)
    print(f"\nIMSI setup complete. You can now: \n\n\t\t cd {runid} \n")

def create_imsi_configuration(imsi_config_path: str, setup_params: Dict) -> (cm.Configuration, cm.ConfigDatabase):
    '''Build and return configuration instance and config db given imsi_config_path
    '''
    if not os.path.isdir(imsi_config_path):
        raise FileNotFoundError(f'Could not find imsi config directory at: {imsi_config_path}')
    
    # create a DB / cm 
    db = cm.JsonConfigDatabase(imsi_config_path)
    config_manager = cm.ConfigManager(db)

    configuration = config_manager.create_configuration(**setup_params)
    # Save the configuration for future editing/reference
    save_run_specific_configuration(configuration)

    # Create a configuration based on user input from setup cli
    return (configuration, db)
    
def build_run_config_on_disk(configuration: cm.Configuration, db: cm.ConfigDatabase, force=False):     
    '''This actually creates the physical config directory on disk, and extracts/modifies various relevant files'''
    # Build the actual config directory with contents for this configuration
    shell_interface_manager.build_config_dir(db, configuration, force=force)

    # Do scheduler/sequencer setup
    setup_params = configuration.get_subconfig('setup_params')['parameters']
    sequencer = create_sequencer(setup_params.get('sequencer_name'))
    sequencer.setup(configuration)
    sequencer.config(configuration)

def load_run_config() -> cm.Configuration:
    '''Try to load a imsi_configuation_{runid}.json either from $WRK_DIR if it exists, or if not, try the local path
    
       This function is re-instantiating the configuration object, which does not exist between independent calls to
       the gui.
    '''
    # Implementing a configuration by convention that the local run config file is called
    # `imsi_configuration_${runid}.json`, but this should be set in a single defined place
    # and used consistently throughout (noting here, but use is widespread in this module).
    
    # First look in the PWD
    wrk_dir = "./" #os.getenv('WRK_DIR', './') -? could be dangerous
    config_file_pattern=os.path.join(wrk_dir, 'imsi_configuration_*.json')
    config_file_matches = glob.glob(config_file_pattern)

    # In no config file in PWD, see if WRK_DIR is defined in the end
    if not config_file_matches:
        # could be dangerous if wrong WRK_DIR is defined. But useful to call imsi from outside WRK_DIR.
        wrk_dir = os.getenv('WRK_DIR') 
        if wrk_dir:
            config_file_pattern = os.path.join(wrk_dir, 'imsi_configuration_*.json')
            config_file_matches = glob.glob(config_file_pattern)   
            if config_file_matches:
                print(f'\n NB: Using configuration defined by WRK_DIR at {config_file_matches[0]} \n') 

    if not config_file_matches:    
        raise FileNotFoundError("Cannot find the imsi configuration file." 
                                "You must be in a valid imsi setup directory, or WRK_DIR must be defined")

    config_file = config_file_matches[0] # glob return a list
    config_manager = cm.ConfigManager()
    configuration = config_manager.load_configuration(config_file)        
    return configuration

def reload_config_from_source(force=False):
    """
    Build a new config directory from upstream imsi source

    This will re-extract everthing out of the cloned repository to re-create 
    the config directory. I.e. if one made changes in the repo after setup, and
    wanted to apply them, they would call this update function.
    """
    original_configuration = load_run_config()

    # This will rebuild the config directory completely
    imsi_config_path = original_configuration.get_unique_key_value('imsi_config_path')
    new_configuration, db = create_imsi_configuration(imsi_config_path, original_configuration.get_subconfig('setup_params')['parameters'])
    build_run_config_on_disk(new_configuration, db, force=force)

def update_config_from_json(force=False):
    ''' Apply changes made in the "imsi_configuration_${runid}.json" file 
        to the configuration and update the config directory as appropriate.
    '''
    configuration = load_run_config()   
    # This will rebuild the config directory completely based on what is in the configuration file.
    # It would be good if there were nominal validity testing.
    db = cm.JsonConfigDatabase(configuration.get_unique_key_value('imsi_config_path'))
    build_run_config_on_disk(configuration, db, force=force)

def set_selections(parm_file=None, selections=None, options=None, force=False):
    """Parse key=value pairs of selection given on the command line
       Try to apply these to the imsi selections for the sim.
    """
    # This function should be split to respect SRP 
    print(f'set selections: {selections}')
    # get existing simulation config
    configuration = load_run_config()   
    # This will rebuild the config directory completely based on what is in the configuration file.
    # It would be good if there were nominal validity testing.
    db = cm.JsonConfigDatabase(configuration.get_unique_key_value('imsi_config_path'))
    config_manager = cm.ConfigManager(db)
   
    # Would be best to get this from a query to a class method probably, 
    # instead of having high coupling by infering the internal structure.
    updated_setup_params = copy.deepcopy(configuration.setup_params.parameters)
    print(updated_setup_params)
    if parm_file:
        file_values = load_json(parm_file) # would actually be more valuable for options I think, since
                                           # selections are few bu options possibly many.
        # This is updating the ._config dict in place
        updated_setup_params = update(updated_setup_params, file_values)
    if selections:
        values = parse_vars(selections)
        print(values)
        # This is updating the ._config dict in place
        updated_setup_params = update(updated_setup_params, values)  
        print(updated_setup_params)

    #Create a new simulation that we imbue with these properties
    new_configuration = config_manager.create_configuration(**updated_setup_params)

    if options:
        options_config = db.get_config('model_options')
        selected_option_names = parse_vars(options)
        new_configuration = apply_options(options_config, configuration, selected_option_names)    

    # With the changed configuration, build a new config dir
    shell_interface_manager.build_config_dir(db, new_configuration, force=force)

    # Update the saved configuration file accordingly
    save_run_specific_configuration(new_configuration)

def apply_options(options_config: Dict, configuration: cm.Configuration, selected_option_names: Dict) -> cm.Configuration:
    """
    Take a set of options or 'patches' and apply them to the simulation configuration and
    return an updated configuration.

    Input:
    ------
    selected_options : dict
       k-v pairs of option name and selection
    """
    # Check first the option is valid
    for option, selection in selected_option_names.items():
        if option in options_config.keys():
            if selection in options_config[option].keys():
                for target_config, target_values in options_config[option][selection].items():
                    # Add to list of applied options
                    # self.options[option] = selection
                    # Set the options in the simulations internal state
                    new_config_dict = configuration.to_dict()
                    update(new_config_dict[target_config], target_values)
                print(f"Updated {option} with {selection}")
            else:
                raise ValueError(f"\n**ERROR**: there is no valid selection {selection} under the option named {option}. " +
                                 f"Available selections are {list(options_config[option]['options'].keys())}")
        else:
            raise ValueError(f"\n**ERROR**: there is no option named {option}. Available options are {list(options_config.keys())}")
        return cm.Configuration.from_dict(new_config_dict)

def list_choices(repo, experiments, models, platforms, compilers, options):
    """List of supported elements in an imsi database, for example names of supported
       experiments, models, machines, compilers.
    """

    db = None

    # Users can provide a path to a imsi config anywhere on disk to parse
    # Or, we can try and load a local imsi state if it exists.
    # Should expand to using the ~/.imsi/imsi-user-config.json
    if not repo:
        configuration = load_run_config()   
        imsi_config_path = configuration.get_unique_key_value('imsi_config_path')
    elif os.path.exists(repo):
        imsi_config_path = os.path.join(repo, 'imsi-config')
    else:
        raise ValueError("Imsi list: You must specify a valid path to an imsi config directory (repo) or be within an imsi configured directory.")
    
    db = cm.JsonConfigDatabase(imsi_config_path)
    

    print(f"Options relating to imsi-config at {imsi_config_path}")
    print('\n')
    if experiments:
        print("Supported experiments:", ' '.join(db.get_config('experiments').keys()), "\n")
    if models:
        print("Supported models:", ' '.join(db.get_config('models').keys()), "\n")
    if platforms:
        print("Supported machines:",' '.join(db.get_config('machines').keys()), "\n")
    if compilers:
        print("Supported compilers:", ' '.join(db.get_config('compilers').keys()), "\n")
    if options:
        if options=="no_opt":
            print("Available options:")
            if db.get_config('model_options'):
                for k in db.get_config('model_options').keys():
                    print(k,":")
                    for sk in db.get_config('model_options')[k]:
                        print('\t', sk)
        else:
            if db.get_config('model_options'):
                #for option in options:
                #print(option)
                if options in db.get_config('model_options').keys():
                    print(json.dumps(db.get_config('model_options')[options], sort_keys=False, indent=2))

# I think this is not a bad idea. But the way it SHOULD be implemented is as
# an abtract interface, with specific implementations (so support different models).
# Also, this is a utility, which should not be mixed directly with the core configuration
# classes.
def compile_model_execs():
    """
    Builds all component executables by calling an upstream script from the
    repository. Pretty rough go. Is it useful abstracting this in imsi TDB.
    """
    configuration = load_run_config()
    work_dir = configuration.get_unique_key_value('work_dir')
    if not os.path.isdir(work_dir):
        raise FileNotFoundError(f'Could not find the run working directory at: {work_dir}')
    os.chdir(work_dir)

    if not os.path.exists("./imsi-tmp-compile.sh"):
        raise FileNotFoundError(f'Could not find compilation file: ./imsi-tmp-compile.sh')
    compile_task = subprocess.Popen(["./imsi-tmp-compile.sh"])
    compile_task.wait()
    #streamdata = compile_task.communicate()[0]
    rc = compile_task.returncode    
    if rc != 0:
        raise ValueError("Error: Compiling failed with imsi-tmp-compile")

def submit_run():
    """Instantiate the configuration object and submit job to queue"""
    configuration = load_run_config()
    setup_params = configuration.get_subconfig('setup_params')['parameters']
    sequencer = create_sequencer(setup_params.get('sequencer_name'))
    sequencer.submit(configuration)

# WIP
def query_time():
    """Instantiate the configuration / SimulationTime instances and enable querying timers"""
    from imsi.time_manager.time_manager import SimulationTime
    configuration = load_run_config()
    sequencing_config = configuration.get_subconfig('sequencing')['parameters']
    sim_timers = SimulationTime.from_kwargs(**sequencing_config['run_dates'])
