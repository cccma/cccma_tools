'''
This module generates timer information for simulations, based on a start date, end date, and a chunking 
duration (amount of time to run the model). Specifically, the module will generate lists of all job start
and stop dates, and from this, be able to assess the number of chunks to simulate. It also provides various
functions for quering and formating time into strings / dicts.

This module uses cftime and extensions of cftime from xarray, notably cftimeindex / ranges. The advantages
of this implementation over previous shell based time logics are that:
- handles precision down to nanoseconds (for "free")
- does not assume monthly chunking (can support any chunk size)
- defaults to, but does not assume a noleap calendar (other calendars easily supported)

Existential question: do we need a time manager module / SimulationTime class? If we really want imsi
to have a deep concept of timers, maybe we do. An alternative is that the only thing we really need
to keep is the functions. Then within the shell_interface, populate the files etc as needed, getting
information from the configuration and through calling these functions to eg. create lists. 

NCS, July 2024
'''
import cftime
import xarray
import json
from datetime import timedelta
from imsi.time_manager.cftime_utils import get_date_type, adjust_start_date, parse_time, parse_frequency

# TODO: 
#   - Move the set_shell_config, including timer stuff, into config_manager
#   - Have a shell_timers.py in shell_interface, and use that to write out time lists,
#     and any other information required by downstream, e.g. segment times and chunking (need CV)
#   - Add a CLI entry point to time manager, that would instantiate a SimulationTime and allow it
#     to be queried (lower priority)

#  Should be refactored. 
# Could be a dataclass, with a Factory to instantiate.
# init method to Factory and try to respect SRP which it does not now
class SimulationTime:
    """
    A class to store simulation time variables and provide a few utility functions

    Attributes:
        calendar: cftime calendar type to use for parsing dates
        run_start_time (cftime.datetime): Start time of the entire simulation.
        run_stop_time (cftime.datetime): Stop time of the entire simulation.
        run_segment_start_time (cftime.datetime): Start time for the current portion of the simulation.
        run_segment_stop_time (cftime.datetime): Stop time for the current portion of the simulation.
        model_chunk_size (str): Length of each model submission (pandas/xr frequency).
        model_internal_chunk_size (str): Length of internal chunking within each model submission (pandas/xr frequency).
        postproc_chunk_size (str): Length of the postprocessing jobs (pandas/xr frequency).
    """

    def __init__(self, calendar: str, run_start_time: cftime.datetime, run_stop_time: cftime.datetime,
                 run_segment_start_time: cftime.datetime, run_segment_stop_time: cftime.datetime,
                 model_chunk_size: str, model_internal_chunk_size: str, 
                 postproc_chunk_size: str):
        self.calendar = calendar
        self.cftime_calendar = get_date_type(self.calendar)

        self.run_start_time = run_start_time
        self.run_stop_time = run_stop_time
        self.run_segment_start_time = run_segment_start_time
        self.run_segment_stop_time = run_segment_stop_time
        self.restart_date = self.run_segment_start_time - timedelta(seconds=1)
        
        self.model_chunk_size = model_chunk_size
        self.model_internal_chunk_size = model_internal_chunk_size
        self.postproc_chunk_size = postproc_chunk_size

        (self.model_submission_job_start_list, 
         self.model_submission_job_stop_list) = self._generate_job_start_end_date_lists(self.run_segment_start_time, 
                                                                            self.run_segment_stop_time, 
                                                                            self.model_chunk_size)
        (self.model_execution_loop_start_list,
         self.model_execution_loop_stop_list) = self._generate_job_start_end_date_lists(self.run_segment_start_time, 
                                                                                 self.run_segment_stop_time, 
                                                                                 self.model_internal_chunk_size)
        
        (self.postproc_submission_job_start_list,
         self.postproc_submission_job_stop_list) = self._generate_job_start_end_date_lists(self.run_segment_start_time, 
                                                                                self.run_segment_stop_time, 
                                                                                self.postproc_chunk_size)

        self.number_of_model_chunks = len(self.model_submission_job_start_list)
        self.number_of_model_inner_chunks = len(self.model_execution_loop_start_list)
        self.number_of_postproc_chunks = len(self.postproc_submission_job_start_list)
        
    @classmethod
    def from_kwargs(cls, **kwargs):
        '''
        Instantiate a SimulationTime instance from KW args

        Parse times into CFTIMES, using start/end logic
        '''
        calendar = kwargs.get('calendar', 'noleap')
        cftime_calendar = get_date_type(calendar)

        run_start_time = parse_time(kwargs.get('run_start_time'), cftime_calendar)
        run_stop_time = parse_time(kwargs.get('run_stop_time'), cftime_calendar, date_type='end')
        run_segment_start_time = parse_time(kwargs.get('run_segment_start_time'), cftime_calendar)
        run_segment_stop_time = parse_time(kwargs.get('run_segment_stop_time'), cftime_calendar, date_type='end')
        
        model_chunk_size = kwargs.get('model_chunk_size')
        model_internal_chunk_size = kwargs.get('model_internal_chunk_size')
        postproc_chunk_size = kwargs.get('postproc_chunk_size')
        
        return cls(calendar, run_start_time, run_stop_time, run_segment_start_time, run_segment_stop_time,
                   model_chunk_size, model_internal_chunk_size, postproc_chunk_size)

   
    # this could be useful in downstream infrastructure, but not explicitly used yet
    def get_time_var(self, time_var: str, component: str, format_spec: str) ->str:
        """
        Retrieve a formatted time variable component.

        Args:
            time_var (str): The base time variable name (e.g., 'run_start_time').
            component (str): The component of the time variable (e.g., 'month').
            format_spec (str): The format specification for the output (e.g., '02d').
        Return: 
            The formatted component string.
        """
        time_obj = getattr(self, time_var)
        value = getattr(time_obj, component)
        return f"{value:{format_spec}}"

    def shell_formatted_time_vars(self):
        """
        Create a dict of formatted time/chunk vars to meet the current canesm scripting interface
        """
        dates2process={'run_start': self.run_start_time, 
                       'run_stop': self.run_stop_time, 
                       'run_segment_start': self.run_segment_start_time, 
                       'run_segment_stop': self.run_segment_stop_time
                       }
        
        formatted_time_vars=format_time_vars(dates2process)
        
        for chunk in ['number_of_model_chunks', 'number_of_model_inner_chunks', 
                      'number_of_postproc_chunks']:
            formatted_time_vars[chunk] = getattr(self, chunk)

        # This is a hack to just put in the duration mulitplier, assuming it is in months.    
        for chunk in ['model_chunk_size', 'model_internal_chunk_size', 'postproc_chunk_size']:
            mulitplier, _ = parse_frequency(getattr(self, chunk))
            formatted_time_vars[chunk.upper()] = mulitplier
        return formatted_time_vars

    def _generate_job_start_end_date_lists(self, start: cftime.datetime, end: cftime.datetime, duration: str):
        '''
            Get an array of job start and end dates, given "segement" start and end dates, and a duration
            of chunking.

            Uses xarray date range generation, and frequency conventions

        Args:
            start (cftime.datetime): The starting time.
            end (cftime.datetime): The ending time.
            duration (str): The duration specifying the step size.

        Returns:
            (job_start_array, job_end_array)  (tuple): tuple of lists containing cftimes of job start/ends
        '''
        # use xarray to generate a cftime_range. This requires quite a few functions/classes to achieve,
        # so they we not replicated here (but could be considered for stability and to remove xarray dependency,
        # which is probably an overkill dependency since only some cftime functionality is being used)
        #offset = cftime_offsets.to_offset(frequency)
        #return list(cftime_offsets._generate_range(start, end, None, offset))
        #nchunks = len(all_starts_times)
        #return nchunks

        # We must adjust the start, to handle cases, for example:
        # start='2000-01-02', end='2000-12-31'
        # which will lead to the range starting at '2000-02-01' if frequency is 'MS'  
        adjusted_start = adjust_start_date(start, duration) # this function should be replaced as it is limited
        job_starts = list(xarray.cftime_range(adjusted_start, end, periods=None, freq=duration, calendar=self.calendar))
        job_starts[0] = start # We want the first element to be the actual specified start; All other fields will fall on a period boundary
        job_ends = list(xarray.cftime_range(adjusted_start, periods=len(job_starts) + 1, 
                                            freq=duration, calendar=self.calendar) 
                            - timedelta(seconds=1) # shift back 1 second from start times to get end times
                        )[1::]                     # Do not use the first element (not an end time)
        job_ends[-1] = end # We want the last element to be the actual specified end; 
        return (job_starts, job_ends) 

def get_job_date_components_for_iteration(sim_time: SimulationTime, iteration):
    '''
    Get the job date components (date, year, month, day...) given a specified integer iteration (or chunk number)
    '''
    # Example of generating "job" level variables like those used in the maestro containers
    job_date_strings = format_time_vars({'job_start':sim_time.model_outer_job_start_list[iteration],
                                         'job_stop':sim_time.model_outer_job_end_list[iteration]
                                         })
    print(json.dumps(job_date_strings, indent=1))

# Likely be useful for downstream
def format_time_vars(time_dict: dict) -> dict:
    """
    Break a dicts of specified cftime's into component elements (date, year, month, day, hour, minute, second)
    and return a dict of these.

    The keys in the input dict are used to label the elements returned in the output dict

    Example:
        format_time_vars({'run_start' : cftime.DatetimeNoLeap(2016,1,4)} )
             {'run_start_year' : '2016'
              'run_start_month' : '1'...
             }
    """
    formatted_time_vars={}
    for prefix, this_cftime in time_dict.items():
        formatted_time_vars[prefix + '_date'] = this_cftime.strftime("%Y-%m-%d")
        for freq in ['year', 'month', 'day', 'hour', 'minute', 'second']:
            formatted_time_vars[prefix + '_' + freq] = getattr(this_cftime, freq)
    return formatted_time_vars

def example_usage():
    # Basic test
    args = {
        'run_start_time': '6000-01-05',
        'run_stop_time': '6009-12',
        'run_segment_start_time': '6000-01-01',
        'run_segment_stop_time': '6009-12',
        'model_chunk_size': '12MS',
        'model_internal_chunk_size': '12MS',
        'postproc_chunk_size': '12MS',
    }

    # Generate the basic SimulationTime instance
    simulation_vars = SimulationTime.from_kwargs(**args)

    # Example of date to be put into shell_parameters file
    tvars  = simulation_vars.shell_formatted_time_vars()
    import json
    print(json.dumps(tvars, indent=2))

    print(simulation_vars.restart_date)
    return
    # Example of what is the in job start/stop lists
    for (s,e) in zip(simulation_vars.model_submission_job_start_list, simulation_vars.model_submission_job_stop_list):
        print(s, '   --   ', e)

    # Example of generating "job" level variables like those used in the maestro containers
    for i in [0,1,2]: # range(simulation_vars.number_of_model_chunks):
        get_job_date_components_for_iteration(simulation_vars, i)

    # Example requerying a specific variable and resolution with a given format
    # which could be used downstream
    print(simulation_vars.get_time_var('run_segment_start_time', 'year', '04d'))

if __name__ == "__main__":
    example_usage()
