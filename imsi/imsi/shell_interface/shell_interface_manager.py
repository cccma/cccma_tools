'''
Generates the shell interface to the modelling system.

In general this means creating a "config" directory with various files that the
model and downstream utilities ingest, such as namelists, and shell_parameters files.
'''
from imsi.config_manager import config_manager as cm
from imsi import shell_interface as si
from imsi.utils.general import write_shell_script
import os, stat
from imsi.utils.dict_tools import recursive_lookup

# Main manager function
def build_config_dir(db: cm.ConfigDatabase, configuration: cm.Configuration, force=False):
    '''Composition function to manange the build the config directory with appropriately parsed content'''
    # The arugements should be reevaluated.

    # Unpacking for ease of reuse below.
    # Obviously having to know what these are represent some coupling. Whether it is reducable is always a good question.
    # Try to use getter/setter implement upstream in the classes
    config_dict = configuration.to_dict()
    component_dict = configuration.get_subconfig('merged_model_experiment')['components']
    sequencing_config = configuration.get_subconfig('sequencing')
    run_dates = sequencing_config['parameters']['run_dates']
    imsi_config_path = configuration.get_unique_key_value('imsi_config_path')
    run_config_path = configuration.get_unique_key_value('run_config_path')
    work_dir = configuration.get_unique_key_value('work_dir')
    
    # 1. Create config directory structure
    si.shell_interface_utilities.create_config_dirs(run_config_path, 
                                                    component_dict.keys(),
                                                    force=force) # setup the config_dir structure

    # 2. shell_parameters files
        # Almost certainly should be done in config_manager, not here.
        # The fact that the db is used here and not elsewhere indicates this.
        # Like the db should only be used in the config not the interfaces, while the 
        # configuration itself is used in the interfaces. i.e. the Configuration is the SSOT
    shell_config = si.shell_config_parameters.set_shell_config(db.get_config('shell_config'), config_dict)
    
        # this writes the shell_parameters files
    write_shell_script(os.path.join(run_config_path,'shell_parameters'), 
                       si.shell_config_parameters.generate_shell_parameters(shell_config))        

        # example of how this could be the end point for imsi and shell utlities could take over
        # That would be a failure, but is nonetheless possible. In this case, imsi could only be used
        # to ingest data from jsons and do the basic inheritance, but not really much else.
        # If nothing else, what this provides is super consistent variable naming in the system
        # but also of course, the json organization. So even this would be an improvement on
        # what we have, but would be a bit underwhelming given the potential for embracing
        # python based utilities that can leverage the power of the data structures more effectively.
    write_shell_script(os.path.join(run_config_path,'comprehensive_shell_parameter_example'), 
                       si.shell_config_parameters.generate_flattened_config(config_dict))        

    #3. Write computational env / compilation files 
        # (to be refactored using common writing functions and to more explicitly specify paths etc)
    write_shell_script(os.path.join(run_config_path,'computational_environment'), 
                       si.shell_comp_environment.generate_computational_environment(configuration))        

    write_shell_script(os.path.join(run_config_path,'compilation_template'), 
                       si.shell_comp_environment.generate_compilation_template(configuration))    

    # 4. This does the processing of namelists and cpp files, taking in reference
    #     versions and modifying them according to changes listed in component_config

        # args passed to be re-evaluated. For example, could extract paths out of config_dict lower down (but more coupling)
    
        # process namelists/cpp files per component
    for component, component_config in component_dict.items():
        si.shell_inputs_outputs.process_model_config_files(component, component_config, 
                                                           imsi_config_path, run_config_path, 
                                                           'cpp_defs', shell_config=shell_config)
        si.shell_inputs_outputs.process_model_config_files(component, component_config, 
                                                           imsi_config_path, run_config_path, 
                                                           'namelists', shell_config=shell_config)
        # extract identified utlitity scripts (like compile)
        files_to_extract = configuration.get_subconfig('utilities')['parameters'].get('files_to_extract',{})
        if not files_to_extract: # backwards compatibility
            files_to_extract={"save_restart_files.sh" : "models/save_restart_files.sh",
                              "imsi-tmp-compile.sh" :  "imsi-tmp-compile.sh"
            }
    si.shell_inputs_outputs.extract_utility_files(files_to_extract, imsi_config_path, work_dir)

    # 5. Write shell scripts for use in pre/post ludes
        # Could split this by component and join with above for loop
    # Input files (prelude)
    write_shell_script(os.path.join(run_config_path,'imsi_get_input_files.sh'), 
                       si.shell_inputs_outputs.generate_input_script_content(component_dict, shell_config))    
    # directory packing in postlude
    write_shell_script(os.path.join(run_config_path,'imsi_directory_packing.sh'),
                       si.shell_inputs_outputs.generate_directory_packing_content(component_dict))                   
    # final saving in postlude
    write_shell_script(os.path.join(run_config_path, 'imsi_save_output_files.sh'),
                       si.shell_inputs_outputs.generate_final_file_saving_content(component_dict))
    
    # 6. Write diag_parameters
    diag_config =  config_dict["postproc"]
    write_shell_script(os.path.join(run_config_path, 'diag_parameters'),
                       si.shell_diag_parameters.generate_diag_parameters_content(diag_config))

    #7. Generate a file with the model run command
    output_path = os.path.join(run_config_path,'imsi-model-run.sh')
    # V Don't love V
    run_commands = list(recursive_lookup('model_run_commands', sequencing_config))[0]
    write_shell_script(output_path, run_commands)
    st = os.stat(output_path)
    os.chmod(output_path, st.st_mode | stat.S_IEXEC)

    # 8. Write timing files
    write_shell_script(os.path.join(run_config_path, 'model_submission_job_start-stop_dates'),
                       si.shell_timing_vars.generate_timers_config(run_dates, 
                                                                   'model_submission_job'))

    
    write_shell_script(os.path.join(run_config_path, 'model_execution_loop_start-stop_dates'),
                       si.shell_timing_vars.generate_timers_config(run_dates, 
                                                                   'model_execution_loop'))
    
    write_shell_script(os.path.join(run_config_path, 'postproc_submission_job_start-stop_dates'),
                       si.shell_timing_vars.generate_timers_config(run_dates, 
                                                                   'postproc_submission_job'))

if __name__ == '__main__':
    # For testing purposes
    # In the wild, would be used via the imsi CLI and associated setup flow

    # create a cm 
    db = cm.JsonConfigDatabase(imsi_config_path='/home/ncs001/canesm/imsi-config/')
    config_manager = cm.ConfigManager(db)

    # Mock in user/run info that would have been received during setup
    setup_dict = {
        'model_name' : 'canesm51_p1',
        'experiment_name' : 'cmip6-piControl',
        'machine_name' : 'ppp6',
        'runid' : 'ncs001',
        'work_dir' : '/home/ncs001/imsi/imsi/shell_interface',
        'run_config_path' : os.path.join('/home/ncs001/imsi/imsi/shell_interface', 'config'), # by convention. Could make it settable.
        'source_path' : '/home/path',
        'imsi_config_path' :'/home/ncs001/canesm/imsi-config/' ,
        "sequencer_name" : "maestro",
       # "flow_name": "cannemo_split_job_flow"
    }

    configuration = config_manager.create_configuration(**setup_dict)
    build_config_dir(db, configuration)