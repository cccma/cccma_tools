import os
from typing import List, Dict
import shutil
from imsi.utils.dict_tools import replace_variables
from imsi.utils import nml_tools
import imsi

def process_model_config_files(component: str, component_config: dict, 
                               imsi_config_path: str, run_config_path: str, 
                               file_type: str, shell_config: dict):
    """Process namelist and cpp files for all component defined in component_config

      Does replacements of {{}} variables using shell_config inputs
      Updates reference files from "ref_file_path" with "file_content"
    """

    if not component_config:
        return

    os.chdir(os.path.join(run_config_path, component))
    
    # For backwards compatibility
    if file_type == 'cpp_defs':
        dirname='compilation'
    else:
        dirname=file_type

    os.makedirs(dirname, exist_ok=True)
    # There is a bit of configuration by convention here.
    if component_config and "config_dir" in component_config:
        component_config_path = os.path.join(imsi_config_path, 'models', component_config['config_dir'])

    if file_type in component_config:
        for file_name, file_content in component_config[file_type].items():
            ref_file_path = os.path.join(component_config_path, file_name)
            target_file_path = os.path.join(dirname, os.path.basename(file_name))

            if file_content:
                print(f"Updating {file_type}: {file_name}")

                if file_type == 'cpp_defs':
                    process_cpp_file(ref_file_path, target_file_path, file_content)
                elif file_type == 'namelists':
                    process_namelist_file(ref_file_path, target_file_path, file_content, shell_config)
                else:
                    shutil.copyfile(ref_file_path, target_file_path)
            else:
                shutil.copyfile(ref_file_path, target_file_path)

def process_cpp_file(ref_file_path, target_file_path, file_content):
    """Process cpp files
    Updates default cpp files from ref_file_path with "file_content"
    """
    nml_tools.cpp_update(ref_file_path, target_file_path, file_content)


def process_namelist_file(ref_file_path, target_file_path, file_content, shell_config):
    """Process namelist files
       Does replacements of {{}} variables using shell_config inputs
       Updates default namelists from ref_file_path with "file_content"
    """
    file_content = replace_variables(file_content, shell_config)
    nml_tools.nml_update(ref_file_path, target_file_path, file_content)

def generate_input_script_content(component_dict: dict, shell_config: dict) -> List[str]:
    """Generate content for the input files script (to be called at runtime with access' etc)"""
    script_content = ["# Imsi created input file", "# Obtain executables and namelists"]

    for component, component_config in component_dict.items():
        if component_config:
            script_content.append(f'# Obtaining {component} inputs')

            # Obtain namelists
            component_namelist_dir = os.path.join(shell_config['WRK_DIR'], 'config', component, 'namelists')
            if "namelists" in component_config:
                for namelist in component_config['namelists'].keys():
                    component_namelist_path = os.path.join(component_namelist_dir, namelist)
                    script_content.append(f"cp {component_namelist_path} .")

            # Obtain executables
            if "exec" in component_config:
                component_exe_path = os.path.join(shell_config['EXEC_STORAGE_DIR'], component_config["exec"])
                script_content.append(f"cp {component_exe_path} .")

            # Obtain files listed in component 'input_files' config
            if 'input_files' in component_config:
                for k, v in component_config['input_files'].items():
                    if not v:
                        raise ValueError(f"\n ** No input file is specified for the required input {k} by component {component}. \n\n")    
                    script_content.append(f"access {k} {v}")

    return script_content

def generate_directory_packing_content(component_dict: dict) -> List[str]:
    """Generate directory packing commands based on content in an imsi "components" config, to be called post model run"""
    packing_content = ["\n# Imsi created directory packing list"]

    for component, component_config in component_dict.items():
        if component_config and 'directory_packing' in component_config:
            packing_content.append(f'# Directory packing for: {component}')
            for target_dir, source_dir in component_config['directory_packing'].items():
                packing_content.append(f'csf_mv_files_to_dir "{source_dir}" "{target_dir}"')

    return packing_content
    
def generate_final_file_saving_content(component_dict: dict) -> List[str]:
    """Generate all required saved commands for final outputs based on an imsi "components" config (dict) to be called
    post model run and post directory packing"""
    saving_content = ["\n# Imsi created output files"]

    for component, component_config in component_dict.items():
        if component_config and 'output_files' in component_config:
            saving_content.append(f'# Saving files/dirs from: {component}')
            for file_name, target_dir in component_config['output_files'].items():
                saving_content.append(f'save {file_name} {target_dir}')

    return saving_content

def extract_utility_files(files_to_extract: Dict, imsi_config_path: str, work_dir: str):
    '''Extract utility files from the source repository into workdir for use.
       The file source path is defined relative to imsi_config_path
       The destination path is defined relative to the run wrk_dir 
    '''
    for target, source in files_to_extract.items():
        source_fullpath = os.path.join(imsi_config_path, source)
        target_fullpath = os.path.join(work_dir, target)
        shutil.copy(source_fullpath, target_fullpath)
