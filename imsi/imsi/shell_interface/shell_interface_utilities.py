'''
General utility functions for creating the "shell interface" (config directory) and associated files.
'''
from typing import List
import os
import shutil
from imsi.utils.general import delete_or_abort

def create_config_dirs(run_config_path: str, components: List[str], force=False):
    """Create the run config dir and subdirectories for each component."""
    # probably should add a "-f" kind of option to force the overwrite,
    # but otherwise ask if they are sure
    if os.path.exists(run_config_path):
        if force:
            print(f"**WARNING**: config dir exists, overwriting at {run_config_path}")
            shutil.rmtree(run_config_path)
        else:
            delete_or_abort(run_config_path)
        
    os.makedirs(run_config_path)
    for component in components:
        os.makedirs(os.path.join(run_config_path, component))

if __name__ == "__main__":
    # Only for original dev testing. See shell_interface_manager for more up to date tests.
    pass