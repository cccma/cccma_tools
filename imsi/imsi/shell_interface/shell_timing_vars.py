from imsi.time_manager.time_manager import SimulationTime, format_time_vars
from datetime import timedelta

def generate_timers_config(run_dates: dict, timer_name: str) -> dict:
    """Generate lists of timer variables for various jobs, to be written out and used
    downstream. Specifically returns start and stop lists for the model outer loop (job submission),
    model internal loop (within jobs), and the postproc loop. 


    Parameters:
        run_dates (dict ) : A dict of run dates, in iso8061 format, with keys
                            [run_, run_segment]_[start,stop]_date, and keys
                            for model_chunk_size, model_internal_chunk_size
                            and postproc_chunk_size.
        timer_name (str) : The prefix of the job timer name (e.g. model_submission_job)

        Return:
         timer_lists (dict): key=timer_name and value=list of start -- stop times
    """
    # There are two big issues here to be resolved
    # 1. There is repetition of creating SimulationTime instance, with shell_config_parameters
    # 2. This occurs because this should be set upstream in config_manager, but how precisely to
    # populate templates has not really been properly figured out yet.

    # The details of these timers might need to change (e.g. for different sequencers or even flows), 
    # and others could easily be added by referring to the time_manager "_generate_job_start_end_date_lists" 
    # function. In fact, those calls might really belong here.

    # Use the time_manager module to get detailed timers for insertion into shell_parameters
    simTime = SimulationTime.from_kwargs(**run_dates)
    format_str="%Y-%m-%d %H:%M:%S"
    cftime_start_list = getattr(simTime, timer_name + '_start_list')
    cftime_stop_list = getattr(simTime, timer_name + '_stop_list')
    if not (cftime_start_list and cftime_stop_list):
        raise ValueError(f"The timer name: {timer_name} did not return any content.")
    timeliststr=[]
    for (start_time, stop_time) in zip(cftime_start_list, cftime_stop_list):
        restart_time=start_time - timedelta(seconds=1)
        timeliststr.append(f"{restart_time},{start_time.strftime(format_str)},{stop_time.strftime(format_str)}")
    return timeliststr  
