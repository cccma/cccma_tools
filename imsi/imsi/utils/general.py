from datetime import datetime
import re
import shutil
from typing import List

def write_shell_script(file_path: str, script_content: List[str], mode='a'):
    """Write shell script content to a file."""
    with open(file_path, mode) as f:
        f.write('\n'.join(script_content))

def get_date_string():
    """Return a datestring of now time"""

    now = datetime.now() # current date and time
    return now.strftime("%Y-%m-%d %H:%M")

def delete_or_abort(path):
    """Asks a user for input to abort or delete and replace an existing directory.
    """
    choice = input(f'{path} already exists: Abort (a) or replace (r)?')
    if choice == "a":
        print( "Exiting")
        exit()
    elif choice == "r":
        print(f"Deleting {path}")
        shutil.rmtree(path)
    else:
        print("Invalid input! Choices are 'a' or 'r'")
        delete_or_abort(path)

def _get_memory_unit_factors_to_bytes(base=2):
    # conversion factors for memory unit to bytes
    if base == 2:
        return {'K': 1 << 10, 'M': 1 << 20, 'G': 1 << 30}
    elif base == 10:
        return {'K': 1e3, 'M': 1e6, 'G': 1e9}
    else:
        raise ValueError('base must be 2 or 10')

def parse_memory_string_to_bytes(memory : str, base=2):
    """Parses a string that specifies memory value and unit and
    returns the value in bytes.

    Parameters
        memory : a string composed of the value and units, where units
            are denoted as KB, MB, or GB (case insensitive, with or
            without trailing 'b'). Space is permitted between the value
            and units.
        base : either 2 or 10, denoting the conversion from the input
            units to bytes. Use with caution. Default 2.

    Returns
        value of the information in bytes

    Example
    >>> parse_memory_string_to_bytes("1 kb")
    1024
    >>> parse_memory_string_to_bytes("10GB")
    10737418240
    """
    unit_map = _get_memory_unit_factors_to_bytes(base)
    M = memory.upper()
    valid = re.search(r'([0-9]+)\s?([K|G|M]{1}[B]{0,1})', M)
    if valid is None:
        raise ValueError(f"unsupported memory format for '{memory}'; units must be one of {{K, G, M, KB, GB, MB}} (case insensitive)")
    value = int(valid.group(1))
    units = valid.group(2).rstrip('B')
    nbytes = int(value * unit_map[units])
    return nbytes
