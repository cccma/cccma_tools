"""
maestro_cap
===========

A module used to initialize a maestro experiment folder and propagate
configuration for a maestro experiment from imsi to corresponding files
of the experiment.

Maestro is a suite of tools used to create and submit tasks to
computer systems. Configuring a new maestro experiment is usually done
via command-line tools and/or the Maestro Manager program `xm`.

This imsi-maestro interface is not fully generalized (ie. there is no
"python module for maestro"). Rather, this module depends on some
initial setup in native maestro to generate the module and resource
files for an experiment. These files are then used as templates
("default"/"source"). Specific flow configuration and job resources can
be set through imsi. In other words, given a pre-made maestro experiment
with defined flow, jobs, and resources, the user can use imsi and these
functions to set specific resources or switch out one template
flow file (flow.xml) for another.

Usage notes:
- the user cannot generate a new flow/task from this module alone
(templates must be specified).
- duplicate job names in maestro are allowed (under different tasks);
for job resources configuration from imsi will be applied to all of these
jobs identically.

Maestro and its tools were written by the Canadian Meteorological Centre.

There is no planned future development for maestro, but you may still
refer to the following internal resources:

- the maestro man page (`man maestro`)
- CMC wiki https://wiki.cmc.ec.gc.ca/wiki/Maestro
- maestro git repository https://gitlab.science.gc.ca/cmoi/maestro

"""

import os
import glob
import shutil
import subprocess
import time
from xml.etree import ElementTree

from imsi.config_manager.config_manager import Configuration
from imsi.utils.general import parse_memory_string_to_bytes, _get_memory_unit_factors_to_bytes
from imsi.utils.nml_tools import update_env_file
from imsi.time_manager.time_manager import SimulationTime
from imsi.sequencer_interface.sequencers import Sequencer

_exec_requirements = [
    'maestro',
    'expbegin',
    'makelinks',
]

class MaestroSequencerInterface(Sequencer):
    """
    The maestro sequencer interface
    """

    # define list of attributes allowed in <BATCH> tags in
    # <NODE_RESOURCE>. Note: Every attributes in the BATCH tag is
    # optional (defaults will be used by maestro if not provided).
    # See:
    #   https://wiki.cmc.ec.gc.ca/wiki/Maestro/sequencer#Batch_System_Resources
    _NODE_RESOURCE_BATCH_ATTRIBUTES = [
        'catchup', 'cpu', 'cpu_multiplier', 'immediate', 'machine',
        'memory', 'mpi', 'queue', 'soumet_args', 'wallclock'
        ]

    # imsi -> maestro for <BATCH> attributes
    _NODE_RESOURCE_BATCH_ATTRIBUTE_ALIASES = {
        'processors': 'cpu'
    }

    def __init__(self):
        # v not great v
        _check_execs_available()

    def _check_if_setup(self, seq_exp_home: str):
        # rough check to see if setup already run
        # (weak check for some paths that exist)
        modules_path = os.path.join(seq_exp_home, 'modules')
        _required_structure = [os.path.join(seq_exp_home, 'EntryModule'), modules_path]
        _empty = False
        if os.path.exists(modules_path):
            if len(os.listdir(modules_path)) == 0:
                _empty = True

        if not any([os.path.exists(f) for f in _required_structure]) or _empty:
            raise OSError("maestro sequencer folder not setup; can't continue configuration.")

    def setup(self, configuration: Configuration):
        """Setup the maestro experiment folder."""
        # Note: this setup function currently mimics most of the
        # functionality and function structure of the CanESM system
        # setup-maestro script in CCCma_tools/maestro-suite/default-config-canesm

        runid = configuration.get_subconfig('setup_params')['parameters']['runid']
        sequencing_config = configuration.get_subconfig('sequencing')['parameters']
        maestro_config = sequencing_config.get('sequencer')

        # TODO: exp_flow should be resolved in imsi so that
        # model_config and base_flow are no longer required to get
        # the right subset for the requested experiment setup
        model_config = configuration.get_subconfig('merged_model_experiment')['model_config']
        base_flow = sequencing_config['sequencing_flow']['base_flow']

        exp_flow = maestro_config['baseflows'][model_config][base_flow]

        maestro_src = exp_flow['maestro_defaults_src']          # source of templates
        maestro_dst = maestro_config['SEQ_EXP_HOME']            # destination for setup
        modules_provided = exp_flow['flow_definitions'].keys()  # ie folder names

        setup_maestro_from_src(maestro_src, maestro_dst, modules_provided, runid=runid)

    def config(self, configuration: Configuration):
        """Configure maestro experiment."""
        # Note: the original config-maestro script in
        #   CCCma_tools/maestro-suite/default-config-canesm
        # does three main "steps":
        #   - sets experiment resources (to resources.def and node
        #     resource xml files, via set_resources())
        #   - sets up the experiment flow (related to flow.xml files
        #      for each module, via set_experiment_flow())
        #   - sets experiment options (to ExpOptions.xml, via
        #     set_expoptions_file())
        # The steps can be done in any order.

        runid = configuration.get_subconfig('setup_params')['parameters']['runid']
        sequencing_config = configuration.get_subconfig('sequencing')['parameters']
        maestro_config = sequencing_config.get('sequencer')
        flow_config = sequencing_config.get('sequencing_flow')

        # FIXME/TODO: exp_flow should be resolved so that model_config and
        # base_flow are no longer required
        model_config = configuration.get_subconfig('merged_model_experiment')['model_config']
        base_flow = sequencing_config['sequencing_flow']['base_flow']

        exp_flow = maestro_config['baseflows'][model_config][base_flow]
        flow_files = exp_flow['flow_definitions']

        # TODO consider removing this imsi dependency just to access
        # the number of model/postproc iterations
        n_model_chunks, n_postproc_chunks = _get_run_chunking(sequencing_config['run_dates'])
        target_dependency_loop_ratio = n_model_chunks/n_postproc_chunks  # dynamic adj

        # folder structure
        # root_dir is the path to the maetro experiment, ie SEQ_EXP_HOME
        # defaults is special, but other structure under root_dir is not
        defaults_dir = maestro_config['baseflows'][model_config][base_flow]['maestro_defaults_src']
        exp_root_dir = maestro_config['SEQ_EXP_HOME']

        if not os.path.exists(defaults_dir):
            raise FileNotFoundError(
                f"no folder for maestro experiment template files found: {defaults_dir}"
                )

        #-- check
        self._check_if_setup(exp_root_dir)

        # FIXME: suppose this could be part of the experiment flow
        # definition structure, eg:
        # {"sequencing": {"sequencers": {"maestro" : {"baseflows": {
        #     "AMIP": {"canam_split_job_flow": {
        #         "flow_definitions"  : {
        #             "postproc" : "..."
        #         },
        #         "flow_adjust_loop_dependency_ratio": {
        #             "postproc" : {
        #                 "from": ["rebuild_loop", "diagnostics_loop", "wrap_up_loop"],
        #                 "to": ["model_loop", "model_loop", "model_loop"]
        #             },
        #         }
        # for now, define separate dict hardcoded here (otherwise would just use exp_flow)
        exp_flow_adj = {'flow_adjust_loop_dependency_ratio': {
            "postproc" : {
                "from": ["rebuild_loop", "diagnostics_loop", "wrap_up_loop"],
                "to": ["model_loop", "model_loop", "model_loop"]
                }
            }
        }
        loop_adjustments = exp_flow_adj.get('flow_adjust_loop_dependency_ratio', {})

        # note: in the imsi framework, most resources for maestro jobs
        # are specific at the job-level, so this section is quite
        # small
        # FIXME still some assumptions about naming/structure here
        updates = {
            'SEQ_JOBNAME_PREFIX': generate_maestro_jobname_prefix(runid),
            'NUMBER_OF_POSTPROC_LOOPS': n_postproc_chunks,
            'NUMBER_OF_MODEL_LOOPS': n_model_chunks
        }

        config_maestro_from_src(defaults_dir, exp_root_dir, runid, flow_files,
                    dependency_loop_ratio=target_dependency_loop_ratio,
                    dependency_loop_adjustments=loop_adjustments,
                    dependency_loop_iter=n_postproc_chunks,
                    resource_def_config=updates,
                    job_resource_config=flow_config['jobs'])

    def submit(self, configuration: Configuration):
        """Submit a maestro job using `expbegin`.

        Note that this call will pass the current time for the datestring
        argument (`-d`), to the nearest minute only.
        """
        # Note: TODO setting the date for expbegin does NOT currently
        # mimic the behaviour of expbegin (which can automatically
        # increment the date if the same job is resubmitted, will
        # default to unix epoch time if empty, and can accept less
        # precision and pad the rest of the string with zeros).
        #
        # For now, the behaviour is to resolve the current time to the
        # nearest minute only (seconds are always "00", YYYYMMDDhhmm00)
        dtstring = get_maestro_current_datetime_string()

        expbegin_path = _get_exec_fullpath('expbegin')
        SEQ_EXP_HOME = configuration.get_subconfig('sequencing')['parameters']['sequencer']['SEQ_EXP_HOME']

        cmd = [expbegin_path, '-e', SEQ_EXP_HOME, '-d', dtstring]

        try:
            proc = subprocess.run(cmd, capture_output=True)
            proc.check_returncode()
        except subprocess.CalledProcessError as e:
            cmd_string = ' '.join(proc.args)

            # error handling in/via expbegin is inconsistent (the 'error
            # message' might be via stdout or sterr, so capture/print both)
            out = proc.stdout.decode().strip()
            err = proc.stderr.decode().strip()
            out = out if not out else '\n' + out
            err = err if not err else '\n' + err

            raise ChildProcessError(f"Command failed: {cmd_string}\n{out}{err}") from e

def _check_execs_available(names=_exec_requirements):
    # lazy check if executables are available
    for n in names:
        _get_exec_fullpath(n)

def setup_maestro_from_src(src: str, dst: str, modules: list[str], runid: str = ''):
    """Setup a maestro experiment from a template source `src` at the
    destination path `dst`.

    The `src` must contain `/modules` and its contents, and a
    `EntryModule` link (optionally: `experiment.cfg`).

    Parameters:
        src : path to maestro experiment template folder. This should
            contain all required files and folders for the maestro
            experiment modules, in the expected structure.
        dst : path to destination experiment folder.
        modules : list of folder (module) names, created under `/modules`.
        runid : model run ID specified by user (usually corresponds
            to filename of $SEQ_EXP_HOME)
    """
    prepare_dst_directory(dst)
    init_folders(src, dst, modules)
    config_hub(dst, runid)

def prepare_dst_directory(dst: str):
    """Create a folder for a maestro experiment
    (the desired path for maestro's `$SEQ_EXP_HOME`).
    """
    shutil.rmtree(dst, ignore_errors=True)
    os.makedirs(dst)

def init_folders(src: str, dst: str, modules: list[str]):
    """Initialize maestro experiment folder structure at a destination
    path `dst` using a template source `src`.

    The `src` must contain `/modules` and its contents and a `EntryModule`
    link (optionally: `experiment.cfg`).

    Parameters:
        src : path to maestro experiment template folder. This should
            contain all required files and folders for the maestro
            experiment modules, in the expected structure.
        dst : path to destination experiment folder.
        modules : list of folder (module) names, created under `/modules`.
    """
    # initialize maestro folder structure
    init_maestro_experiment_folders(dst)

    copy_default_main_config(src, dst)

    # create module folders and link
    src_modules = os.path.join(src, 'modules')    # defaults
    dst_modules = os.path.join(dst, 'modules')

    entrymodule_name = get_entrymodule_name(src)
    init_maestro_module_folders(dst, entry_module=entrymodule_name, modules=modules)

    create_maestro_modules_from_src(src_modules, dst_modules)

    # symlink defaults folder
    os.symlink(os.path.join(src, "defaults"), os.path.join(dst, "defaults"))

def config_hub(seq_exp_home, experiment):
    """Create the host-specific directories in the maestro
    experiment `/hub`.

    This uses the `makelinks` program to create links to local storage
    on the appropriate machines. Note: this does not pass additional
    arguments to `makelinks`.
    """

    # setup links to machines using the makelinks maestro tool using
    # the makelinks utility (unless specified, uses
    # ~/.suites/.default_links)
    # makelinks requires:
    #   - the running location to be SEQ_EXP_HOME -> cwd
    #   - SEQ_EXP_HOME to be set as an env variable -> env
    # TODO the makelinks tool isn't overly complicated; in the future
    # it might desirable to remove this dependency in favour of
    # having the same functionality implemented here.

    makelinks_path = _get_exec_fullpath('makelinks')

    maestro_env = None
    if 'SEQ_EXP_HOME' not in os.environ:
        # this is a weak check (could be set but empty/DNE)
        maestro_env = os.environ.copy()
        maestro_env['SEQ_EXP_HOME'] = seq_exp_home

    cmd = [makelinks_path, '-f', f'--experiment={experiment}']

    try:
        proc = subprocess.run(cmd, cwd=seq_exp_home, env=maestro_env, capture_output=True)
        proc.check_returncode()
    except subprocess.CalledProcessError as e:
        raise Exception("Failed call: '{cmd}': {err}".format(cmd=' '.join(proc.args), err=proc.stderr))

def copy_default_main_config(src: str, dst: str):
    """Copy the configuration `experiment.cfg` file defined in
    source `src` directory to the destination `dst` directory.

    References:
        [1] https://wiki.cmc.ec.gc.ca/wiki/Maestro/experiment.cfg
    """
    exp_cfg = os.path.join(src, 'experiment.cfg')
    if os.path.exists(exp_cfg):
        shutil.copy2(exp_cfg, dst)

def init_maestro_module_folders(path: str, entry_module: str = 'module', modules: list[str] = None):
    """Initialize the experiment folder structure as required by maestro.

    Parameters:
        path: path to maestro experiment folder (top level), which contains
            the `/modules` folder (among others).
        entry_module: name of entry module, required by maestro. The
            EntryModule is then symlinked to the folder under
            `/modules/{entry_module}`
        modules: list of names of modules (subfolders). If None, will only
            include the `entry_module`.

    References:
        [1] https://wiki.cmc.ec.gc.ca/wiki/Maestro_Files_and_Folders
    """

    modules_path = os.path.join(path, 'modules')
    if not os.path.exists(modules_path):
        os.makedirs(modules_path)

    if modules is None:
        modules = [entry_module]

    if not entry_module in modules:
        raise ValueError('entry_module must match one of modules provided')

    for m in modules:
        os.makedirs(os.path.join(modules_path, m))

    # this is a relative symlink
    os.symlink(os.path.join('modules', entry_module), os.path.join(path, "EntryModule"))

def init_maestro_experiment_folders(path: str):
    """Initialize the experiment folder structure at `path` as
    required by maestro. Only the folder structure is created;
    no folders are populated.

    References:
        [1] https://wiki.cmc.ec.gc.ca/wiki/Maestro_Files_and_Folders
    """

    # required by maestro
    folders = ['modules', 'resources', 'hub', 'listings', 'logs', 'sequencing']

    for f in folders:
        os.makedirs(os.path.join(path, f))

def get_entrymodule_name(path):
    """Given the `path` to EntryModule, get the real name of the module"""
    # note: path is the input to enforce finding the file with the
    # literal name "EntryModule" (maestro requirement)
    em = os.path.join(path, "EntryModule")
    if not os.path.exists(path) or not os.path.islink(em):
        raise FileNotFoundError(f"EntryModule not found; EntryModule must be a symlink located in {path}.")
    # get name of folder that EntryModule links to
    name = os.path.basename(os.path.realpath(em))
    return name

def create_maestro_modules_from_src(src: str, dst: str):
    """Create maestro module structure from `src`/modules
    (default/templates) to `dst`/modules.

    Folder structure is copied, files (cfg, tsk) are symlinked.
    """

    if not os.path.basename(os.path.normpath(dst)) == 'modules':
        raise ValueError("dst path must be a folder named 'modules' for maestro")

    # get list of all files (recursively)
    src_files = [p for p in glob.glob(os.path.join(src, "**"), recursive=True) if os.path.isfile(p)]

    # only include folders that are subfolders of src and immediate
    # subfolders, ie. do not include src, src/module1, src/module2, etc.
    allfolders = glob.glob(os.path.join(src, '*/**/'), recursive=True)

    # use normpath before dirname to account for potential trailing os.sep
    # TODO a pathlib.Path solution would be cleaner:
    # [f.relative_to(src) for f in Path(src).rglob(".") if not f.samefile(src) and not f.parent.samefile(src)]
    src_rel_folders = [os.path.relpath(f, src) for f in allfolders if os.path.dirname(os.path.normpath(f)) != src]

    # create local dirs within modules
    for f in src_rel_folders:
        dst_folder = os.path.join(dst, f)
        if os.path.exists(dst_folder):
            shutil.rmtree(f)
        os.makedirs(dst_folder)

    # link in version controlled files
    for f in src_files:
        s = os.path.relpath(f, src)
        os.symlink(f, os.path.join(dst, s))

def create_experiment_flow_file(src: str, dst: str):
    """Create a `flow.xml` file at the destination path `dst` by copying
    a template flow file (`xml`) `src`. While the `src` filename
    (basename) can be any name, the resulting file at `dst` is always
    `flow.xml` (maestro requirement).

    This function can be used to swap out different flow files for the
    same experiment.
    """
    if not os.path.exists(src):
        raise FileNotFoundError(f'no existing flow template file found: {src}')
    if not os.path.exists(dst):
        raise FileNotFoundError(f'no existing module set up for {dst}')
    if not os.path.basename(os.path.dirname(os.path.normpath(dst))) == 'modules':
        raise ValueError(f'incorrect destination for modules: {dst}')

    dst_flow_file = os.path.join(dst, 'flow.xml')
    shutil.copy(src, dst_flow_file)

def update_experiment_options_file(src, dst, short_name=None, display_name=None):
    """Update the attributes of the `ExpOptions` tag in the ExpOptions.xml
    file.

    The `ExpOptions.xml` is an optional file used in `xflow_overview` to
    help display experiment information.

    Note: This function has extremely limited functionality. It is
    currently only used to update two attributes on the root node
    ExpOptions tag.

    References:
        [1] https://wiki.cmc.ec.gc.ca/wiki/Maestro/ExpOptions.xml
    """

    expoptions_basename = "ExpOptions.xml"

    if short_name is None and display_name is None:
        raise ValueError('one of short_name or display_name must be specified')

    default_expoptions_file = os.path.join(src, expoptions_basename)
    if not os.path.exists(default_expoptions_file):
        raise FileNotFoundError(f'no default {default_expoptions_file} file found in {src}')

    target_expoptions_file = os.path.join(dst, expoptions_basename)
    shutil.copy2(default_expoptions_file, target_expoptions_file)

    # set attributes
    exop = ElementTree.parse(target_expoptions_file)
    root = exop.getroot()
    if short_name:
        root.set("shortName", short_name)
    if display_name:
        root.set("displayName", display_name)
    exop.write(target_expoptions_file)

def set_dependency_loop_index_by_ratio(
        flow_file: str, from_loop: str, to_loop: str, ratio: int = 1,
        n_iter : int = 1, start_iter: int = 1):
    """Set the `DEPENDS_ON` loop dependency by ratio between the index
    and the local loop, and generate each `DEPENDS_ON` element for
    the requested number of iterations.

    From the maestro docs, "a `DEPENDS_ON` element can specify
    dependencies. It tells the sequencer that the current node cannot
    be submitted until the dependency is satisfied." This function
    allows for setting iteration `N` of one task (`from_loop`) to
    depend on iteration `N` of another task (`to_loop`), ie.
    `M = r * N`, where `r` is a constant (`ratio`).

    This function edits the `flow.xml` file to 'manually' add each
    dependency loop, starting from `start_iter` index until the total
    number of iterations (`n_iter`) are reached.

    Note: This function does not check the current state of the
    `DEPENDS_ON` element. If `ratio = 1` or `n_iter = 1`, then no edits
    will be made, but this does not ensure that there is a 1-to-1
    dependency.

    Parameters:
        flow_file : path to flow file (usually `flow.xml`). Must contain
            at least one `LOOP` with name `to_loop`, which has at
            least one dependency `DEPENDS_ON` element.
        from_loop : name of loop that is the `dep_name` (dependency name)
            and is submitted for every `index`.
        to_loop : name of loop that will be submitted for every
            `local_index`.
        ratio : ratio of loop iteration between `from_loop` and `to_loop`.
        n_iter : total number of iterations of the `to_loop` (`local_index`).
        start_iter : starting index of first iteration.

    Examples:

    In `flow.xml`, a 1-to-1 loop dependency is written as:

    ```xml
    <!-- input flow.xml -->
    <MODULE name="postproc">
        <SUBMITS sub_name="task_loop" />
        <LOOP name="task_loop">
            <DEPENDS_ON dep_name="model_loop" index="model_loop=$((LOOP_INDEX))"
            local_index="task_loop=$((LOOP_INDEX))"
            />
        </LOOP>
    </MODULE>
    ```

    For illustration, use this function to modify the original xml
    with a 2-to-1 loop dependency (one iteration of `task_loop`
    will run after 2 iterations of `model_loop`):

    >>> set_dependency_loop_index_by_ratio('flow.xml',
    from_loop='task_loop', to_loop='model_loop', ratio=2, n_iter=3)

    ```xml
    <!-- output flow.xml -->
    <MODULE name="postproc">
        <SUBMITS sub_name="task_loop" />
        <LOOP name="task_loop">
            <DEPENDS_ON dep_name="model_loop" index="model_loop=2" local_index="task_loop=1" />
            <DEPENDS_ON dep_name="model_loop" index="model_loop=4" local_index="task_loop=2" />
            <DEPENDS_ON dep_name="model_loop" index="model_loop=6" local_index="task_loop=3" />
        </LOOP>
    </MODULE>
    ```

    References:
        [1] https://wiki.cmc.ec.gc.ca/wiki/Maestro/dependency
    """
    # Note: an issue was opened to add this feature directly into
    # maestro. The issue has since been addressed and closed, but has
    # not been tested yet by our team. Once confirmed, this function
    # may be greatly simplified if the LOOP_INDEX still needs to be set
    # dynamically.
    # https://gitlab.science.gc.ca/CMOI/maestro/-/issues/423

    if not float(ratio).is_integer():
        raise ValueError(f'ratio must be integer greater than one, not {ratio}')

    if n_iter < 1:
        raise ValueError(f'n_iter must be integer greater than one, not {n_iter}')
    elif n_iter > 1 or ratio > 1:
        # Note: (n_iter == 1 and ratio == 1) means there's nothing to update

        tree = ElementTree.parse(flow_file)
        root = tree.getroot()

        # find the loop node
        # ensure that there is only one LOOP named loop_name
        target_loop = root.findall(f'.//LOOP[@name="{from_loop}"]')
        if not target_loop:
            raise ValueError(f'malformed flow file {flow_file}; does not contain loop {from_loop}')
        if len(target_loop) > 1:
            raise ValueError(f'malformed flow file {flow_file}; contains more than one loop for {from_loop}')
        target_loop = target_loop[0]  # safe

        # find all nodes in DEPENDS_ON node
        target_dep_nodes = list(target_loop.findall("DEPENDS_ON"))

        # make sure the dep_name for each DEPENDS_ON node matches to_loop
        filtered_nodes = []
        for node in target_dep_nodes:
            if os.path.basename(node.get('dep_name')) == to_loop:
                filtered_nodes.append(node)

        if not filtered_nodes:
            raise ValueError(f'requested to_loop {to_loop} does not exist in flow file {flow_file}')

        # now that the list is filtered, use the current attribs for the
        # DEPENDS_ON nodes and reconstruct new nodes with modified attribs
        for node in filtered_nodes:

            # get attribs
            path_to_dep_target = os.path.basename(node.get('dep_name'))
            index_name = node.get('index').split('=')[0]
            local_index_name = node.get('local_index').split('=')[0]
            tail = node.tail

            # remove node
            target_loop.remove(node)

            # loop to construct new nodes based on original attribs
            for idx,i in enumerate(range(start_iter, n_iter+1)):
                target_index = int(i*ratio)
                target_index_def = f'{index_name}={target_index:d}'
                target_local_index_def = f'{local_index_name}={i:d}'

                # create xml element
                new_dep_node = ElementTree.Element('DEPENDS_ON')
                new_dep_node.set('dep_name', path_to_dep_target)
                new_dep_node.set('index', target_index_def)
                new_dep_node.set('local_index', target_local_index_def)
                new_dep_node.tail = tail

                # insert to node
                target_loop.insert(idx, new_dep_node)

        # save updated flow (overwrite)
        tree.write(flow_file)

def set_resources_def(resource_file: str, updates: dict = None):
    """Update parameters in resources.def file"""
    update_env_file(resource_file, updates=updates, key_value_only=True)

def config_node_resource_file(filename: str, resources: dict, aliases: dict = None):
    """Set attributes in node resource job file.

    Parameters:
        filename : full filename of `.xml`
        resources : dict of resources to set. See Note [1].
        aliases : mapping of keys in `resources` dict to name of
            attributes allowed in maestro NODE_RESOURCES (key should
            be name in `resources`, value should be name for maestro
            node resource attribute).

    Note:
        [1] the 'memory' resource should be set by total memory (not
        memory per cpu as required by maestro; this will be handled
        internally).

    Examples:

    Given the input file `xml_file`:

    ```xml
    <NODE_RESOURCES><BATCH machine="machine" wallclock="10" /></NODE_RESOURCES>
    ```
    >>> config_node_resource_file(xml_file,
        resources={'amount_of_time': '120', 'memory': '40 g'},
        aliases={'amount_of_time': 'wallclock'})

    Output:
    ```xml
    <NODE_RESOURCES><BATCH machine="machine" wallclock="120" memory="40G" /></NODE_RESOURCES>
    ```
    """
    # note: aliases are used here just so that we can avoid renaming
    # the keys of the input resources dict

    aliases = {} if aliases is None else aliases

    # attributes for NODE_RESOURCES accepted by maestro.
    # use these as a simple check for valid config; if these are
    # variable, suggest to implement a check via `nodeinfo` instead.
    accepted_attributes = MaestroSequencerInterface._NODE_RESOURCE_BATCH_ATTRIBUTES

    contents = ElementTree.parse(filename)
    root = contents.getroot()
    if root.tag != 'NODE_RESOURCES':
        # confirm that this is the correct xml contents
        raise ValueError(f'{filename} must begin with a <NODE_RESOURCE> tag')

    batch_node = contents.findall('BATCH')
    if len(batch_node) != 1:
        raise ValueError(f'{filename} must only contain one <BATCH> tag')
    batch_node = batch_node[0]   # safe

    for k, v in resources.items():

        if k in aliases:
            k = aliases[k]

        if k not in accepted_attributes:
            # not permitted in maestro BATCH resource
            print(f"'{k}' not permitted in <BATCH> for maestro "
                  f"node resources {filename}; skipping.")
            continue

        if not v:
            # empty (""), nothing to replace
            # NOTE: not used to "null" in template
            # (TODO also have not tested if empty value in xml
            # works for maestro)
            continue

        # check value of specific attributes
        if k == 'wallclock':
            _maestro_check_wallclock(v)
        elif k == 'memory':
            proc = int(resources.get('processors', 1))
            v = _imsi_to_maestro_memory_resource(v, processors=proc, unit='G')

        batch_node.set(k, v)

    contents.write(filename)

def copy_resources_from_src(src, dst):
    # copy in the structure/files
    if not os.path.exists(dst) or not any(os.scandir(dst)):
        # if doesn't exist or empty
        # dirs_exist_ok will then only ignore parent dir (desired)
        shutil.copytree(src, dst, copy_function=shutil.copy2, dirs_exist_ok=True)
    else:
        # TODO this is the previous behaviour- should this do something different? (raise / recopy?)
        print('resource directory already populated')

def _get_run_chunking(run_dates_dict):
    # run_dates_dict is the dict required to create a SimulationTime
    # object using the from_kwargs method.
    # this could be more explicit (ie only use args needed to generate
    # chunk sizes), but using built in object for now:
    sim_timers = SimulationTime.from_kwargs(**run_dates_dict)
    n_model_chunks = sim_timers.number_of_model_chunks
    n_postproc_chunks = sim_timers.number_of_postproc_chunks
    return n_model_chunks, n_postproc_chunks

def _create_basename_lookup(filelist):
    """Given a list of files (`filelist`), create lookup dictionary
    by filename (keys) to all file paths with the same filename
    (values). Each list is the full path including the original
    basename (with extension). Real/abs paths are not resolved,
    duplicates are preserved.

    Examples:
    >>> _create_basename_lookup(['/path/to/file_a', '/path/to/file_b', 'path/to/another/file_a'])
    {'file_a': ['/path/to/file_a', 'path/to/another/file_a'],
     'file_b': ['/path/to/file_b']}
    >>> _create_basename_lookup(['/path/to/file_a', '/path/to/file_a'])  # duplicates
    {'file_a': ['/path/to/file_a', '/path/to/file_a']}
    """
    basename_lookup = {}
    for f in filelist:
        fname = os.path.splitext(os.path.basename(f))[0]
        if fname in basename_lookup:
            basename_lookup[fname].append(f)
        else:
            basename_lookup[fname] = [f]
    return basename_lookup

def generate_maestro_jobname_prefix(s, sep='-') -> str:
    """Generate a jobname prefix start with `s` and ending with `sep`.
    By convention, `s` is usually the "runid" of the job. `sep` is only
    added if `s` does not already end with `sep`.
    """
    if s.endswith(sep):
        return s
    else:
        return f'{s}{sep}'

def config_maestro_from_src(
        src: str, seq_exp_home: str, runid: str, flow_files_lookup: dict,
        dependency_loop_ratio: int = 1,
        dependency_loop_iter: int = 1,
        dependency_loop_adjustments: dict = None,
        resource_def_config: dict = None,
        job_resource_config: dict = None):
    """Configure the maestro experiment resources from templates (source).
    Set the main `flow.xml` files and configure the job node resource
    `.xml` files with requested resources.

    Parameters:
        src : path to maestro experiment template folder that contains
            the template files under a `/resources` folder.
        seq_exp_home : destination for maestro sequencer setup.
        runid : model run ID specified by user (usually corresponds
            to filename of $SEQ_EXP_HOME)
        flow_files_lookup : dict where keys are the name of the maestro
            experiment module and values are paths to the template flow
            files. These template flow files may have any name and will
            be renamed to `flow.xml` in the appropriate destination
            module folder when copied.
        dependency_loop_ratio : int
        dependency_loop_iter : int
        dependency_loop_adjustments : dict
        resource_def_config : where keys are names of a maestro resource
            resource in `resources.def` and values are the desired setting.
        job_resource_config : nested dict of settings for each maestro
            job, which contains a dict of 'resources' for maestro. eg:
            {'job_name_A' : {'resources': {'wallclock': '120'}}}
    """

    # src / templates
    resources_src_dir = os.path.join(src, 'defaults', 'resources')

    # dst / experiment folder
    modules_dst_dir = os.path.join(seq_exp_home, 'modules')
    resources_dst_dir = os.path.join(seq_exp_home, 'resources')

    dlr = dependency_loop_ratio
    dli = dependency_loop_iter
    dla = dependency_loop_adjustments

    #-- set experiment options (ExpOptions)
    update_experiment_options_file(
        os.path.join(src, 'defaults'), seq_exp_home,
        short_name=runid, display_name=runid
        )

    #-- set module flows (flow.xml)
    for module, src_flow_file in flow_files_lookup.items():

        create_experiment_flow_file(src_flow_file, os.path.join(modules_dst_dir, module))

        # special edits as defined in dla for dependency loops
        # (maestro DEPENDS_ON)
        if module in dla:
            # ratio of n loops of different experiment components
            # (see info in set_dependency_loop_index_by_ratio)
            if dlr > 1:
                adjust_flow = os.path.join(modules_dst_dir, module, 'flow.xml')
                for to_loop,from_loop in zip(dla[module]['to'], dla[module]['from']):
                    set_dependency_loop_index_by_ratio(
                        from_loop=from_loop,
                        to_loop=to_loop,
                        flow_file=adjust_flow,
                        ratio=dlr,
                        n_iter=dli
                        )

    #-- set common task resources (resources.def)
    copy_resources_from_src(resources_src_dir, resources_dst_dir)

    # made by running copy_resources_from_src (risky?)
    resource_file = os.path.join(resources_dst_dir, 'resources.def')

    set_resources_def(resource_file=resource_file, updates=resource_def_config)

    #-- set specific job node resources (xml NODE_RESOURCES)

    # get all the xml files - use the ones in the dst experiment folder
    # (they have been copied in and now need "updating" with specific
    # configuration)
    taskfiles = glob.glob(os.path.join(resources_dst_dir, '**/*.xml'), recursive=True)

    # the lookup below handles duplicate filenames between different
    # folders. This assumes that the file contents and resources
    # requested are IDENTICAL.
    taskfile_lookup = _create_basename_lookup(taskfiles)

    # configure resources for each job
    #
    # this loop goes over the the jobs defined in the json; this means
    # there can be templates for other jobs/resources that are not
    # explicitly configured through the imsi interface.
    # note: therefore, with this loop over jobs from json, there is no need
    # to explicitly handle (skip) 'container.xml' files
    for name,spec in job_resource_config.items():

        # find corresponding task file
        try:
            taskfiles = taskfile_lookup[name]
        except KeyError:
            # if the job is defined in imsi, then it must exist in
            # maestro defaults -> raise if not
            raise FileNotFoundError(
                    "no corresponding maestro task file found "
                    f"for '{name}' in sequencing_flow; settings "
                    "will not be applied"
                    )

        resources = spec['resources']

        for tf in taskfiles:
            config_node_resource_file(
                tf, resources=resources,
                aliases=MaestroSequencerInterface._NODE_RESOURCE_BATCH_ATTRIBUTE_ALIASES
            )

def _maestro_check_wallclock(duration):
    """Check wallclock value format"""
    if not float(duration).is_integer():
        raise ValueError(f'invalid wallclock value: {duration}')

def _maestro_format_resource_memory(nbytes, unit='G', precision=0, base=2):
    """Take number of bytes `nbytes` and return a string formatted with
    the memory in the units of `unit`. The string can be used for
    configuring maestro resources (xmls).
    """
    unit_map = _get_memory_unit_factors_to_bytes(base)
    cm = nbytes / unit_map[unit]
    return '{cm:.{n}f}{unit}'.format(cm=cm, unit=unit, n=precision)

def _imsi_to_maestro_memory_resource(imsi_memory: str, processors=1, unit='G', precision=0):
    """Convert memory specified in imsi (total memory) to maestro
    requirement (memory per cpu/processor).

    Examples:
    >>> _imsi_to_maestro_memory_resource("120GB")
    '120G'
    >>> _imsi_to_maestro_memory_resource("480 GB", processors=240)
    '2G'

    References:
        [1] https://wiki.cmc.ec.gc.ca/wiki/Maestro/sequencer#Batch_System_Resources
        [2] https://portal.science.gc.ca/confluence/display/SCIDOCS/ord_soumet+Usage
    """
    base = 2     # set (though maestro/ord_soumet uses base 10) -- reconsider?
    nbytes = parse_memory_string_to_bytes(imsi_memory, base=base)
    cm_bytes = nbytes / processors    # mem per cpu
    fmt = _maestro_format_resource_memory(cm_bytes, unit=unit, precision=precision, base=base)
    return fmt

def _get_exec_fullpath(program_name: str) -> str:
    # return full path to a maestro executable -> `which`
    pgm = shutil.which(program_name)
    if pgm is None:
        raise FileNotFoundError(
            f"No program '{program_name}' found. For using the maestro "
            "sequencer, maestro-related tools must be available "
            "on your system."
            )
    return pgm

def get_os_time() -> time.struct_time:
    return time.localtime(time.time())

def get_maestro_current_datetime_string() -> str:
    """Generate a string of datetime information for the current date
    and time that can be used by maestro's `expbegin` tool, to the
    nearest minute (`YYYYMMDDhhmm00`).

    Note: while `expbegin` accepts a datetime string up to 14 characters (ie.
    `YYYYMMDDhhmmss`), this function only generates a datetime string
    to the nearest minute (forced to zero).
    """
    t = get_os_time()
    fmt = "%Y%m%d%H%M00"
    dts = time.strftime(fmt, t)
    return dts

if __name__ == "__main__":
    # development testing only
    # TODO move to official tests

    import sys, os, json
    from imsi.sequencer_interface.sequencers import create_sequencer
    from imsi.config_manager.config_manager import JsonConfigDatabase, ConfigManager

    runid = "imsi-maestro-test"
    setup_params = {
        'model_name': 'canam51_p1',
        'experiment_name': 'cmip6-amip',
        'machine_name': 'ppp6',
        'runid': runid,
        'work_dir': '/home/ncs001/imsi/imsi/shell_interface',
        'run_config_path': os.path.join('/home/ncs001/imsi/imsi/shell_interface', 'config'),
        'source_path': '/home/path',
        'imsi_config_path': '/home/ncs001/canesm/imsi-config/',
        "sequencer_name": "maestro",
        "flow_name": "canam_split_job_flow-hall6"  # resolved to 'machine'
        # "flow_name": "canam_split_job_flow"      # should fail (not resolved to 'machine')
    }

    db = JsonConfigDatabase(imsi_config_path=setup_params.get('imsi_config_path'))
    config_manager = ConfigManager(db)    # inject to db
    configuration = config_manager.create_configuration(**setup_params)

    sequencer = create_sequencer(setup_params.get('sequencer_name'))
    sequencer.setup(configuration)
    sequencer.config(configuration)

    print(json.dumps(configuration.get_subconfig('sequencing').get('parameters'), indent=2))
