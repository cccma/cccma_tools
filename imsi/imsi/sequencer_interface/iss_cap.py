"""
Imsi shell sequencer (ISS)
===========================

The ISS is a simple, highly portable shell batch sequencer for running model simulations,
and built directly adjacent to the main imsi source. 

The ISS provides a simple time-looping mechanism to handle job resubmission, and supports
submission of downstream dependencies. The iss leverages knowledge from :mod:`imsi.scheduler_tools`
to support submissions to different batch schedulers. 
"""
# Should add an overall description of how ISS works. e.g. has a .simulation.time.state file,
# relies on job scripts to alter the resubmit, creates a listing dir in wrk_dir, etc, etc.

import shutil
import os
from imsi.sequencer_interface.sequencers import Sequencer
from imsi.utils.general import get_date_string, write_shell_script
from typing import List
import stat
from imsi.scheduler_interface.schedulers import create_scheduler, Scheduler
from imsi.config_manager.config_manager import Configuration
import imsi.sequencer_interface.sss as sss
from dataclasses import asdict

class ImsiSimpleShellSequencerInterface(Sequencer):
    """
    The imsi shell sequencer cap
    """
    def setup(self,configuration: Configuration):
        # Update to include explicit paths to write files at.
        work_dir = configuration.get_unique_key_value('work_dir')
        if not os.path.isdir(work_dir):
            raise FileExistsError(f'The run working directory at {work_dir} does not exist')
        os.chdir(work_dir)

        listings_dir = os.path.join(work_dir, 'listings')
        if not os.path.isdir(listings_dir):
            os.mkdir(listings_dir)
    
    def config(self, configuration: Configuration):
        """
        Write files needed for the imsi shell sequencer, including
        the `.simulation.time.state` file, and the submission files
        for the model and diagnostics.
        """
        work_dir = configuration.get_unique_key_value('work_dir')
        if not os.path.isdir(work_dir):
            raise FileExistsError(f'The run working directory at {work_dir} does not exist')
        os.chdir(work_dir)

        # Creates scheduler and SSS sequencer/job objects
        scheduler = create_scheduler(configuration.get_unique_key_value('scheduler'))
        shell_sequencer = create_shell_sequencer(configuration, scheduler)
        model_job = create_model_job(shell_sequencer, configuration)
        shell_sequencer.add_job(model_job)
        diag_job = create_diag_job(shell_sequencer, configuration)
        shell_sequencer.add_job(diag_job)
        # Configure the files on disk
        shell_sequencer.configure()

        # Save this sequencer object for later resuse
        work_dir = configuration.get_unique_key_value('work_dir')
        if not os.path.isdir(work_dir):
            raise FileExistsError(f'The run work directory at {work_dir} does not exist')

        shell_sequencer.save_to_json(os.path.join(work_dir, '.iss.json'))

        run_config_dir = configuration.get_unique_key_value('run_config_path')
        if not os.path.isdir(run_config_dir):
            raise FileExistsError(f'The run config directory at {run_config_dir} does not exist')
        os.chdir(run_config_dir)


        # These are files which need to be extracted. They are the pre-defined shell
        # scripts for running the model and diagnostics which SSS will source within its wrapper scripts.
        # Quite brutal. We should rather have an SSS json in the imsi input files to define
        # things. Would also be useful for expanding job granularity and input info, e.g. directives
        shutil.copyfile(os.path.join(configuration.get_unique_key_value('imsi_config_path'), 'models', 
                                     configuration.get_unique_key_value('model_run_script')
                                     ),
                        configuration.get_unique_key_value('model_run_script')
                        )
        
        shutil.copyfile(os.path.join(configuration.get_unique_key_value('imsi_config_path'), 'diagnostics', 
                                     configuration.get_unique_key_value('diagnostic_run_script')
                                     ),
                        configuration.get_unique_key_value('diagnostic_run_script')
                        )   
    def submit(self, configuration: Configuration):
        work_dir = configuration.get_unique_key_value('work_dir')
        shell_sequencer = sss.SimpleShellSequencer.load_from_json(os.path.join(work_dir, '.iss.json'))
        shell_sequencer.run()

def create_shell_sequencer(configuration: Configuration, scheduler: Scheduler):
    '''Create a SimpleShellSequencer instance from a Configuration and Scheduler'''
    
    # Deal with cases where an ssh before submission is required. Don't love the try except,
    # but it is not defined for all machines. Could reconsider how this is done.
    try:
        submit_prefix=configuration.get_unique_key_value('submit_prefix') 
    except:
        submit_prefix=''
    extended_submission_command = submit_prefix + scheduler.submission_command

    return  sss.SimpleShellSequencer(
                runid=configuration.get_unique_key_value('runid'), 
                run_config_dir=configuration.get_unique_key_value('run_config_path'),          
                sequencing_scratch_dir=configuration.get_unique_key_value('scratch_dir'),
                work_dir=configuration.get_unique_key_value('work_dir'),
                scheduler=scheduler,            
                scheduler_submission_command=extended_submission_command,
                start_time=configuration.get_unique_key_value('start_time'), 
                end_time=configuration.get_unique_key_value('end_time'),                 
            )

def create_model_job(shell_sequencer: sss.SimpleShellSequencer, configuration: Configuration):
    """
    Writes a model batch submission file for a given configuration and shell_sequencer.
    Decodes imsi configuration to meet the SSS interface. 
    """

    # Basically just define the exchange of parameters across the interface
    # In a sense, this is thrashing the data. We could just meet the interface of generate_simple_shell_sequencing_content
    # directly without these classes. Perhaps they are useful, but to be reconsider if is is helpful or just uncecessary.

    return sss.SSSjob(
        submission_script_fullpath=os.path.join(shell_sequencer.work_dir, 
                                                f'imsi-submission-{shell_sequencer.runid}.sh'),
        job_identifier=f"canesm_{shell_sequencer.runid}",
        user_script_to_source=f"{shell_sequencer.run_config_dir}/{configuration.get_unique_key_value('model_run_script')}",
        directives=configuration.machine.parameters['batch_commands'].get('directives',''),#.get_unique_key_value('directives'), # super poorly labelled/handled
        listing_prefix=os.path.join(shell_sequencer.work_dir, 'listings', f"imsi_run_canesm_{shell_sequencer.runid}"),
        dependencies=[f"{shell_sequencer.work_dir}/imsi-diagnostics-{shell_sequencer.runid}.sh"],
        cycle_flag='MODEL_RESUBMIT',
        time_counter='CURRENT_YEAR',
        clean_scratch=True
    )

def create_diag_job(shell_sequencer: sss.SimpleShellSequencer, configuration: Configuration):
    """
    Writes a model batch submission file for a given configuration and shell_sequencer.
    Decodes imsi configuration to meet the SSS interface. 
    """
    # Basically just define the exchange of parameters across the interface
    # In a sense, this is thrashing the data. We could just meet the interface of generate_simple_shell_sequencing_content
    # directly without these classes. Perhaps they are useful, but to be reconsider if is is helpful or just uncecessary.
    return sss.SSSjob(
        submission_script_fullpath=os.path.join(shell_sequencer.work_dir, 
                                                f'imsi-diagnostics-{shell_sequencer.runid}.sh'),
        job_identifier=f"diag_{shell_sequencer.runid}",
        user_script_to_source=f"{shell_sequencer.run_config_dir}/{configuration.get_unique_key_value('diagnostic_run_script')}",
        directives=configuration.machine.parameters['batch_commands'].get('diag_directives',''),# configuration.get_unique_key_value('directives'), # Was relying on default directives before. Need diag directives.
        listing_prefix=os.path.join(shell_sequencer.work_dir, 'listings', f"imsi_diag_canesm_{shell_sequencer.runid}"),
        dependencies=None,
        cycle_flag='DIAG_RESUBMIT',
        time_counter='DIAG_YEAR',
        clean_scratch=True
    )

#def write_run_cmd_file(run_commands):
#    """Only for testing. Should potentially be put directly into the run file?
#    """
#    with open(f'imsi-model-run.sh', 'w') as f:
#        f.write("#!/bin/bash\n")
#        for cmd in run_commands:
#            f.write(f"{cmd}\n")
#    st = os.stat('imsi-model-run.sh')
#    os.chmod('imsi-model-run.sh', st.st_mode | stat.S_IEXEC)
