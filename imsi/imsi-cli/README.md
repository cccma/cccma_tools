# imsi-cli

This is a wrapper for the main `cli` of `imsi`. It is responsible for interfacing with the main `imsi-src` code located in this repository. Please note this is distinct from `imsi/cli.py`, although users can still use the `cli.py` independently if they have installed imsi. 

What this solution provides is a way to interact with different versions of imsi through a shared environment.

## How this version works:

The `imsi setup` command is the most important for this cli as it needs to gain access to imsi before running imsi commands. 

For fetch_method = clone:
1. The specified repository is cloned and placed in a temporary directory.
2. This cli adds the version of imsi in the temporary directory to the $PYTHONPATH
3. This cli calls `imsi setup repo=path/to/tmp/CanESM... --fetch_method=copy ...` through the `imsi-src` setup steps.
4. The repo from the temporary directory is copied into the setup directory and the temporary directory is cleaned.

For the remaining imsi commands, after `imsi setup` is run, they are all treated the same once a setup directory is created. Namely, the `imsi-cli` searches for the pre-defined path to imsi in the setup directory and executes each command through the `imsi-src`. The difference between running the imsi setup command vs the other imsi commands (list, config, run etc...) is that the source location to imsi needs to be defined as coming from a cloned version of the code (if specified as the fetch method) or a setup directory.

### Installation

From within the `imsi-cli` tree
```bash
pip install . 
```

### Tests

From within the `imsi-cli`:
```bash
pip install .[test] # this installs test requirements
```

To run the tests, do:
```bash
coverage run -m pytest tests/
```

### Technical notes
This code has upstream dependencies on the imsi-src code, so when those are updated, the CLI will also need to be updated.