import subprocess
import sys
import pytest
import tempfile
import shutil
from pathlib import Path
import logging

logger = logging.getLogger(__name__)

@pytest.fixture
def temporary_venv():
    """Create a temporary virtual environment for testing the package installation."""
    temp_dir = tempfile.mkdtemp()
    venv_dir = Path(temp_dir) / "venv"

    # Create a virtual environment
    subprocess.run([sys.executable, "-m", "venv", venv_dir], check=True)

    yield venv_dir

    # Clean up after the test
    shutil.rmtree(temp_dir)


def test_install_imsi_no_errors(temporary_venv):
    """Test that the imsi package installs without errors."""
    venv_dir = temporary_venv
    pip_path = venv_dir / "bin" / "pip"
    python_path = venv_dir / "bin" / "python"

    # Install the imsi package using pip within the virtual environment
    result = subprocess.run(
        [pip_path, "install", "../"],
        cwd=Path(__file__).parent.resolve(),
        shell=True
    )
    
    # Assert that the installation was successful
    assert result.returncode == 0, f"Installation failed: {result.stderr.decode()}"

    # Optionally, you can also check if the imsi command is available and runs without errors
    result = subprocess.run(
        [python_path, "-m", "imsi", "--help"],
        shell=True
    )
    
    assert result.returncode == 0, f"imsi command failed: {result.stderr.decode()}"
