# run an initial setup with imsi setup
import pytest
import subprocess
from pathlib import Path
import logging
import shutil
from test_helpers import repo, branch, run_imsi_setup

logger = logging.getLogger(__name__)


@pytest.mark.dependency()
def test_run_setup_command():
    repo_str = repo()
    test_branch_str = branch()
    run_imsi_setup(repo_str, test_branch_str)
    assert Path("test-imsi-run").exists()
    assert Path("test-imsi-run/src/CCCma_tools/imsi").exists()
    assert Path("test-imsi-run/src/imsi-config/imsi-src").exists()


@pytest.fixture
def cleanup():
    yield
    Path(".imsi-cli.log").unlink(missing_ok=True)
    Path(Path("test-imsi-run")/".imsi-cli.log").unlink(missing_ok=True)
    shutil.rmtree(Path("test-imsi-run").resolve(), ignore_errors=True)


@pytest.mark.parametrize("test_cmd", ["imsi list -emco", "imsi set -h", "imsi reload -h", "imsi config -h"])
@pytest.mark.dependency(depends=["test_run_setup_command"])
def test_imsi_command(test_cmd):
    # delete the test directory no matter the result 
    try:
        result = subprocess.run(
            test_cmd,
            cwd=Path("test-imsi-run").resolve(),
            shell=True,
            stdout=subprocess.PIPE,
        )
        assert result.returncode == 0
        assert "ERROR" not in open(Path("test-imsi-run").resolve() / ".imsi-cli.log").read()

    except Exception as e:
        logger.info(e)

    finally:
        shutil.rmtree(Path("test-imsi-run").resolve(), ignore_errors=True)
