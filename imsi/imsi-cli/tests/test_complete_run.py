import pytest
import subprocess
import shutil
from pathlib import Path
import logging
import os
from test_helpers import branch, repo, run_imsi_setup

logger = logging.getLogger(__name__)


# Pytest fixture for determining the repository URL
@pytest.fixture
def _repo():
    return repo()

# Pytest fixture for the test branch
@pytest.fixture
def _test_branch():
    return branch()

# Pytest fixture to clean up the test directories after the test
@pytest.fixture
def cleanup():
    list_of_runids = ["tst-default", "tst-wrkdir", "tst-copy", "tst-link"]
    yield
    for runid in list_of_runids:
        Path(Path(runid) / ".imsi-cli.log").unlink(missing_ok=True)
        shutil.rmtree(Path(runid).resolve(), ignore_errors=True)

@pytest.fixture
def cleanup_repo():
    yield

    shutil.rmtree(Path("test_clone").resolve(), ignore_errors=True)

def test_setup_default(_repo, _test_branch, cleanup, cleanup_repo):
    runid = "tst-default"
    run_imsi_setup(_repo, _test_branch, runid=runid)
    assert Path(runid).exists()
    assert (Path(runid) / "src" / "CCCma_tools" / "imsi").exists()
    assert (Path(runid) / "src" / "imsi-config" / "imsi-src").exists()

def test_setup_copy(_repo, _test_branch, cleanup, cleanup_repo):
    # Clone the repo to a temporary directory
    runid = "tst-copy"
    clone_location = Path("test_clone").resolve()
    clone_repo = f"git clone -b {_test_branch} --recursive {_repo} {clone_location}"
    subprocess.run(clone_repo, cwd=Path(__file__).parent.resolve(), shell=True)
    checkout_branch = f"git submodule foreach --recursive git checkout {_test_branch}"
    subprocess.run(checkout_branch, cwd=clone_location, shell=True)
    assert clone_location.exists(), "directory not properly cloned!"

    run_imsi_setup(
        clone_location.resolve(),
        _test_branch,
        fetch_method="copy",
        runid=runid,
    )

    assert Path(runid).exists()
    assert (Path(runid) / "src" / "CCCma_tools" /"imsi").exists()
    assert (Path(runid) / "src" / "imsi-config" / "imsi-src").exists()  # symlink

def test_setup_symlink(_repo, _test_branch, cleanup, cleanup_repo):
    # Clone the repo to a temporary directory
    # Clone the repo to a temporary directory
    runid = "tst-link"
    clone_location = Path("test_clone").resolve()
    clone_repo = f"git clone -b {_test_branch} --recursive {_repo} {clone_location}"
    subprocess.run(clone_repo, cwd=Path(__file__).parent.resolve(), shell=True)
    checkout_branch = f"git submodule foreach --recursive git checkout {_test_branch}"
    subprocess.run(checkout_branch, cwd=clone_location, shell=True)
    assert clone_location.exists(), "directory not properly cloned!"

    run_imsi_setup(
        clone_location.resolve(),
        _test_branch,
        fetch_method="link",
        runid=runid,
    )

    assert Path(runid).exists()
    assert (Path(runid) / "src" / "CCCma_tools" / "imsi").exists()
    assert (Path(runid) / "src" / "imsi-config" / "imsi-src").exists()  # symlink

def test_setup_and_config_workdir(_repo, _test_branch, cleanup, cleanup_repo):
    runid = "tst-wrkdir"
    run_imsi_setup(_repo, _test_branch, runid=runid)
    os.environ["WRK_DIR"] = str(Path(runid).resolve())
    process = subprocess.Popen("imsi config", stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
    process.communicate("r\n".encode())
    # unset WRK_DIR so that it doesn't interfere with other tests
    del os.environ["WRK_DIR"]
    # check for errors
    assert "ERROR" not in open(".imsi-cli.log").read()
    assert Path(runid).exists()
    assert (Path(runid) / "src"/ "CCCma_tools" / "imsi").exists()
    assert (Path(runid) / "src"/ "imsi-config" / "imsi-src").exists()  # symlink
