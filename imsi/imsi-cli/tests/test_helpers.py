import socket
import subprocess


# Function to check if a host is available
def is_host_available(host: str, port: int = 22, timeout: int = 5) -> bool:
    """Check if a host is available by trying to connect to a given port."""
    try:
        socket.setdefaulttimeout(timeout)
        with socket.create_connection((host, port)):
            return True
    except (socket.timeout, socket.error):
        return False


def branch():
    return "cli-update-clone-steps"

def repo():
    repos = {
        "gitlab.com": "git@gitlab.com:nannau/CanESM5.git",
        "gitlab.science.gc.ca": "git@gitlab.science.gc.ca:rna001/CanESM5.git",
    }
    return (
        repos["gitlab.science.gc.ca"]
        if is_host_available("gitlab.science.gc.ca")
        else repos["gitlab.com"]
    )


def run_imsi_setup(repo, test_branch, fetch_method="clone", runid="test-imsi-run"):
    test_run = (
        f"imsi setup --fetch_method={fetch_method} --repo={repo} --ver={test_branch} "
        f"--exp=cmip6-piControl --model=canesm51_p1 --seq=maestro --runid={runid}"
    )
    subprocess.run(test_run, shell=True)
