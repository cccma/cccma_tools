from imsi_cli import imsi_logger, imsi_init, path_tools
import click
import sys
import tempfile
from pathlib import Path
import functools

logger = imsi_logger.setup_logger(__name__, log_name=".imsi-setup.log")

@click.command()
@click.option(
    "--runid",
    required=True,
    help='The name of the run to use. Typically a unique short string, not including "_" or special chars.',
)
@click.option(
    "--repo",
    default="git@gitlab.science.gc.ca:CanESM/CanESM5.git",
    help="url or path or repository to clone",
)
@click.option(
    "--ver",
    default="develop_canesm",
    help="Version of the code to clone (commit, branch, tag...",
)
@click.option(
    "--exp",
    default="cmip6-piControl",
    help="The experiment to run, contained under experiments directory in CanESM/CONFIG",
)
@click.option(
    "--model",
    default=None,
    required=True,
    help="The model run, contained under model in CanESM/CONFIG. Can be changed later.",
)
@click.option(
    "--fetch_method",
    default="clone",
    help='Defaults to "clone". Optionally specify "link" or "copy" to softlink or copy the code from disk instead of cloning',
)
@click.option(
    "--seq",
    default=None,
    help='Sequencer to use. Valid options are "iss" and "maestro". Defaults to "iss"',
)
@click.option(
    "--machine",
    default=None,
    help="Optionally specify the machine to use. If not specified, infer machine from hostname.",
)
@click.option(
    "--compiler",
    default=None,
    help="Optionally specify the compiler to use. If not specified, infer compiler from machine.",
)
@click.option(
    "--flow",
    default=None,
    help="Sequencing flow to use.",
)
@click.pass_context
def setup(ctx: click.Context, runid: str, repo: str, ver: str, exp: str, model: str, fetch_method: str, seq: str, machine: str, compiler: str, flow: str) -> None:
    "See imsi setup --help for more information"
    logger.info(f"IMSI setup for {runid}")
    logger.info(f"--runid={runid} "\
                f"--repo={repo} --ver={ver} --exp={exp} "\
                f"--model={model} --fetch_method={fetch_method} "\
                f"--seq={seq} --machine={machine} --compiler={compiler} --flow={flow}")
    logger.info("🎉 🎉 🎉 🎉")  # Separator for clarity in logs

    def setup_ui_manager(repo_path: str, fetch_method: str):
        imsi_path = Path(repo_path) / ctx.obj["imsi_subtree_src"]
        sys.path.insert(0, str(Path(imsi_path).resolve()))
        from imsi.user_interface import ui_manager as uim
        uim.setup_run(
            runid=runid,
            repo=repo_path,
            ver=ver,
            exp=exp,
            model=model,
            fetch_method=fetch_method,
            seq=seq,
            machine=machine,
            compiler=compiler,
            flow=flow,
        )

    if fetch_method == "clone":
        with tempfile.TemporaryDirectory() as tmp_repo_src:
            path_tools.recursive_clone(repo, Path(tmp_repo_src), ver)
            path_tools.check_imsi_src(Path(tmp_repo_src) / ctx.obj["imsi_subtree_src"])
            partial_setup = functools.partial(
                setup_ui_manager,
                tmp_repo_src,
                "copy",
            )
            partial_setup()

    elif fetch_method in ["copy", "link"]:
        partial_setup = functools.partial(setup_ui_manager, repo, fetch_method)
        partial_setup()
