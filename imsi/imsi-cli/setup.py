from setuptools import setup, find_packages

requires = ["Click", "setuptools", "xarray[io]", "json5"]
test_requires = ["pytest", "pytest-dependency", "coverage"]

extras = {
    "test": test_requires,
}

setup(
    name="imsi-cli",
    version="0.1.0",
    py_modules=[
        "imsi_entry",
        "imsi_commands",
        "imsi_standalone_commands",
        "imsi_setup",
    ],
    packages=find_packages(),
    install_requires=requires,
    test_requires=test_requires,
    extras_require=extras,
    entry_points={
        "console_scripts": [
            "imsi=imsi_entry:cli",
        ],
    },
)
