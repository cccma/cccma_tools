from imsi_cli import imsi_init
import click


@click.command("list", help='List available configuration selections')
@click.option(
    "-r",
    "--repo",
    required=False,
    default=None,
    help="Model code directory containing imsi-config subdirectory with imsi json configurations",
)
@click.option(
    "-e",
    "--experiments",
    is_flag=True,
    required=False,
    help="Supported experiment names",
)
@click.option(
    "-m", "--models", is_flag=True, required=False, help="Supported model names"
)
@click.option(
    "-p", "--platforms", is_flag=True, required=False, help="Supported machine names"
)
@click.option(
    "-c", "--compilers", is_flag=True, help="Supported compiler names for this machine"
)
@click.option("-o", "--options", is_flag=True, help="Available model options")
@imsi_init.with_uim
def list(ctx, uim, repo, experiments, models, platforms, compilers, options):
    if any([repo, experiments, models, platforms, compilers, options]):
        uim.list_choices(repo, experiments, models, platforms, compilers, options)  
    else:
        print(click.get_current_context().get_help())


@click.command("set", help="Set an imsi selection in the simulation configuration")
@click.option(
    "-f", "--file", help="The name of a json file containing the imsi selections."
)
@click.option(
    "-s",
    "--selections",
    metavar="KEY=VALUE",
    multiple=True,
    help="A series of KEY=VALUE selection pairs to apply to the imsi simulation configuration",
)
@click.option(
    "-o",
    "--options",
    metavar="KEY=VALUE",
    multiple=True,
    help="A series of KEY=VALUE optin pairs to apply to the imsi simulation configuration",
)
@imsi_init.with_uim
def set(ctx, uim, file, selections, options):
    if any([file, selections, options]):
        uim.set_selections(file, selections, options, force=ctx.obj["force"])
    else:
        print(click.get_current_context().get_help())
