from imsi_cli import imsi_init
import click


@click.command("build", help="Build a simulation")
@imsi_init.with_uim
def build(ctx, uim):
    uim.compile_model_execs()  


@click.command(
    "config",
    help="Configure a simulation, using updated settings from the imsi_configuration_${runid}.json file",
)
@imsi_init.with_uim
def config(ctx, uim):
    uim.update_config_from_json(force=ctx.obj["force"])


@click.command(
    "reload",
    help="Reload the imsi configuration from the on-disk repo jsons and update the simulation configuration",
)
@imsi_init.with_uim
def reload(ctx, uim):
    uim.reload_config_from_source(force=ctx.obj["force"])

@click.command("submit", help="Submit a simulation to run")
@imsi_init.with_uim
def submit(ctx, uim):
    uim.submit_run()
