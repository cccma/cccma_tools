import pathlib
import os
import subprocess


def recursive_clone(repo_address, local_name, ver):
    """
    Clone a git repository recursively, and consistently
    checkout a specified version across all submodules.

    Inputs:
        repo_address : str
            The address to clone from, typically a url.
        local_name : str
            The name to use for the on disk code
        ver : str
            The reference to checkout
    """
    cwd = os.getcwd()
    subprocess.run(["git", "clone", "--recursive", repo_address, local_name])
    os.chdir(local_name)
    subprocess.run(["git", "checkout", "--recurse-submodules", ver])
    subprocess.run(
        ["git", "submodule", "update", "--recursive", "--init", "--checkout"]
    )
    os.chdir(cwd)


def check_runid(runid: str) -> None:
    "Checks length of runid and that it contains only lowercase alphanumeric characters or hyphens"
    runid_length = len(runid)
    if runid_length > 20:
        raise ValueError(
            f"Your runid ---> {runid} <--- is too long! "
            "Runids must be 20 or fewer characters to avoid conflicting with filename restrictions"
        )


def check_imsi_src(model_imsi_dir: pathlib.Path) -> None:
    if not model_imsi_dir.exists():
        raise ValueError(f"IMSI directory not found in {model_imsi_dir}")
