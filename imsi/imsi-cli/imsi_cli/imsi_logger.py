import logging
from pathlib import Path

def setup_logger(name, log_path=".", log_name=".imsi-cli.log"):
    """Sets up the logger."""
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # File handler

    fh = logging.FileHandler(Path(log_path) / log_name)
    fh.setLevel(logging.DEBUG)

    # Console handler
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # Formatter
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    # Add handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    return logger
