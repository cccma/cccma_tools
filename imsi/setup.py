#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

#with open('HISTORY.rst') as history_file:
history = ''

requirements = ['cftime', 'xarray', 'dataclasses', 'json5']

test_requirements = [ ]

setup(
    author="CCCma Technical Development Team",
    author_email='',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Open Government License Canada',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Integrated Modelling System Infrastructure (IMSI) is a comprehensive software project used to download, configure, build and run the suite of models in the CCCma Integrated Modelling System.",
    entry_points={
        'console_scripts': [
            'imsi-src-cli=imsi.cli:main',
        ],
    },
    install_requires=requirements,
    license="Open Government License Canada 2.0",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='imsi',
    name='imsi',
    packages=find_packages(include=['imsi', 'imsi.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='',
    version='0.3.0',
    zip_safe=False,
)
