[![build status](https://gitlab.science.gc.ca/CanESM/canesm-ensemble/badges/master/build.svg)](https://gitlab.science.gc.ca/CanESM/canesm-ensemble/commits/master) 
[![coverage report](https://gitlab.science.gc.ca/CanESM/canesm-ensemble/badges/master/coverage.svg)](https://gitlab.science.gc.ca/CanESM/canesm-ensemble/commits/master)



# CanESM5 Setup Helper

A python package to help setup and run CanESM5 ensembles (or single runs). You can use it from your laptop if you
have python 3 installed, or you can use a version installed on the Science network.

[Read the docs](http://hpfx.science.gc.ca/~scrd104/documentation/canesm-ensemble/) for more info
on how it works.