from canesm.canesm_database import CanESMensembleDB
from canesm.exceptions import RemoteError
from fabric import Connection
import time
from datetime import datetime


class CanESMsubmitter:

    def __init__(self, db: CanESMensembleDB, delay: int = 0, num_jobs: int = None, debug: bool = False):

        self.db = db
        self.max = num_jobs
        self.n = 0
        self.current_job = None
        self.delay = delay
        self.query_delay = 60  # delay between queries in seconds if waiting on a job setup
        self.debug = debug

    def __iter__(self):

        self.current_job = None
        self.n = 0
        return self

    def __next__(self):

        if self.max is not None and self.n >= self.max:
            raise StopIteration

        try:
            self.current_job = self.db.query('SELECT runid, rundirectory '
                                             'FROM status WHERE submitted=0 AND setup=1 LIMIT 1')
        except FileNotFoundError:
            message = 'Could not find the ensemble database. Have you called "setup-ensemble" ' \
                      'before calling "submit-ensemble"?'
            raise FileNotFoundError(message)

        if len(self.current_job) == 0:
            unsubmitted = self.db.query('SELECT runid FROM status WHERE submitted=0 AND setup=0 LIMIT 1')
            if len(unsubmitted) == 0:
                raise StopIteration
            else:
                print('waiting on job setup...')
                self.current_job = False
        else:
            self.n += 1
            self.current_job = self.current_job[0]
        return self

    def submit(self, runid: str = None):
        """
        Submit the next job in the queue to the remote machine

        Parameters
        ----------
        runid:
            job to be submitted on remote machine
        """
        launch_command = f"expbegin -e $SEQ_EXP_HOME -d {datetime.now().strftime('%Y%m%d%H')}"
        env_setup_commands = f"source ~/.profile >> /dev/null 2>&1; source env_setup_file && "
        submit_command = f"{env_setup_commands} {launch_command}"

        if runid is None:
            runid = self.current_job[0]
            run_directory = self.current_job[1]
        else:
            run_directory = self.db.get(column='rundirectory', keys=runid)

        if not runid:
            time.sleep(self.query_delay)
        else:
            with Connection(self.db.machine, self.db.user) as remote:
                try:
                    if self.debug:
                        print(submit_command)
                    else:
                        with remote.cd(run_directory):
                            remote.run(submit_command)
                    self.db.set(column='submitted', keys=runid, values=1)
                    time.sleep(self.delay * 60)
                except RemoteError:
                    raise RemoteError('could not submit job: ' + runid)
                print(f"submitted {runid}!")
