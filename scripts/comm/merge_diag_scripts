#!/bin/sh

USAGE='
 USAGE
        merge_diag_scripts -o merged.sh script1.sh script2.sh ...

 DESCRIPTION
        This shell script merges individual diagnostic scripts script1, 
        script2, ... into a single diagnostic script.

 FLAGS
        -o merged.sh
                The name of the output merged diagnostic script. Must be specified.

        -f      If specified, Overwrite the existing merged diagnostic script.
                Otherwise, the script will abort if the output script exists.

        -l diag_libs
                The colon separated list of directories to search for input scripts.
                The default is "$CCRNSRC/source/diag4".

        script1.sh script2.sh ...
                Input diagnostic script names to be merged. At least one script must 
                be specified.
 
        -h      Print usage help.

# Authors:
#
# S.Kharin and F.Majaess, Oct 2017
'
if [ $# -lt 1 ] ; then
    echo "$USAGE"
    exit
fi

#
# Evaluate the command line options
#
input_scripts=''
output_script=''
force='off'
diag_libs="$CCRNSRC/source/diag4"
while [ $# -ge 1 ] ; do
    case "$1" in
        -o)     shift
                output_script=$1
                ;;
        -f)     force='on'
                ;;
        -l)     shift
                diag_libs=$1
                ;;
        -h)     echo "$USAGE"
                exit
                ;;
        *)      input_scripts="${input_scripts} $1"
                ;;
    esac
    shift
done

echo input_scripts="${input_scripts}"
if [ -z "${input_scripts}" ] ; then
    echo "ERROR: input scripts are not specified."
    echo "$USAGE"
    exit
fi
echo output_script="${output_script}"
if [ -z "${output_script}" ] ; then
    echo "ERROR: output_script script is not specified."
    echo "$USAGE"
    exit
fi
echo diag_libs="$diag_libs"
echo force="$force"

# check if the output script already exists
if [ -s ${output_script} ] ; then
    if [ "$force" = "on" ] ; then
	echo "remove the existing output script ${output_script}"
	rm -f ${output_script}
    else
	echo "ERROR: the output script ${output_script} already exist."
	echo "Please remove it first, or use -f flag"
	exit 1
    fi
fi

# Execute_Script opening part
outname=`basename ${output_script} .sh`

echo "#!/bin/sh
set -e
startdate=\`date +%s\`
pwd=\`pwd\`" > ${output_script}

# merge all scripts (create temp dir for each script)
for script in ${input_scripts} ; do
  echo Processing $script ...
  # scan all directories where scripts can be found
  dirs=${diag_libs}
  while [ -n "$dirs" ] ; do
    dir=`echo "$dirs" | cut -f1 -d':'`
    script_found="off"
    if [ -f $dir/$script ] ; then
      script_found="on"
      echo Found $dir/$script
      echo "
#========= $script start ===================================
echo $script start ; rm -rf \$pwd/tmp.$script ; mkdir -p \$pwd/tmp.$script ; cd \$pwd/tmp.$script" >> ${output_script}
      cat $dir/$script >> ${output_script}
      echo "
echo $script end ; cd \$pwd; rm -rf \$pwd/tmp.$script
#========= $script end =====================================" >> ${output_script}

      # break if the script is processed
      break
    fi
    # search in the next directory
    dirs=`echo "$dirs" | cut -s -f2- -d':'`
  done # dirs
  if [ "$script_found" = "off" ] ; then
    echo "ERROR: $script is not found!"
    exit 1
  fi
done # input_scripts

# Execute_Script closing part

echo "
stopdate=\`date +%s\`
echo runtime \$flabel \$startdate \$stopdate \`expr \$stopdate - \$startdate\`
" >> ${output_script}

echo "Done!"
