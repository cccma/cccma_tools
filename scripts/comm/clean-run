#!/bin/bash
# take the given runid as the first and only argument and clean up various directories associated with it
#   note: only cleans up the setup directory if called in the directory that contains it
#
#   TODO: Add support for different platforms
#   TODO: rationalize the split between the CCCma_tools/generic/ and
#           CONFIG/PLATFORM/
#   TODO: figure out a better place to store the maestro paths
#~~~~~~~~~~~~~~~~~
# Define defaults
#~~~~~~~~~~~~~~~~~
default_platform="eccc-u2"
maestro_link_definitions="${HOME}/.suites/.default_links"
maestro_canesm_links="${HOME}/.suites/canesm"
clean_setup_dir=0 # 0 = no, 1 = yes
clean_temp_dir=0 # 0 = no, 1 = yes
clean_data_dir=0 # 0 = no, 1 = yes
use_pattern=0 # 0 = no, 1 = yes

#~~~~~~~~~~~~~~~~~
# Define functions
#~~~~~~~~~~~~~~~~~
function usage(){
    echo "$(basename $0) [-h] [-s] [-t] [-d] [-p] RUNID/PATTERN"
    echo "  Delete any combination of temporary run files [-t], run data files [-d], and"
    echo "    the run's local setup directory [-s] (which must exist in the PWD"
    echo "    to be cleaned). Where the temporary run files"
    echo "    include the source repo and maestro files."
    echo "    Default with no flags deletes temporary run files and"
    echo "    run data files."
    echo ""
    echo "    Positional Params:"
    echo "        RUNID : the desired run to clean up"
    echo "  "
    echo "    Flags:"
    echo "        -h : display this usage information"
    echo "        -s : clean local setup directory"
    echo "        -t : clean temp data directory"
    echo "        -d : clean run data directory"
    echo "        -p : clean all runs with PATTERN"
    echo ""
    echo "          NOTE: Make sure when flag '-p' is set you put quotes around PATTERN"
    echo "          For example, clean-run -p \"test*\" would be all runs beginning with test"
    echo ""
}

function parse_args(){
    local _num_args _flag
    while getopts hstdp _flag; do
        case ${_flag} in
            s) clean_setup_dir=1 ;;
            t) clean_temp_dir=1 ;;
            d) clean_data_dir=1 ;;
            p) use_pattern=1 ;;
            h) usage; exit 0 ;;
            *) usage; exit 1 ;;
        esac
    done
    shift $(( OPTIND - 1 ))

    # Check if no options were processed, then activate temp and data
    if [ "$clean_setup_dir" -eq 0 ] && [ "$clean_temp_dir" -eq 0 ] && [ "$clean_data_dir" -eq 0 ]; then
        clean_temp_dir=1
        clean_data_dir=1
    fi
    RUNID="$1"

    _num_args=$#
    if (( _num_args != 1 )); then
        echo "$(basename $0) expects a single runid at the command line!"
        echo "if '-p' set, wildcard may have expanded to multiple items. Use quotes around PATTERN to avoid this."
        usage
        exit 1
    fi
}

function check_env(){
    [[ -z $CCCMA_REF ]] && { echo "$(basename $0) requires CCCMA_REF to be defined!"; exit 1 ;}
    [[ -z $RUN_TEMP_FILES_STORAGE_DIR ]] && { echo "$(basename $0) requires RUN_TEMP_FILES_STORAGE_DIR to be defined!"; exit 1 ;}
}

function determine_target_machines(){
    local _platform_config_directory
    local _platform_compute_system_files
    local _compute_system_file
    local _frontend _backend

    _platform_config_directory=${CCCMA_REF}/CanESM_source_link/CONFIG/PLATFORM/${default_platform}
    _platform_compute_system_files=$(ls ${_platform_config_directory}/*.compute_system.cfg)

    # extract machine names from platform config files
    MACHINES=""
    for _compute_system_file in ${_platform_compute_system_files}; do
        _frontend=$(grep "FRONTEND=" $_compute_system_file | awk -F= '{print $2}' | sed 's/\"//g')
        _backend=$(grep "BACKEND=" $_compute_system_file | awk -F= '{print $2}'| sed 's/\"//g')
        MACHINES="${MACHINES} ${_frontend} ${_backend}"
    done
    echo "Cleaning files from $MACHINES"
}

function clean_run_temp_files(){
    local _run_temp_file_storage_dir
    local _machine
    local _maestro_temp_location
    local _remote_command
    local _dir_doesnt_exist_status=66
    local _remote_command_return_status

    # main temporary file space
    _run_temp_file_storage_dir=${RUN_TEMP_FILES_STORAGE_DIR}/${RUNID}
    if [[ -d ${_run_temp_file_storage_dir} ]]; then
        echo "Cleaning $_run_temp_file_storage_dir"
        rm -rf $_run_temp_file_storage_dir
    else
        echo "  Could not find a temporary file storage dir at $_run_temp_file_storage_dir"

        # TEMPORARY: handle old RUN_TEMP_FILES_STORAGE definition
        if [[ $RUN_TEMP_FILES_STORAGE_DIR =~ .*canesm_runs.* ]]; then
            _run_temp_file_storage_dir=${RUN_TEMP_FILES_STORAGE_DIR/\/canesm_runs/}/${RUNID}
            if [[ -d ${_run_temp_file_storage_dir} ]]; then
                echo "Cleaning $_run_temp_file_storage_dir"
                rm -rf $_run_temp_file_storage_dir
            fi
        fi
    fi

    # maestro temporary files
    for _machine in $MACHINES; do
        _maestro_temp_location=$(readlink -f ${maestro_link_definitions}/${_machine})
        echo "Attempting to clean ${_maestro_temp_location}/${RUNID} on ${_machine}"
        _remote_command="[[ -d ${_maestro_temp_location}/${RUNID} ]] && cd $_maestro_temp_location || exit ${_dir_doesnt_exist_status}; rm -rf $RUNID"
        ssh -o LogLevel=quiet ${_machine} "${_remote_command}" > /dev/null 2>&1 # supress output to avoid superfluous output
        _remote_command_return_status=$?
        if (( _remote_command_return_status == _dir_doesnt_exist_status )); then
            echo "  ${_maestro_temp_location}/${RUNID} doesn't exist on ${_machine}"
        elif (( _remote_command_return_status == 0 )); then
            echo "  Cleaned"
        else
            echo "  Problem cleaning temporary files on ${_machine}"
        fi
    done

    # maestro suites link
    find ${maestro_canesm_links} -name "${RUNID}" -delete
}

function clean_run_data_directories(){
    local _machine
    for _machine in $MACHINES; do
        delete_machines_data_directory ${_machine}
    done
}

function clean_run_setup_directory(){
    local _setup_directory
    local _num_matches
    local _response
    _setup_directory=$(find . -type d -name "${RUNID}")
    _num_matches=$(echo $_setup_directory | wc -w)
    if (( _num_matches == 1 )); then
        echo "Cleaning $_setup_directory..."
        rm -rf ${_setup_directory}
    elif (( _num_matches == 0 )); then
        echo "Failed to find any local setup directory for $RUNID"
    else
        echo "Found more than one local setup directory for $RUNID!!! Unable to determine which to clean."
    fi
}

function delete_machines_data_directory(){
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Here we try to delete `RUNPATH_ROOT/users/USER/canesm_runs/RUNID` AND
    #   the old template of `RUNPATH_ROOT/users/USER/RUNID.
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    local _machine=$1
    local _remote_command
    local _remote_command_return_status
    local _runpath_dir_doesnt_exist_status=64
    local _data_dir_doesnt_exist_status=65
    printf "Attempting to clean \n\t\t\$RUNPATH_ROOT/users/$(whoami)/canesm_runs/${RUNID} or "
    printf "\n\t\t\$RUNPATH_ROOT/users/$(whoami)/${RUNID} \n\ton ${_machine}\n"

    # build command
    _remote_command="source $CCCMA_REF/env_setup_file;"
    _remote_command+=" user_runpath=\$RUNPATH_ROOT/users/$(whoami);"
    _remote_command+=" [[ -d \$user_runpath ]] && cd \$user_runpath || exit ${_runpath_dir_doesnt_exist_status};"
    _remote_command+=" [[ -d canesm_runs/$RUNID ]] && chmod -R a+rw canesm_runs/$RUNID || exit ${_data_dir_doesnt_exist_status};"
    _remote_command+=" rm -rf canesm_runs/$RUNID"

    # run command
    ssh -o LogLevel=quiet ${_machine} "$_remote_command" > /dev/null 2>&1 # supress output to avoid superfluous output
    _remote_command_return_status=$?

    # inform user
    if (( _remote_command_return_status == _runpath_dir_doesnt_exist_status )); then
        echo "  $(whoami)'s \$RUNPATH_ROOT user directory doesn't exist!"
    elif (( _remote_command_return_status == _data_dir_doesnt_exist_status )); then
        echo "  no $RUNID directory under \$RUNPATH_ROOT/users/$(whoami)/canesm_runs on ${_machine}"

        # TEMPORARY for legacy file structure template
        delete_legacy_data_directory_template ${_machine}
    elif (( _remote_command_return_status == 0 )); then
        echo "  Cleaned"
    else
        echo "  Problem cleaning data directories on ${_machine}!"
    fi
}

function delete_legacy_data_directory_template(){
    local _machine=$1
    local _remote_command
    local _remote_command_return_status
    local _legacy_data_dir_doesnt_exist_status=66

    # build command
    _remote_command="source $CCCMA_REF/env_setup_file;"
    _remote_command+=" legacy_data_dir=\$RUNPATH_ROOT/users/$(whoami)/${RUNID};"
    _remote_command+=" [[ -d \$legacy_data_dir ]] && chmod -R a+rw \$legacy_data_dir || exit ${_legacy_data_dir_doesnt_exist_status};"
    _remote_command+=" rm -rf \$legacy_data_dir"

    # run command and catch return code
    ssh -o LogLevel=quiet ${_machine} "$_remote_command" > /dev/null 2>&1 # supress output to avoid superfluous output
    _remote_command_return_status=$?

    # inform user
    if (( _remote_command_return_status == _legacy_data_dir_doesnt_exist_status )); then
        echo "  no $RUNID directory under \$RUNPATH_ROOT/users/$(whoami) on ${_machine}"
    elif (( _remote_command_return_status == 0 )); then
        echo "  found $RUNID directory under \$RUNPATH_ROOT/users/$(whoami) on ${_machine}!"
        echo "  Cleaned"
    else
        echo "  Problem cleaning legacy data directory on ${_machine}!"
    fi
}

function get_runids_from_pattern(){
    # Find all directories containing the pattern in their names
    runid_list=()

    if (( clean_temp_dir == 1 )); then
        DIRECTORY=${RUN_TEMP_FILES_STORAGE_DIR} # temp data directory
        echo $DIRECTORY
        while IFS= read -r dir; do
            runid_list+=("$(basename "$dir")")
        done < <(find "$DIRECTORY" -maxdepth 1 -type d -name "$pattern")
    fi
    if (( clean_data_dir == 1 )); then
        DIRECTORY=$RUNPATH_ROOT/users/$(whoami)/canesm_runs/  # data directory
        echo $DIRECTORY
        while IFS= read -r dir; do
            runid_list+=("$(basename "$dir")")
        done < <(find "$DIRECTORY" -maxdepth 1 -type d -name "$pattern")
    fi
    if (( clean_setup_dir == 1 )); then
        DIRECTORY=. # setup directory
        while IFS= read -r dir; do
            runid_list+=("$(basename "$dir")")
        done < <(find "$DIRECTORY" -maxdepth 1 -type d -name "$pattern")
    fi

    runid_list=($(printf "%s\n" "${runid_list[@]}" | sort -u))  # removes duplicate entries

}


#~~~~~~~~~~~~~~
# Main Program
#~~~~~~~~~~~~~~
parse_args "$@"
check_env
determine_target_machines
if (( use_pattern == 1 )); then
    pattern="$RUNID"
    get_runids_from_pattern
    if [ ${#runid_list[@]} == 0 ]; then
        echo "This patten does not delete any runs. "
        usage
        exit 1
    fi
    echo "WARNING: This will be deleting ${#runid_list[@]} runs."
    echo "The names of the runs you are deleting are as follows:"
    for item in "${runid_list[@]}"; do
        echo - $item
    done
    read -p "Do you want to proceed with deletion? (y/[n]) " -r 
    if  [[ $REPLY =~ ^[Yy]$ ]] ; then

        for item in "${runid_list[@]}"; do
            RUNID=$item
            echo deleting run "$RUNID"
            if (( clean_temp_dir == 1 )); then
                clean_run_temp_files
            fi
            if (( clean_data_dir == 1 )); then
                clean_run_data_directories
            fi
            if (( clean_setup_dir == 1 )); then
                clean_run_setup_directory
            fi
        done
    else
        echo "Aborting deletion"
    fi
else

    RUNID="${RUNID%/}"  # removes trailing "/" if there
    if (( clean_temp_dir == 1 )); then
        clean_run_temp_files
    fi
    if (( clean_data_dir == 1 )); then
        clean_run_data_directories
    fi
    if (( clean_setup_dir == 1 )); then
        clean_run_setup_directory
    fi
fi
