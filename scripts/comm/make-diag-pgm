#!/bin/bash

set -e
#=============
# Set defaults
#=============
candiag_build_dir=${CANDIAG_REF_PATH}/build
candiag_lib_name=libcandiag.a
candiag_size_module_name=diag_sizes.o
compiler="intel"
platform="eccc-u2"
machine=""
precision="64bit"
link_in_ncar_graphics="no"
output_name=""
log_file="$(pwd)/.make_diag.log"

#==========
# Functions
#==========
# usage
usage(){
    echo "Compile fortran program using CanDIAG library"
    echo ""
    echo "Usage: $(basename $0) [-s|-d] [-r] [-N] [-o OUTEXE] [-l CANDIAGBUILDDIR] [-c COMPILER] [-p PLATFORM] [-m MACHINE] SRC_FILE"
    echo " [-s|-d]                  : (s)ingle or (d)ouble precision (32bit vs 64bit)"
    echo "                            Defaults to double precision"
    echo " [-N]                     : activates compilation with linking to NCAR graphics - only functional"
    echo "                            on machines with NCAR graphics library installed. NOT YET SUPPORTED ON U2"
    echo " [-o OUTEXE ]             : name/path of the desired executable"
    echo "                            Defaults to using the name of the source file without the extension"
    echo " [-l CANDIAGBUILDDIR]     : CanDIAG build directory that should contain the libraries."
    echo '                            Defaults to $CANDIAG_REF_PATH/build'
    echo ' [-c COMPILER]            : what compiler to use. Currently supports : intel' 
    echo "                            Defaults to intel"
    echo " [-p PLATFORM]            : what PLATFORM directory $(basename $0) looks for the make templates in."
    echo "                            Defaults to eccc-u2"
    echo " [-m MACHINE]             : use a machine specific make template within desired platform config directory. "
    echo "                            If not given, defaults to the default template (with no machine appended to the end)."
    echo "                            Ex: -m ppp, will cause $(basename $0) to look for CANDIAG/BUILDDIR/PLATFORM/PLATFORM_NAME/make.template.COMPILER.ppp"
    echo "                            else it will use CANDIAGBUILDDIR/PLATFORM/PLATFORM_NAME/make.template.COMPILER"
    echo " SRC_FILE                 : Fortran source file"
    echo ""
    echo ""
    echo "Examples:"
    echo ""
    echo ' compile a double precision executable, myexe, from test.F, using the default platform (eccc-u2) and compiler (intel)'
    echo '  combination, linking against the CanDIAG library under ${CANDIAG_REF_PATH}/build/lib64 where'
    echo '  the flags are pulled from $CANDIAG_REF_PATH/build/PLATFORM/eccc-u2/make.template.intel'
    echo ""
    echo "      make-diag-pgm -o myexe test.F"
    echo ""
    echo "  or to create an executable named test"
    echo ""
    echo "      make-diag-pgm test.F"
    echo ""
    echo ' compile a single precision executable, test, from test.F using the eccc-u2 platforms intel compiler template'
    echo '  and linking against the default CanDIAG library contained at ${CANDIAG_REF_PATH}/build/lib32.'
    echo '  Where the flags are pulled from $CANDIAG_REF_PATH/build/PLATFORM/eccc-u2/make.template.intel'
    echo ""
    echo "      make-diag-pgm -p eccc-u2 -c intel -s test.F"
    echo ""
    echo ' compile a double precision executable, test, from test.F, using the default platform (eccc-u2) and compiler (intel)'
    echo '  combination, linking against the CanDIAG library under /path/to/user/CanESM5/CanDIAG/build/lib64 where'
    echo '  the flags are pulled from /path/to/user/CanESM5/CanDIAG/build/PLATFORM/eccc-u2/make.template.intel'
    echo ""
    echo "      make-diag-pgm -d -l /path/to/user/CanESM5/CanDIAG/build test.F"
    echo ""
    echo ' compile a single precision executalble, test, from test.F, special-platform/special-machine intel compilation flags'
    echo '   linking against CanDIAG library under /path/to/user/CanESM5/CanDIAG/build/lib64 where'
    echo '   the flags would be pulled from /path/to/user/CanESM5/CanDIAG/build/PLATFORM/special-platform/make.template.intel.special-machine'
    echo ""
    echo "      make-diag-pgm -l /path/to/user/CanESM5/CanDIAG/build -p special-platform -m special-machine test.F"
    echo ""
}

# bail
bail(){
    echo "*******************************************"
    echo "ERROR:"
    echo "  $1"
    echo "*******************************************"
    exit 1
}

# extract variable from make template file
extract_make_var(){
    var_to_extract=$1
    file_to_extract_from=$2
    expand=$3
    var=$( sed -n "s/${var_to_extract}\s*=//p" ${file_to_extract_from} )

    # check for additional variables in the extracted string
    #   - we use a while loop as this may need to happen a few times due to nested variables
    while true; do
        # break from this loop if expand is explicitly set to 'dont_expand'
        if [[ "${expand}" == "dont_expand" ]]; then
            break
        fi

        # break from this loop if there aren't any other variables
        #  - *\$\([^\s]*\)* attempts to find vars matching $(VAR)
        if ! [[ "$var" == *\$\([^\s]*\)* ]]; then
            break
        fi
        
        # extract the nested variables
        extra_vars=$( echo $var | grep -oP '(?<=\$\().*?(?=\))' )

        # loop through the extra vars and extract them replace in
        # 'var' string
        for extra_var in $extra_vars; do
            extra_var_value=$( sed -n "s/${extra_var}\s*=//p" ${file_to_extract_from} )
            var=$( echo $var | sed "s/\$(${extra_var})/${extra_var_value}/" )
        done
    done
    echo $var
}

#==========================
# Process Command Line Args
#==========================
# flags
while getopts "l:c:dso:m:p:hN" opt; do
    case $opt in
        l) candiag_build_dir="$OPTARG"     # where the libraries and compiler settings are found
           user_defined_build_dir=1     ;;
        d) precision="64bit"            ;; # double precision
        s) precision="32bit"            ;; # single precision
        c) compiler="$OPTARG"           ;; # compiler to use
        o) output_name="$OPTARG"        ;; # name/path of the desired output executable
        p) platform="$OPTARG"           ;; # what platform config directory should be used
        m) machine="$OPTARG"            ;; # use a machine specific make template for the desired platform
        N) link_in_ncar_graphics="yes"  ;; 
        h) usage ; exit                 ;; 
        ?) usage ; exit 1               ;; 
    esac
done
shift $(( OPTIND - 1 ))

# argument (only accepts one)
num_args=$#
if ! (( num_args == 1 )); then
    bail "$(basename $0) expects ONE source file. See $(basename $0) -h"
else
    source_file=$1
fi

# consistency/error checks
#--- candiag build directory
if ! (( user_defined_build_dir == 1 )); then
    # trying to use $CANDIAG_REF_PATH, check that the variable is defined
    if [[ -z $CANDIAG_REF_PATH ]]; then
        bail 'No CanDIAG build directory was given, but $CANDIAG_REF_PATH is not defined! Either load a proper environemnt or point to one with -l'
    fi
fi
if ! [[ -d $candiag_build_dir ]]; then
    bail "The defined CanDIAG build directory, $candiag_build_dir, doesn't exist!"
fi

#-- link_in_ncar_graphics is not yet supported on U2
if [[ $link_in_ncar_graphics == "yes" ]]; then
    bail 'Linking against ncar graphics library is not yet supported on U2!'
fi

#--- check that the given source file exists
if ! [[ -e $source_file ]]; then
    bail "$source_file doesn't exist!"
fi

#--- define output_name if none given
if [[ -z $output_name ]]; then
    output_name=${source_file%.*}
fi

#--- check that the desired platform directory exists
platform_config_dir=${candiag_build_dir}/PLATFORM/${platform}
if ! [[ -d ${platform_config_dir} ]]; then
    # note, if desired we could add the ability to define a directory outside of CANDIAGBUILDDIR
    bail "There is no ${platform} directory within ${candiag_build_dir}/PLATFORM. You must use an existing platform! See $(basename $0) -h"
fi

#--- check that the compiler template for the compiler/platform and machine (if applicable) combo exists
make_template_file=${platform_config_dir}/make.template.${compiler}
[[ -n $machine ]] && make_template_file=${make_template_file}.${machine}
if ! [[ -e $make_template_file ]]; then
    bail "There is not a make template file for the desired platform/compiler and machine (if applicable) combination! See $(basename $0) -h"
fi

#==============
# Main Program
#==============
#--- source the compilation environment
source ${platform_config_dir}/compilation_environment >> /dev/null 2>&1 || 
    echo "WARNING: failed to source the default compilation environment. Compilation may still work."

#--- determine the library we need and what flags we need to use to be consistent with the make 
#    CanDIAG Makefile
if [[ "$precision" == "64bit" ]]; then
    lib_dir=lib64
    fflags_to_use=$( extract_make_var FFLAGS_64BIT ${candiag_build_dir}/Makefile dont_expand )
elif [[ "$precision" == "32bit" ]]; then
    lib_dir=lib32
    fflags_to_use=$( extract_make_var FFLAGS_32BIT ${candiag_build_dir}/Makefile dont_expand )
fi
fflags_to_use=$(echo $fflags_to_use | sed 's/\$(//g;s/)//g') # remove '$(' and ')' from list of flags to extract

#--- get compiler and compilation flag information
FC=$( extract_make_var FC $make_template_file )
FFLAGS_MOD=$( extract_make_var FFLAGS_MOD $make_template_file )
if [[ "$link_in_ncar_graphics" == "yes" ]]; then
    NCAR_GRAPHICS_LINK_FLAGS=$( extract_make_var LDFLAGS_CANDIAG_PLT $make_template_file )
    if [[ -z ${NCAR_GRAPHICS_LINK_FLAGS} ]]; then
        bail "Failed to extract linking flags for NCAR Graphics from $make_template_file. Is it supported for this platform/compiler/machine?"
    fi
fi
FFLAGS=$(for flag in $fflags_to_use; do extract_make_var $flag $make_template_file; done)
FFLAGS=$( echo $FFLAGS | tr '\n' ' ')

#--- check if the library and size module exists, else lets make them
candiag_lib=${candiag_build_dir}/${lib_dir}/${candiag_lib_name}
candiag_sizes_mod=${candiag_build_dir}/${lib_dir}/${candiag_size_module_name}
cd $candiag_build_dir
make folders COMPILER_TEMPLATE=${make_template_file} > $log_file 2>&1
if ! [[ -e $candiag_lib ]]; then
    echo "$candiag_lib does not exist! Building it first."
    make -j 4 ${lib_dir}/${candiag_lib_name} COMPILER_TEMPLATE=${make_template_file} >> $log_file 2>&1
fi
if ! [[ -e $candiag_sizes_mod ]]; then
    echo "$candiag_sizes_mod does not exist! Building it first."
    make ${lib_dir}/${candiag_size_module_name} COMPILER_TEMPLATE=${make_template_file} >> $log_file 2>&1
fi
cd - >> /dev/null


#--- compile!
compile_command="${FC} ${FFLAGS} -o ${output_name} ${source_file} ${candiag_lib} ${candiag_sizes_mod} ${FFLAGS_MOD} ${candiag_build_dir}/${lib_dir}"
[[ ${link_in_ncar_graphics} == "yes" ]] && compile_command="${compile_command} ${NCAR_GRAPHICS_LINK_FLAGS}"
echo "$compile_command"
$compile_command
