#!/bin/sh

#    Sep 11/91 - E. Chan

#id  rewind  - rewind restores an ascii text file back to its original  
#id            form after a copyr, copyf, or copyd operation

#    AUTHOR  - E. Chan

#hd  PURPOSE - "rewind" regenerates the original file after a "copyr",
#hd            "copyf", or "copyd" operation has been performed on the file.
#hd            One or more files may be rewound.
#hd 
#hd            The copy operations emulate Fortran (or COS) file pointer
#hd            positioning by accumulating the copied lines in a separate
#hd            file and replacing the original input file with the 
#hd            remaining uncopied lines. Subsequent copy operations on 
#hd            the same input file name will result in the further editing
#hd            of these files. "Rewind" concatenates these two files
#hd            back into the original file.
#hd 
#hd            This is applicable only if the file contains ascii text. If the
#hd            file contains non-ascii text, the script doesn't do anything
#hd            since the copy operations do not alter non-ascii files.
#hd            Non-ascii files are already "rewound".
#hd 
#hd            This script is designed to emulate the "rewind" command
#hd            of the Cray Operating System.

#pr  PARAMETERS:
#pr 
#pr    POSITIONAL
#pr 
#pr      fn1 fn2 ... fnm = list of m files to be rewound

#ex  EXAMPLE:
#ex    
#ex   rewind file1 file2 file3
#ex 
#ex   The above example restores 'file1', 'file2', and 'file3' back to 
#ex   their original form prior to any "copyr"/"copyf"/"copyd" operation.

#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script).

IFS=' '
export IFS

#  * Get the files.
 
file_list=$@

#  * For each specified file, join the two pieces of the original ascii file
#  * back together if the file with the '_Copied_Lines' extension exists.

for file in $file_list
do 
  if [ -f "${file}_Copied_Lines" ] ; then
    cat ${file}_Copied_Lines $file > File 
    mv File $file
    rm -f ${file}_Copied_Lines    
  fi 
done

exit

